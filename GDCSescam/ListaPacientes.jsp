<html>
<head>
<title>Pacientes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/style_ex.css" type="text/css">
<script language="JavaScript" src="script/css.js"></script>
<script Langage="JavaScript">
<!--
function validate(theForm) {
  return true;
}

function accionAceptar(){
	window.close();
}

function accionCancelar(){
	window.close();
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function accionTarjeta(){
	window.showModalDialog("registroTarjeta.jsp","","dialogWidth:400px;dialogHeight:250px;center:yes;dialogTop:200px;dialogLeft:300px");
}

function accionFechaDesde(){
	document.all.txtFDesde.value = window.showModalDialog("calendario.jsp","","dialogWidth:370px;dialogHeight:240px;center:yes;dialogTop:200px;dialogLeft:300px");
}

function accionFechaHasta(){
	document.all.txtFHasta.value = window.showModalDialog("calendario.jsp","","dialogWidth:370px;dialogHeight:240px;center:yes;dialogTop:200px;dialogLeft:300px");
}


//-->
</script>
</head>
<body topmargin="16" leftmargin="16" rightmargin="0" onLoad="MM_preloadImages('imagenes/bot_buscar_sel.gif','imagenes/bot_aceptar_sel.gif','imagenes/bot_cancelar_sel.gif','imagenes/calendario_sel.gif','imagenes/Tarxeta_sel.gif')">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      
    <td width="25" height="24" > <img src="imagenes/Circunfai-p.gif" border="0" width="25" height="100%" alt=""> 
    </td>
      <td height="24" width="100%" bgcolor="#DAE4F2"></td>
      <td width="25" height="24">
	  	  <img src="imagenes/Circunfad-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
    </tr>
    <tr>
      <td width="25" bgcolor="#DAE4F2"></td>
      <td width="100%" bgcolor="#DAE4F2">
  <form method="post" onsubmit="return validate(this);">
        <table width="546" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
        <td colspan="3">
          <table border="0" width="100%" cellspacing="0" cellpadding="12" align="center">
            <tr>
                  <td class="titulo" align="center">Pacientes</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr height="1">
        <td height="1" colspan="3" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr>
        <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
        <td>
                <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
                <tr> 
                  <td colspan="2"  class="standar"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr> 
                        <td width="9%" class="standar">NHC:</td>
                        <td width="18%" class="standar"><input type="text" name="serie" size="15" maxlength="20" class="input" value=""></td>
                        <td width="13%" class="standar">TIS:</td>
                        <td width="13%" class="standar"><input type="text" name="serie43" size="15" maxlength="20" class="input" value=""></td>
                        <td width="17%" class="standar"> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','imagenes/Tarxeta_sel.gif',1)"><img src="imagenes/Tarxeta.gif" alt="Tarjeta Sanitaria" name="Image41" width="32" height="20" border="0" onClick="MM_callJS('accionTarjeta()')"></a></td>
                        <td width="11%" class="standar">NSS:</td>
                        <td width="19%" class="standar"><input type="text" name="serie42" size="15" maxlength="20" class="input" value=""></td>
                      </tr>
                      <tr> 
                        <td class="standar">Nombre:</td>
                        <td class="standar"><input type="text" name="serie5" size="20" maxlength="50" class="input" value=""></td>
                        <td class="standar">1 Apellido:</td>
                        <td colspan="2" class="standar"><input type="text" name="serie23" size="20" maxlength="50" class="input" value=""></td>
                        <td class="standar">2 Apellido:</td>
                        <td class="standar"><input type="text" name="serie22" size="20" maxlength="50" class="input" value=""></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td colspan="6" class="standar"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr> 
                        <td width="21%" class="standar">F.Nacimiento desde:</td>
                        <td width="20%"><table width="100%" border="0">
                            <tr> 
                              <td width="42%"><input name="txtFDesde" type="text" class="input" id="txtFDesde" size="12" maxlengh="10"></td>
                              <td width="58%"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image20','','imagenes/calendario_sel.gif',1)"><img src="imagenes/calendario.gif" alt="Calendario" name="Image20" width="16" height="16" border="0" onClick="MM_callJS('accionFechaDesde()')"></a></td>
                            </tr>
                          </table></td>
                        <td width="21%" class="standar">F.Nacimiento hasta:</td>
                        <td width="38%"><table width="100%" border="0">
                            <tr> 
                              <td width="33%"><input name="txtFHasta" type="text" class="input" id="txtFHasta" size="12" maxlengh="10"></td>
                              <td width="67%"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image201','','imagenes/calendario_sel.gif',1)"><img src="imagenes/calendario.gif" alt="Calendario" name="Image201" width="16" height="16" border="0" id="Image201" onClick="MM_callJS('accionFechaHasta()')"></a></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td class="standar"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                      <tr> 
                        <td width="13%"  class="standar">Municipio:</td>
                        <td width="33%"  class="standar"> <select name="select" class="input">
							<option lastvalue="0"> 
                            <option lastvalue="1"> Abegondo 
                            <option lastvalue="2"> Ames 
                            <option lastvalue="3"> Aranga 
                            <option lastvalue="4"> Ares 
                            <option lastvalue="5"> Arteixo 
                            <option lastvalue="6"> Arzua 
                            <option lastvalue="7"> A Bana 
                            <option lastvalue="8"> Bergondo 
                            <option lastvalue="9"> Betanzos 
                            <option lastvalue="10"> Boimorto 
                            <option lastvalue="11"> Boiro 
                            <option lastvalue="12"> Boqueixon 
                            <option lastvalue="13"> Brion 
                            <option lastvalue="14"> Cabana 
                            <option lastvalue="15"> Cabanas 
                            <option lastvalue="16"> Camarinas 
                            <option lastvalue="17"> Cambre 
                            <option lastvalue="18"> A Capela 
                            <option lastvalue="19"> Carballo 
                            <option lastvalue="20"> Carino 
                            <option lastvalue="21"> Carnota 
                            <option lastvalue="22"> Carral 
                            <option lastvalue="23"> Cedeira 
                            <option lastvalue="24"> Cee 
                            <option lastvalue="25"> Cerceda 
                            <option lastvalue="26"> Cerdido 
                            <option lastvalue="27"> Cesuras 
                            <option lastvalue="28"> Coiros 
                            <option lastvalue="29"> Corcubion 
                            <option lastvalue="30"> Coristanco 
                            <option lastvalue="31"> A Coruna 
                            <option lastvalue="32"> Culleredo 
                            <option lastvalue="33"> Curtis 
                            <option lastvalue="34"> Dodro 
                            <option lastvalue="35"> Dumbria 
                            <option lastvalue="36"> Fene 
                            <option lastvalue="37"> Ferrol 
                            <option lastvalue="38"> Fisterra 
                            <option lastvalue="39"> Frades 
                            <option lastvalue="40"> Irixoa 
                            <option lastvalue="41"> Laracha 
                            <option lastvalue="42"> Laxe 
                            <option lastvalue="43"> Lousame 
                            <option lastvalue="44"> Malpica 
                            <option lastvalue="45"> Manon 
                            <option lastvalue="46"> Mazaricos 
                            <option lastvalue="47"> Melide 
                            <option lastvalue="48"> Mesia 
                            <option lastvalue="49"> Mino 
                            <option lastvalue="50"> Moeche 
                            <option lastvalue="51"> Monfero 
                            <option lastvalue="52"> Mugardos 
                            <option lastvalue="53"> Muros 
                            <option lastvalue="54"> Muxia 
                            <option lastvalue="55"> Naron 
                            <option lastvalue="56"> Neda 
                            <option lastvalue="57"> Negreira 
                            <option lastvalue="58"> Noia 
                            <option lastvalue="59"> Oleiros 
                            <option lastvalue="60"> Ordes 
                            <option lastvalue="61"> Oroso 
                            <option lastvalue="62"> Ortigueira 
                            <option lastvalue="63"> Outes 
                            <option lastvalue="64"> Oza Dos Rios 
                            <option lastvalue="65"> Paderne 
                            <option lastvalue="66"> Padron 
                            <option lastvalue="67"> O Pino 
                            <option lastvalue="68"> Pobra Do Caraminal 
                            <option lastvalue="69"> Ponteceso 
                            <option lastvalue="70"> Pontedeume 
                            <option lastvalue="71"> As Pontes 
                            <option lastvalue="72"> Porto Do Son 
                            <option lastvalue="73"> Rianxo 
                            <option lastvalue="74"> Ribeira 
                            <option lastvalue="75"> Rois 
                            <option lastvalue="76"> Sada 
                            <option lastvalue="77"> San Sadurnino 
                            <option lastvalue="78"> Santa Comba 
                            <option lastvalue="79"> Santiago 
                            <option lastvalue="80"> Santiso 
                            <option lastvalue="81"> Sobrado 
                            <option lastvalue="82"> Somozas 
                            <option lastvalue="83"> Teo 
                            <option lastvalue="84"> Toques 
                            <option lastvalue="85"> Tordoia 
                            <option lastvalue="86"> Touro 
                            <option lastvalue="87"> Trazo 
                            <option lastvalue="88"> Val Do Dubra 
                            <option lastvalue="89"> Valdovino 
                            <option lastvalue="90"> Vedra 
                            <option lastvalue="91"> Vilarmaior 
                            <option lastvalue="92"> Vilasantar 
                            <option lastvalue="93"> Vimianzo 
                            <option lastvalue="94"> Zas </select> </td>
                        <td width="24%"  class="standar">Provincia:</td>
                        <td width="30%" ><select name="select" class="input">
							<option lastvalue="0">
                            <option lastvalue="1"> A Coru�a 
                            <option lastvalue="2"> Lugo 
                            <option lastvalue="3"> Ourense 
                            <option lastvalue="4"> Pontevedra </select></td>
                      </tr>
                    </table></td>
                  <td width="19%"  colspan="5" align="right"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','imagenes/bot_buscar_sel.gif',1)"><img src="imagenes/bot_buscar.gif" alt="Buscar" name="Image17" width="67" height="19" border="0"></a></td>
                </tr>
              </table>
        </td>
        <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3">&nbsp;</td>
      </tr>
    <tr height="1"> 
            <td height="1" colspan="3" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
    </tr>
          <tr> 
            <td width="1" rowspan="2" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
            <td > <table width="100%">
                <tr> 
                  <td class="titulo" align="left" width="18%">NHC</td>
                  <td width="35%" align="left" class="titulo">Nombre y Apellidos</td>
                  <td width="47%" align="left" class="titulo">Fecha Nacimiento</td>
                </tr>
              </table>
			</td>
            <td width="1" rowspan="2" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
          </tr>
		  <tr> 
            <td class="standar" colspan="2" bgcolor="#002969"></td>
          </tr>
          <tr> 
		  	<td class="standar" bgcolor="#002969" width="1"></td>
            <td > <iframe id="iframe1"    marginheight="0" marginwidth="0" width="100%" scrolling="yes" src="datosPacientes.jsp"></iframe> 
            </td>
			<td class="standar" bgcolor="#002969" width="1"></td>
          </tr>
		  <tr> 
            <td class="standar" colspan="3" bgcolor="#002969"></td>
          </tr>
		  <tr> 
		   <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td> 
            <td valign="top" align="right">
			 <table border="0" cellspacing="0" cellpadding="3" align="center" width="100%">
			 <tr>
                  <td align="right"> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','imagenes/bot_aceptar_sel.gif',1)"><img src="imagenes/bot_aceptar.gif" alt="Aceptar" name="Image18" width="67" height="19" border="0" onClick="MM_callJS('accionAceptar()')"></a>&nbsp; 
                    <a href="#" onClick="MM_callJS('accionCancelar()')" onMouseOver="MM_swapImage('Image19','','imagenes/bot_cancelar_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/bot_cancelar.gif" alt="Cancelar" name="Image19" width="67" height="19" border="0"></a>&nbsp; 
                  </td>
                </tr></table>
           </td>
		   <td width="1" align="right" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
           </tr>
          <tr height="1"> 
            <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
          </tr>
        </table>
  </form>
	  </td>
      <td width="25" bgcolor="#DAE4F2"></td>
    </tr>
    <tr>
      <td width="25">
	  	  <img src="imagenes/Circunfabi-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
      <td width="100%" bgcolor="#DAE4F2"></td>
      <td height="24">
	  	  <img src="imagenes/Circunfabd-p.gif" border="0" width="25" height="100%" alt=""> 
	  </td>
    </tr>
</table>
</HTML>
