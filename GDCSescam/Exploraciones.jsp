<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Exploraciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>
<link rel="stylesheet" href="css/style_ex.css" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function accionCuerpo( localizacion ){
	//Actualizamos el contenido del iframe
	document.all.item("ifArbol").src = "javascript:accionCuerpo('" + localizacion + "')";
}

function accionAceptar(){
	alert( "Registrando la exploración..." );
	window.close();
}

function accionCancelar(){
	window.close();
}

//-->
</script>

<body topmargin="16" leftmargin="16" rightmargin="0" onLoad="MM_preloadImages('imagenes/bot_aceptar_sel.gif','imagenes/bot_cancelar_sel.gif')">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
      
    <td width="25" height="24" ><img src="imagenes/Circunfai-p.gif" border="0" width="25" height="100%" alt=""> 
    </td>
      <td height="24" width="100%" bgcolor="#DAE4F2"></td>
      <td width="25" height="24">
	  	  <img src="imagenes/Circunfad-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
    </tr>
    <tr>
      <td width="25" bgcolor="#DAE4F2"></td>
      <td width="100%" bgcolor="#DAE4F2">
	  
<table width="750" height="200" border="0" cellspacing="0" cellpadding="0" bgcolor="#DAE4F2">
        <tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
	<tr height="20">
 	    <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
		<td align="center" class="titulo"></td>
		<td align="center" class="titulo">ARBOL DE EXPLORACIONES</td>
 	    <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
	</tr>
    <tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
	<tr>
	  <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
	      <td width="300" align="left"> <img src="imagenes/MapaCuerpo.jpg" width="299" height="190" border="0" usemap="#Map"> 
            <map name="Map">
              <area shape="rect" coords="41,113,94,131" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Genitales\&quot;)')">
              <area shape="circle" coords="67,21,17" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Cabeza\&quot;)')">
              <area shape="circle" coords="158,108,17" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Cabeza\&quot;)')">
              <area shape="circle" coords="239,32,17" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Cabeza\&quot;)')">
              <area shape="rect" coords="40,89,94,112" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Estomago\&quot;)')">
              <area shape="rect" coords="212,120,265,138" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Genitales\&quot;)')">
              <area shape="rect" coords="142,147,172,156" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Genitales\&quot;)')">
              <area shape="rect" coords="212,96,266,119" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Estomago\&quot;)')">
              <area shape="rect" coords="30,43,106,87" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Torax\&quot;)')">
              <area shape="rect" coords="138,137,176,147" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Estomago\&quot;)')">
              <area shape="rect" coords="201,50,277,95" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Torax\&quot;)')">
              <area shape="rect" coords="125,126,190,137" href="#" target="_self" onClick="MM_callJS('accionCuerpo(\&quot;Torax\&quot;)')">
            </map></td>
 	  <td>
	  	<iframe name="ifArbol" id="ifArbol" src="ArbolExploraciones.jsp" width="100%" height="100%">
		</iframe>
	  </td>
 	  <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
	</tr>
	<tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
    <tr height="30">
 	    <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
		<td colspan="2" align="right">
			<table cellspacing="0" cellpadding="0">
				<tr>
			          
                <td width="75"><a href="#" onClick="MM_callJS('accionAceptar()')" onMouseOver="MM_swapImage('Image16','','imagenes/bot_aceptar_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/bot_aceptar.gif" alt="Aceptar los cambios" name="Image16" width="67" height="19" border="0"></a></td>
          			  
                <td width="75"><a href="#" onClick="MM_callJS('accionCancelar()')" onMouseOver="MM_swapImage('Image17','','imagenes/bot_cancelar_sel.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="imagenes/bot_cancelar.gif" alt="Cancelar los cambios" name="Image17" width="67" height="19" border="0"></a></td>
				</tr>
			</table>
		</td>
 	    <td bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="100%" width="100%"></td>
    </tr>
    <tr height="1">
 	    <td bgcolor="#002969" colspan="4"><img src="imagenes/shim.gif" border="0" height="1" width="100%"></td>
    </tr>
</table>

	  </td>
      <td width="25" bgcolor="#DAE4F2"></td>
    </tr>
    <tr>
      <td width="25">
	  	  <img src="imagenes/Circunfabi-p.gif" border="0" width="25" height="100%" alt="">
	  </td>
      <td width="100%" bgcolor="#DAE4F2"></td>
      <td height="24">
	  	  <img src="imagenes/Circunfabd-p.gif" border="0" width="25" height="100%" alt=""> 
	  </td>
    </tr>
</table>
</body>
</html>
