
//Con esta funci�n le podremos pasar cualquier cadena con un n�mero de posiciones decimales
//y un n�mero de posiciones enteras de modo que compruebe si el n�mero es correcto
function validaDatoDecimal(participacion, enteros, decimales)
{
	var posicionComa;
	var posicionUltimaComa;
	var primerDecimal;
	var parteDecimal;
	var numMinimoCaracteres = 0;


        if (participacion != null) {
           posicionComa = participacion.indexOf(".");
           posicionUltimaComa = participacion.lastIndexOf(".");

           // Comprueba que no haya �nicamente coma o menos o ambos
           if (posicionComa != -1)
                {
                numMinimoCaracteres++;
                }
           if ((participacion.length == numMinimoCaracteres) && (participacion.length != 0))
            {
                alert("ERROR: No puede introducir �nicamente un punto.");
                return false;
            }

                // Comprobaci�n de enteros y decimales
          if (posicionComa != posicionUltimaComa)
            {
                alert("ERROR: Solo esta permitido un punto");
                return false;
               }
          else if (posicionComa == -1)
            {
                if (participacion.length > enteros)
                {
                        alert("ERROR: Parte real mayor de lo permitido.");
                        return false;
                }
            }
            else
              {
                if (posicionComa > enteros)
                {
                        alert("ERROR: Parte real mayor de lo permitido.");
                        return false;
                }
                primerDecimal=parseInt(posicionComa)+1;
                parteDecimal = participacion.substring(parseInt(primerDecimal));
                if (parteDecimal.length > decimales)
                {
                        alert("ERROR: Parte decimal mayor de lo permitido.");
                        return false;
                }
              }
        }
	return true;

}


function validaNumero(numero) {
   var num = "0123456789+-.";  // N�meros v�lidos

        /* Ciclar sobre el contenido restante. Si alg�n car�cter no es un n�mero,
      establecer false como valor de vuelta. */
   for (var intLoop = 0; intLoop < numero.length; intLoop++){
           if ( num.indexOf(numero.charAt(intLoop)) == -1 ) {
                     return false;
             }
   }
   return true;
}

/*
        Comprueba si una cadena de texto es un n�mero v�lido
*/
function esNumero(texto) {

        if( !validaNumero(texto) ){
                return false;
        }
        return (!isNaN(parseInt(texto, 10)));
}
