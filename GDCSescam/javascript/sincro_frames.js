leftFrame = parent.frames.datos_left; // enlace a los frames
topFrame = parent.frames.cabecera_right;

if (document.all) onscroll = keepTogether;
else onload = checkScroll;

function keepTogether(){
  leftFrame.document.body.scrollTop =
                       document.body.scrollTop;
  topFrame.document.body.scrollLeft =
                       document.body.scrollLeft;
}

function checkScroll() {
  if (leftFrame.scrollbars.visible) {
    setInterval("scrollFrame()",10);
  }
  else {
    setInterval("scrollLayer()",10);
  }
}

function scrollFrame() {
  leftFrame.scrollTo(leftFrame.pageXOffset,pageYOffset);
  topFrame.scrollTo(pageXOffset,topFrame.pageYOffset);
}

function scrollLayer() {
  topFrame.document.layers[0].left = -pageXOffset;
  leftFrame.document.layers[0].top = -pageYOffset;
}

