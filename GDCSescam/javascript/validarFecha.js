function probarFechaString(Str_Fecha){
    
      var int_Dia;
      var int_Mes;
      var int_Anno;
     
      if ((Str_Fecha=="") || (Str_Fecha==null) || enBlanco(Str_Fecha))
      {
      	return true;
      }
      else
      {
	      if (esStringFecha(Str_Fecha))
	      {
		  	int_Dia = getDia(Str_Fecha);
	   	  	int_Mes = getMes(Str_Fecha);
	 	  	int_Anno = getAnno(Str_Fecha);
	        if (esFechaCorrecta(int_Dia, int_Mes, int_Anno))
	        {
				return true;	
	        }
	        else
	        {
		  		alert("ERROR: La fecha no es correcta.");
		  		return false;
	        }
	      }
	      else
	      {
		  	alert("ERROR: formato de fecha incorrecto.");
		  	return false;
	      }
	 }
}

      
function esFechaCorrecta(int_Dia, int_Mes, int_Anno){
      
      var boo_Return = true;       

        if (int_Dia >= 0 && int_Dia <=31){
          switch (int_Mes){
	    case 4:
	    case 6:
	    case 9:
	    case 11:
	      if (int_Dia == 31){
		boo_Return = false;
	        }
	      break;
	    case 2:
	      if (int_Dia >= 30){
		boo_Return = false;
	        }	
	      else{
		if (int_Dia == 29){
		  if (!esAnnoBisiesto(int_Anno)){
		    boo_Return = false;
		    }
		  }
		}
	      break;
	    case 1:
	    case 3:
	    case 5:
	    case 7:
	    case 8:
	    case 10:
	    case 12:
	      break;
	    default:
	      boo_Return = false;
	      break;
            }
          }
        else{
          boo_Return = false;
          }

      return boo_Return;
}

function esAnnoBisiesto(int_Anno){

      var boo_Return = false;

      if ((int_Anno % 4) == 0){
	if ((int_Anno % 100) == 0){
	  if ((int_Anno % 400) == 0){
	    boo_Return = true;
	    }
	  }
	else{
	  boo_Return = true;	
	  }
        }

      return boo_Return;
}

function esStringFecha(Str_Cadena){

	var boo_Return = true;
	var byt_Contador;
	var byt_NumLetras = 0;
    var byt_Campo = 0;
    var cha_Letra;
    
    if (Str_Cadena.length <= 10){ 

		for (byt_Contador = 0; byt_Contador < Str_Cadena.length; byt_Contador++){
	    	cha_Letra = Str_Cadena.charAt(byt_Contador);
	        if (cha_Letra >= '0' && cha_Letra <= '9'){
	        	byt_NumLetras = byt_NumLetras + 1;
	        }
	        else{
				if (cha_Letra == '/'){
	            	if (byt_Campo == 0){
	                	if (byt_NumLetras == 1 || byt_NumLetras ==2){        
	                  		byt_Campo = byt_Campo + 1;
	                  		byt_NumLetras = 0;
	                  	}
	                	else{
	                  		boo_Return = false;
	                  		break;
	                  	}  
	                }
	              	else {
	                	if (byt_Campo == 1){
	                  		if (byt_NumLetras == 1 || byt_NumLetras ==2){
	                    		byt_Campo = byt_Campo + 1;
	                    		byt_NumLetras = 0;
	                    	}
	                  		else{
	                    		boo_Return = false;
	                    		break;
	                    	}
	                  	}
	                	else{
	                  		if (byt_Campo == 2){
	                    		boo_Return = false;
	                    	}
	                  		break;
	                  	}
					}
				}
	        	else{
	              boo_Return = false;
	              break;
	            }
			}
		}
		if ((byt_NumLetras != 4) || (byt_Campo <2)){
			boo_Return = false;
		} 
	}
	else{
		boo_Return = false;
	}
    return boo_Return;
} 

function getDia(Str_Fecha){

      var int_Return = 0;
      var cha_Letra;

      cha_Letra = Str_Fecha.charAt(0);
      int_Return = parseInt(cha_Letra - '0');
      cha_Letra = Str_Fecha.charAt(1);
      if (cha_Letra >= '0' && cha_Letra <= '9'){
        int_Return = int_Return * 10 + parseInt(cha_Letra - '0');
        }

      return int_Return;
}

function getMes(Str_Fecha){

      var int_Return = 0;
      var int_Posicion;
      var cha_Letra;

      int_Posicion = Str_Fecha.indexOf('/',0) + 1;
      cha_Letra = Str_Fecha.charAt(int_Posicion);
      int_Return = parseInt(cha_Letra - '0');
      cha_Letra = Str_Fecha.charAt(int_Posicion + 1);
      if (cha_Letra >= '0' && cha_Letra <= '9'){
        int_Return = int_Return * 10 + parseInt(cha_Letra - '0');
        }

      return int_Return;
}

function getAnno(Str_Fecha){

      var int_Return = 0;
      var int_Posicion;
      var cha_Letra;

      int_Posicion = Str_Fecha.indexOf('/',0) + 1;
      int_Posicion = Str_Fecha.indexOf('/',int_Posicion) + 1;
      for ( ; int_Posicion < Str_Fecha.length; int_Posicion++){
        cha_Letra = Str_Fecha.charAt(int_Posicion);
        int_Return = int_Return * 10 + parseInt(cha_Letra - '0');
        }

      return int_Return;
}

function enBlanco(cadena)
  {
     resp = true;
     if (cadena.length>0){
        for (i=0;i<cadena.length;i++){
           caracter = cadena.substr(i,1);
           if (caracter != " "){
              resp = false;
           }
        }
     }else{
        resp = true;
     }
     return resp;
  }
  
  
  // Valida una fecha dada
	function validacion_fecha (dia, mes, anio)
	{
		var meses = new Array(31,29,31,30,31,30,31,31,30,31,30,31);
 
   	if (mes > 12 ){ 
			return false;
		}
   	if (dia > meses[mes-1] || dia < 1){ 
			return false;
		}
		// Validacion del a�o bisiesto
 		if ( (mes==2)  && (dia==29) ){
    			if ( ((anio % 4 == 0) && (anio % 100 != 0)) || 
        			(anio % 400 == 0) ){
             		return true;
				}
        		else{
					return false;
				} 
    	}
		return true;
   }   
   
   // Maximo numero de dias para un mes determinado.
   function maxDiasMes(mes,anio){
   	
   	var meses = new Array(31,29,31,30,31,30,31,31,30,31,30,31);
 
   	if (mes > 12 ){ 
			return -1;
		}
		
		if (mes==2){
			// Validacion del a�o bisiesto
			if ( ((anio % 4 == 0) && (anio % 100 != 0)) || 
        		(anio % 400 == 0) ){
            		return 29;
			}
        	else{
      		return 28;
			} 
    	}
    	
    	return meses[mes-1];
   }