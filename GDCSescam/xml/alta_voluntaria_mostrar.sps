<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="alta_voluntaria.xsd" workingxmlfile="alta_voluntaria_mostrar.xml" templatexmlfile="">
	<nspair prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<template>
				<match match="alta_voluntaria"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<text fixtext="Historia Nº ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="historia"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<newline/>
							<paragraph paragraphtag="center">
								<children>
									<text fixtext="ALTA VOLUNTARIA">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="large"/>
									</text>
								</children>
							</paragraph>
							<newline/>
							<newline/>
							<text fixtext="Con esta fecha se da el alta en este Hospital a:">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
							<table>
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties width="120"/>
														<children>
															<text fixtext="D.">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
															<text fixtext=" ">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																<match match="paciente"/>
																<children>
																	<xpath allchildren="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</xpath>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="120"/>
														<children>
															<text fixtext="Cartilla nº: ">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																<match match="cartilla"/>
																<children>
																	<xpath allchildren="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</xpath>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="120"/>
														<children>
															<text fixtext="Beneficiario nº:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																<match match="beneficiario"/>
																<children>
																	<xpath allchildren="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</xpath>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
							<text fixtext=" a petición de (indiquese parentesco y nombre) : ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="peticion"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<text fixtext="en contra de la opinión del médico que suscribe, que estima debe continuar la hospitalización por:">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="continuar_hospitalizacion"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<newline/>
							<newline/>
							<text fixtext="y habiéndose advertido al peticionario que la Seguridad Social queda exenta de responsabilidad por las consecuencias que se deriven de esta Alta, así como se le han hecho presentes los perjuicios que puedan irrogarse al enfermo.">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
							<newline/>
							<text fixtext="   Y para que conste, se extiende este documento en">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="lugar"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<text fixtext="a">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="dia"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<text fixtext="de">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="mes"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<text fixtext="de">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<template>
								<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
								<match match="año"/>
								<children>
									<xpath allchildren="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
									</xpath>
								</children>
							</template>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<text fixtext="por triplicado ejemplar.">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
						</children>
					</paragraph>
					<newline/>
					<template>
						<match match="firmas"/>
						<children>
							<paragraph paragraphtag="p">
								<children>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="El peticionario:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="peticionario"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="El jefe de la Clínica:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="jefe_clinica"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="El director:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																		<match match="director"/>
																		<children>
																			<xpath allchildren="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</xpath>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
								</children>
							</paragraph>
						</children>
					</template>
					<newline/>
					<newline/>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
