<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="informe_endoscopia.xsd" workingxmlfile="informe_endoscopia.xml" templatexmlfile="">
	<nspair prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<template>
				<match match="informe_endoscopia"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<paragraph paragraphtag="center">
								<children>
									<text fixtext="SECCION DE ENDOSCOPIA DIGESTIVA">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="large" font-weight="bold"/>
									</text>
								</children>
							</paragraph>
							<newline/>
							<newline/>
							<template>
								<match match="datos_paciente"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<children>
																	<tablecol>
																		<properties width="142"/>
																		<children>
																			<text fixtext="Primer apellido:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties colspan="3"/>
																		<children>
																			<template>
																				<match match="apellido1"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="50"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties width="142"/>
																		<children>
																			<text fixtext="Segundo apellido:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties colspan="3"/>
																		<children>
																			<template>
																				<match match="apellido2"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="50"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties width="142"/>
																		<children>
																			<text fixtext="Nombre:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties colspan="3"/>
																		<children>
																			<template>
																				<match match="nombre"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="50"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties width="142"/>
																		<children>
																			<text fixtext="Edad:">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties colspan="3"/>
																		<children>
																			<template>
																				<match match="edad"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties value=""/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties width="142"/>
														<children>
															<template>
																<match match="ambulatorio"/>
																<children>
																	<checkbox ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties type="checkbox"/>
																	</checkbox>
																	<text fixtext="Ambulatorio">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</template>
														</children>
													</tablecol>
													<tablecol>
														<properties width="133"/>
														<children>
															<template>
																<match match="ingresado"/>
																<children>
																	<checkbox ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties type="checkbox"/>
																	</checkbox>
																</children>
															</template>
															<text fixtext=" ">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
															</text>
															<text fixtext="Ingresado">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<properties width="68"/>
														<children>
															<text fixtext="Hab:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="habitacion"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="142"/>
														<children>
															<text fixtext="Exploración solicitada:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<properties colspan="3" width="133"/>
														<children>
															<template>
																<match match="exploracion_solicitada"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="142"/>
														<children>
															<text fixtext="Doctor:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<properties colspan="3" width="133"/>
														<children>
															<template>
																<match match="doctor"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="142"/>
														<children>
															<text fixtext="Servicio:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<properties colspan="3" width="133"/>
														<children>
															<template>
																<match match="servicio"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="142"/>
														<children>
															<text fixtext="Fecha:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<properties width="133"/>
														<children>
															<template>
																<match match="fecha_pedido"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
													<tablecol>
														<properties colspan="2" width="68"/>
														<children>
															<template>
																<match match="urgente"/>
																<children>
																	<checkbox ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties type="checkbox"/>
																	</checkbox>
																	<text fixtext="Urgente">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																	</text>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
							<newline/>
							<text fixtext="Datos clínicos:">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
							<template>
								<match match="datos_clinicos"/>
								<children>
									<multilinefield ownvalue="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
										<properties cols="75" rows="5"/>
									</multilinefield>
								</children>
							</template>
							<newline/>
							<newline/>
							<text fixtext="Hipótesis diagnóstica:">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<newline/>
							<template>
								<match match="hipotesis"/>
								<children>
									<multilinefield ownvalue="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
										<properties cols="75" rows="5"/>
									</multilinefield>
								</children>
							</template>
							<newline/>
							<newline/>
							<template>
								<match match="informes"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties align="center" bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties colspan="2"/>
																		<children>
																			<text fixtext="INFORME ENDOSCÓPICO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Foto1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="informe1"/>
																				<children>
																					<multilinefield ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties cols="75" rows="5"/>
																					</multilinefield>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Foto2">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="informe2"/>
																				<children>
																					<multilinefield ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties cols="75" rows="5"/>
																					</multilinefield>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Foto3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="informe3"/>
																				<children>
																					<multilinefield ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties cols="75" rows="5"/>
																					</multilinefield>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Foto4">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<children>
																			<template>
																				<match match="informe4"/>
																				<children>
																					<multilinefield ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties cols="75" rows="5"/>
																					</multilinefield>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<newline/>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
