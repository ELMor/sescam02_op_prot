<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="resultados_laboratorio.xsd" workingxmlfile="resultados_laboratorio.xml" templatexmlfile="">
	<nspair prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<template>
				<match match="resultados_laboratorio"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<template>
								<match match="hemograma"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" table-layout="fixed" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties align="left" bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5"/>
																		<children>
																			<text fixtext="HEMOGRAMA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LEUCOS.Formula">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="leucos.formula"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="x10e3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="HEMATIES.Formula">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="hematies.formula"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="x10e6">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="HEMOGLOBINA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="hemoglobina"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="HEMATOCRITO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="hematocrito"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="VCM">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="vcm"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="fL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MCH">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="mch"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="pg">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MCHC">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="mchc"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IDH">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="idh"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IDHb">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="idhb"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="PLAQUETAS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="plaquetas"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="x10e3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="VPM">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="vpm"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="fL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IDP">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="idp"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="Hem. microciticos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="hem.microciticos"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.0 - 2.5">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="Hem. hipocromicos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="hem.hipocromicos"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.0 - 4.0">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="formula"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="FORMULA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="NEUTRO%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="neutro"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="NEUAB">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="neuab"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LINFO%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="linfos"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LINA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="lina"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MONOS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="monos"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MONOSA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="monosa"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="EOSINO%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="eosino"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="EOSA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="eosa"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="BASOFILOS%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="basofilos"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="BASA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="basa"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LUC%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="luc"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LUCA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="luca"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="il"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MPXI">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="mpxi"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="VSG">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="vsg"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mm">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="5 - 15">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TQ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="tq"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TTPA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext="L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="ttpa"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="seg">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="23.0 - 33.0">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="FIBRINOGENO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="fibrinogeno"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="150 - 400">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="pt"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="PT+Albu+Gto+Coc A/B">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="P.TOTALES">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="p.totales"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="6.5 - 8.3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="ALBUMINA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="albumina"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="4.0 - 5.2">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="Globulina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="globulina"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="ALB/GLOB">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="alb_glob"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GLUCOSA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="glucosa"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="74 - 105">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="UREA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext="H">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="urea"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="12 - 44">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="creatinina"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Creatina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="CREATINA suero">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="creatina_suero"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.4 - 1.3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="URICO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="urico"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="2.4 - 7.0">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="grupo_bil"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Grupo bil. Total">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="BILT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="bilt"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.20 - 1.20">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="colesterol_trigliceridos"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Colest + Triglic">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="COLESTEROL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="colesterol"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="120 - 254">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TRIGLICERIDOS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext="H">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="trigliceridos"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="25 - 150">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="got_gpt_ggt"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="GOT, GPT, GGT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GOT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="got"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0 - 25">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GPT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="gpt"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0 - 29">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GGT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="ggt"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="5 -38">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="grupo_fal"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Grupo FAL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="F.ALCAL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="f.alcal"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="65 - 195">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="COLINESTERASA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="colinesterasa"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="3500 - 10500">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="iones"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="IONES">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="NA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="na"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mM/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="137 - 148">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="K">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="k"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mM/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="3.6 - 4.9">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="CALCIO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="calcio"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="8.7 - 10.6">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TSH">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="tsh"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mUI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.35 - 5.50">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="T4 LIBRE">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="t4_libre"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="ng/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.89 - 1.80">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="T3 LIBRE">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" "/>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<editorproperties adding="mandatory" autoaddname="1" editable="0" markupmode="hide"/>
																				<match match="t3_libre"/>
																				<children>
																					<xpath allchildren="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</xpath>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="pg/mL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="2.30 - 4.20">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
			<newline/>
		</children>
	</template>
</structure>
