<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="informe_urgencias.xsd" workingxmlfile="informe_urgencias2.xml" templatexmlfile="">
	<nspair prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<template>
				<match match="Informe_urgencias"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<paragraph paragraphtag="center">
								<children>
									<text fixtext="UNIDAD DE URGENCIAS">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="large" font-weight="bold"/>
									</text>
								</children>
							</paragraph>
							<newline/>
							<newline/>
							<text fixtext="Viene por iniciativa propia">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
							</text>
							<text fixtext=" ">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
							</text>
							<template>
								<match match="iniciativa"/>
								<children>
									<checkbox ownvalue="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
										<properties type="checkbox"/>
									</checkbox>
								</children>
							</template>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties width="216"/>
														<children>
															<text fixtext="Enviado por (Doctor o Centro):">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="enviado_por"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="35" value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="216"/>
														<children>
															<text fixtext="Se requiere el Servicio(s) de:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="servicio"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="35" value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties width="216"/>
														<children>
															<text fixtext="Se avisa a:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="avisar_a"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="35" value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<properties align="left" bgcolor="#DAE4F2"/>
												<children>
													<tablecol>
														<children>
															<text fixtext="Datos Médicos">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<template>
								<match match="datos_medicos"/>
								<children>
									<newline/>
									<text fixtext="Motivación clínica: ">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
									</text>
									<template>
										<match match="motivacion_clinica"/>
										<children>
											<field ownvalue="1">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
												<properties size="50"/>
											</field>
										</children>
									</template>
									<newline/>
									<paragraph paragraphtag="p">
										<children>
											<text fixtext="1. Anamnesis:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="anamnesis"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<text>
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
											</text>
											<newline/>
											<newline/>
											<text fixtext="2. Exploración física:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="exploracion_fisica"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<table>
												<properties align="center" border="0" cellpadding="0" cellspacing="0" width="80%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#EFF3FA"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Datos complementarios">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
											<template>
												<match match="datos_complementarios"/>
												<children>
													<table dynamic="1" topdown="0">
														<properties align="center" border="0" cellpadding="0" cellspacing="0" width="80%"/>
														<children>
															<tablebody>
																<children>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Peso:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol dynamic="1">
																				<children>
																					<template>
																						<match match="peso"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties value=""/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Puls:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol dynamic="1">
																				<children>
																					<template>
																						<match match="puls"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties value=""/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Resp:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol dynamic="1">
																				<children>
																					<template>
																						<match match="resp"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties value=""/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Tª:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol dynamic="1">
																				<children>
																					<template>
																						<match match="temperatura"/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties value=""/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="T.A.:">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																					</text>
																				</children>
																			</tablecol>
																			<tablecol dynamic="1">
																				<children>
																					<template>
																						<match match="t.a."/>
																						<children>
																							<field ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																								<properties value=""/>
																							</field>
																						</children>
																					</template>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																</children>
															</tablebody>
														</children>
													</table>
												</children>
											</template>
											<newline/>
											<newline/>
											<text fixtext="3. Analítica:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="analitica"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<text fixtext="4. Radiología:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="radiologia"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<text fixtext="5. Otras exploraciones:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="otras_exploraciones"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<text fixtext="    Hipótesis diagnóstica:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="hipotesis_diagnostica"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<text fixtext="6. Tratamiento administrado en la Unidad:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="tratamiento_administrado"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<newline/>
											<text fixtext="    Tratamiento prescrito para el domicilio:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="tratamiento_prescrito"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<newline/>
											<text fixtext="    Otras disposiciones tomadas:">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="otras_disposiciones"/>
												<children>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
												</children>
											</template>
											<newline/>
											<newline/>
											<template>
												<match match="ingreso"/>
												<children>
													<checkbox ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties type="checkbox"/>
													</checkbox>
												</children>
											</template>
											<text fixtext=" Ingreso H.G.G. Traslado a: ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
											</text>
											<template>
												<match match="traslado"/>
												<children>
													<field ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties size="50"/>
													</field>
												</children>
											</template>
											<newline/>
											<newline/>
											<template>
												<match match="pasa_domicilio"/>
												<children>
													<checkbox ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties type="checkbox"/>
													</checkbox>
												</children>
											</template>
											<text fixtext=" Pasa a domicilio ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
											</text>
											<newline/>
											<newline/>
											<text fixtext="    Otras recomendaciones (alertas, etc...):">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<newline/>
											<template>
												<match match="recomendaciones"/>
												<children>
													<newline/>
													<multilinefield ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties cols="75" rows="3"/>
													</multilinefield>
													<newline/>
												</children>
											</template>
											<newline/>
											<newline/>
											<text fixtext="   ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
											</text>
											<text fixtext=" Próximo control en el servicio de: ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<template>
												<match match="proximo_control"/>
												<children>
													<field ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties size="50"/>
													</field>
												</children>
											</template>
											<newline/>
											<text fixtext="    ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
											</text>
											<newline/>
											<text fixtext="    ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
											</text>
											<text fixtext="Dr: ">
												<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
											</text>
											<template>
												<match match="doctor"/>
												<children>
													<field ownvalue="1">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
														<properties size="50"/>
													</field>
												</children>
											</template>
											<newline/>
											<newline/>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<newline/>
							<newline/>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
