<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="informe_radiologia.xsd" workingxmlfile="informe_radiologia.xml" templatexmlfile="">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<paragraph paragraphtag="center">
				<children>
					<text fixtext="SERVICIO DE RADIODIAGNOSTICO">
						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="large"/>
					</text>
				</children>
			</paragraph>
			<newline/>
			<newline/>
			<template>
				<match match="informe_radiologia"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<template>
								<match match="datos_paciente"/>
								<children>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#DAE4F2"/>
														<children>
															<tablecol>
																<properties align="left" colspan="2" width="140"/>
																<children>
																	<text fixtext="Datos paciente">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Nombre:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="nombre"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="50"/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Sexo:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="sexo"/>
																		<children>
																			<select ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="0"/>
																				<selectoption description="Hombre" value="hombre"/>
																				<selectoption description="Mujer" value="mujer"/>
																			</select>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Fecha de nacimiento:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="fecha_nacimiento"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="50"/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Edad:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="edad"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties value=""/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Historia clínica:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="historia_clinica"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="50"/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="T.I.S.">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="tis"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="50"/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
								</children>
							</template>
							<newline/>
							<template>
								<match match="datos_solicitante"/>
								<children>
									<newline/>
									<table>
										<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
										<children>
											<tablebody>
												<children>
													<tablerow>
														<properties bgcolor="#DAE4F2"/>
														<children>
															<tablecol>
																<properties align="left" colspan="2" width="140"/>
																<children>
																	<text fixtext="Datos médicos">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Médico solicitante:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="medico_solicitante"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="50"/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
													<tablerow>
														<children>
															<tablecol>
																<properties align="left" width="140"/>
																<children>
																	<text fixtext="Servicio solicitante:">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																	</text>
																</children>
															</tablecol>
															<tablecol>
																<children>
																	<template>
																		<match match="servicio_solicitante"/>
																		<children>
																			<field ownvalue="1">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																				<properties size="50"/>
																			</field>
																		</children>
																	</template>
																</children>
															</tablecol>
														</children>
													</tablerow>
												</children>
											</tablebody>
										</children>
									</table>
								</children>
							</template>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Exploraciones:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="exploraciones"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
							<paragraph paragraphtag="center">
								<children>
									<text fixtext="INFORME RADIOLOGICO">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="medium"/>
									</text>
								</children>
							</paragraph>
							<newline/>
							<newline/>
							<template>
								<match match="informe"/>
								<children>
									<multilinefield ownvalue="1">
										<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
										<properties cols="75" rows="5"/>
									</multilinefield>
								</children>
							</template>
							<newline/>
							<newline/>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Fecha diagnóstico:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="fecha_diagnostico"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Firmado:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="doctor"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
