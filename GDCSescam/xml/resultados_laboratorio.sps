<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="resultados_laboratorio.xsd" workingxmlfile="resultados_laboratorio.xml" templatexmlfile="">
	<nspair prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<newline/>
			<template>
				<match match="resultados_laboratorio"/>
				<children>
					<paragraph paragraphtag="p">
						<children>
							<template>
								<match match="hemograma"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" table-layout="fixed" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties align="left" bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5"/>
																		<children>
																			<text fixtext="HEMOGRAMA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LEUCOS.Formula">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="leucos.formula"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="x10e3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="HEMATIES.Formula">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="hematies.formula"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="x10e6">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="HEMOGLOBINA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="hemoglobina"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="HEMATOCRITO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="hematocrito"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="VCM">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="vcm"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="fL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MCH">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="mch"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="pg">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MCHC">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="mchc"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IDH">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="idh"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IDHb">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="idhb"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="PLAQUETAS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="plaquetas"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="x10e3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="VPM">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="vpm"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="fL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IDP">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="idp"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="Hem. microciticos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="hem.microciticos"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.0 - 2.5">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="Hem. hipocromicos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="hem.hipocromicos"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.0 - 4.0">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="formula"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="FORMULA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="NEUTRO%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="neutro"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="NEUAB">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="neuab"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LINFO%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="linfos"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LINA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="lina"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MONOS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																			<text fixtext="%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="monos"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MONOSA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="monosa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="EOSINO%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="eosino"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="EOSA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="eosa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="BASOFILOS%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="basofilos"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="BASA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="basa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LUC%">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="luc"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="LUCA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="luca"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="IL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="il"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="MPXI">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="mpxi"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="VSG">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="vsg"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mm">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="5 - 15">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TQ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="tq"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TTPA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="ttpa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="seg">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="23.0 - 33.0">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="FIBRINOGENO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="fibrinogeno"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="150 - 400">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="pt"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="PT+Albu+Gto+Coc A/B">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="P.TOTALES">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="p.totales"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="6.5 - 8.3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="ALBUMINA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="albumina"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="4.0 - 5.2">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="Globulina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="globulina"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="g/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="ALB/GLOB">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="alb_glob"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GLUCOSA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="glucosa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="74 - 105">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="UREA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="urea"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="12 - 44">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="creatinina"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Creatina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="CREATINA suero">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="creatina_suero"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.4 - 1.3">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="URICO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="urico"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="2.4 - 7.0">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="grupo_bil"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Grupo bil. Total">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="BILT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="bilt"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.20 - 1.20">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="colesterol_trigliceridos"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Colest + Triglic">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="COLESTEROL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="colesterol"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="120 - 254">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TRIGLICERIDOS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="trigliceridos"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="25 - 150">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="got_gpt_ggt"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="GOT, GPT, GGT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GOT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="got"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0 - 25">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GPT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="gpt"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0 - 29">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="GGT">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="ggt"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="5 -38">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="grupo_fal"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="Grupo FAL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="F.ALCAL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="f.alcal"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="65 - 195">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="COLINESTERASA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="colinesterasa"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="UI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="3500 - 10500">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
							<template>
								<match match="iones"/>
								<children>
									<paragraph paragraphtag="p">
										<children>
											<table>
												<properties border="1" border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<properties align="left" colspan="5" width="160"/>
																		<children>
																			<text fixtext="IONES">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="NA">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="na"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mM/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="137 - 148">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="K">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="k"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mM/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="3.6 - 4.9">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="CALCIO">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="calcio"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mg/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="8.7 - 10.6">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="TSH">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="tsh"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="mUI/L">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.35 - 5.50">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="T4 LIBRE">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="t4_libre"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="ng/dL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="0.89 - 1.80">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<properties align="left" width="160"/>
																		<children>
																			<text fixtext="T3 LIBRE">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="40"/>
																		<children>
																			<text fixtext=" ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="200"/>
																		<children>
																			<template>
																				<match match="t3_libre"/>
																				<children>
																					<field ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																						<properties size="4"/>
																					</field>
																				</children>
																			</template>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="pg/mL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																	<tablecol>
																		<properties align="center" width="135"/>
																		<children>
																			<text fixtext="2.30 - 4.20">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</paragraph>
								</children>
							</template>
							<newline/>
						</children>
					</paragraph>
				</children>
			</template>
			<newline/>
			<newline/>
		</children>
	</template>
</structure>
