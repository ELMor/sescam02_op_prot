<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:a="http://www.xmlspy.com/schemas/orgchart"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ipo="http://www.altova.com/IPO">
	<xsl:output method="xml" version="1.0" encoding="ISO-8859-1" omit-xml-declaration="no" indent="no" media-type="text/html"/>
	<!--XSL Stylesheet for generating simple Orgchart-->
	<xsl:template match="a:OrgChart">
		<html>
			<head>
				<title>
					<xsl:value-of select="a:Name"/>
				</title>
			</head>
			<body>
			<table width="100%">
				<tr>
					<td>
						<h1>
							<xsl:value-of select="a:Name"/>
						</h1>
					</td>
					<td align="right">
						<img alt="Logo">
							<xsl:attribute name="src">
								<xsl:value-of select="a:CompanyLogo/@href" />
							</xsl:attribute>
						</img>
					</td>
				</tr>
			</table>
			<xsl:for-each select="a:Office">
				<h2>
					<xsl:value-of select="a:Name"/>
				</h2>
				<table width="70%">
					<tr valign="top">
						<td width="60%">
							<xsl:apply-templates select="a:Address"/>
						</td>
						<td width="40%">
							<table>
								<tr>
									<td>
										<b><small>Phone:</small></b>
									</td>
									<td>
										<xsl:value-of select="a:Phone"/>
									</td>
								</tr>
								<tr>
									<td>
										<b><small>Fax:</small></b>
									</td>
									<td>
										<xsl:value-of select="a:Fax"/>
									</td>
								</tr>
								<tr>
									<td>
										<b><small>E-Mail:</small></b>
									</td>
									<td>
										<xsl:apply-templates select="a:EMail"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<small>
					<xsl:apply-templates select="a:Desc"/>
				</small>
				<table width="100%">
					<xsl:apply-templates select="a:Department"/>
				</table>
			</xsl:for-each>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="a:Address">
		<table>
			<tr>
				<td>
					<xsl:value-of select="ipo:street"/>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td>
					<xsl:value-of select="ipo:postcode"/>
				</td>
				<td>
					<xsl:value-of select="ipo:city"/>
				</td>
				<td>
					<xsl:value-of select="ipo:state"/>
				</td>
				<td>
					<xsl:value-of select="ipo:zip"/>
				</td>
			</tr>
		</table>
	</xsl:template>
	<xsl:template match="a:Department">
		<tr>
			<td width="16">
			</td>
			<td>
				<h3>
					<xsl:value-of select="a:Name"/>
				</h3>
				<table width="100%">
					<tr bgcolor="#C0C0C0">
						<td width="20%">
							<b>First</b>
						</td>
						<td width="20%">
							<b>Last</b>
						</td>
						<td width="30%">
							<b>Title</b>
						</td>
						<td width="5%">
							<b>Ext</b>
						</td>
						<td width="25%">
							<b>E-Mail</b>
						</td>
					</tr>
					<xsl:apply-templates select="a:Person|a:VIP"/>
				</table>
				<br/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="a:Person">
		<tr bgcolor="#E0E0E0">
			<td>
				<xsl:value-of select="a:First"/>
			</td>
			<td>
				<b>
					<xsl:value-of select="a:Last"/>
				</b>
			</td>
			<td>
				<xsl:value-of select="a:Title"/>
			</td>
			<td>
				<xsl:value-of select="a:PhoneExt"/>
			</td>
			<td>
				<xsl:apply-templates select="a:EMail"/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="a:EMail">
		<a>
			<xsl:attribute name="href">mailto:<xsl:value-of select="."/></xsl:attribute>
			<xsl:value-of select="."/>
		</a>
	</xsl:template>
	<xsl:template match="a:VIP">
		<tr bgcolor="yellow">
			<td>
				<xsl:value-of select="a:First"/>
			</td>
			<td>
				<b>
					<xsl:value-of select="a:Last"/>
				</b>
			</td>
			<td>
				<xsl:value-of select="a:Title"/>
			</td>
			<td>
				<xsl:value-of select="a:PhoneExt"/>
			</td>
			<td>
				<xsl:apply-templates select="a:EMail"/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="a:para">
		<p>
			<xsl:value-of select="."/>
		</p>
	</xsl:template>
</xsl:stylesheet>
