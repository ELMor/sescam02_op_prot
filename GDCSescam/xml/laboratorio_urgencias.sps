<?xml version="1.0" encoding="UTF-8"?>
<structure version="2" schemafile="laboratorio_urgencias.xsd" workingxmlfile="laboratorio-urgencias.xml" templatexmlfile="">
	<nspair prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
	<template>
		<match overwrittenxslmatch="/"/>
		<children>
			<newline/>
			<template>
				<match match="laboratorio_urgencias"/>
				<children>
					<newline/>
					<newline/>
					<paragraph paragraphtag="center">
						<children>
							<text fixtext="LABORATORIO URGENCIAS">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="large"/>
							</text>
						</children>
					</paragraph>
					<newline/>
					<newline/>
					<template>
						<match match="datos_paciente"/>
						<children>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<properties bgcolor="#DAE4F2"/>
												<children>
													<tablecol>
														<properties align="left" colspan="2" width="140"/>
														<children>
															<text fixtext="Datos paciente">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Nº tarjeta sanitaria:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="tarjeta_sanitaria"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Nº historia">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="num_historia"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Primer apellido:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="apellido1"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Segundo apellido:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="apellido2"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Nombre:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="nombre"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Fecha nacimiento:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="fecha_nacimiento"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="50"/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Sexo:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="sexo"/>
																<children>
																	<select ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties size="0"/>
																		<selectoption description="Hombre" value="hombre"/>
																		<selectoption description="Mujer" value="mujer"/>
																	</select>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
							<newline/>
						</children>
					</template>
					<newline/>
					<template>
						<match match="datos_pedido"/>
						<children>
							<newline/>
							<table>
								<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
								<children>
									<tablebody>
										<children>
											<tablerow>
												<properties bgcolor="#DAE4F2"/>
												<children>
													<tablecol>
														<properties align="left" colspan="2" width="140"/>
														<children>
															<text fixtext="Datos médicos">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Código de servicio:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="cod_servicio"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Código de médico:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="cod_medico"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Habitación:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="habitacion"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Cama:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="cama"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
											<tablerow>
												<children>
													<tablecol>
														<properties align="left" width="140"/>
														<children>
															<text fixtext="Control:">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
															</text>
														</children>
													</tablecol>
													<tablecol>
														<children>
															<template>
																<match match="ctrl"/>
																<children>
																	<field ownvalue="1">
																		<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																		<properties value=""/>
																	</field>
																</children>
															</template>
														</children>
													</tablecol>
												</children>
											</tablerow>
										</children>
									</tablebody>
								</children>
							</table>
						</children>
					</template>
					<newline/>
					<table>
						<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
						<children>
							<tablebody>
								<children>
									<tablerow>
										<children>
											<tablecol>
												<properties align="left" width="140"/>
												<children>
													<text fixtext="Código del extractor:">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<children>
													<template>
														<match match="cod_extractor"/>
														<children>
															<field ownvalue="1">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																<properties value=""/>
															</field>
														</children>
													</template>
												</children>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<properties align="left" width="140"/>
												<children>
													<text fixtext="Diuresis 24h (en ml):">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<children>
													<template>
														<match match="diuresis"/>
														<children>
															<field ownvalue="1">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																<properties value=""/>
															</field>
														</children>
													</template>
												</children>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<properties align="left" width="140"/>
												<children>
													<text fixtext="Semana gestación:">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<children>
													<template>
														<match match="semana_gestacion"/>
														<children>
															<field ownvalue="1">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																<properties value=""/>
															</field>
														</children>
													</template>
												</children>
											</tablecol>
										</children>
									</tablerow>
									<tablerow>
										<children>
											<tablecol>
												<properties align="left" width="140"/>
												<children>
													<text fixtext="Dia del ciclo:">
														<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
													</text>
												</children>
											</tablecol>
											<tablecol>
												<children>
													<template>
														<match match="dia_del_ciclo"/>
														<children>
															<field ownvalue="1">
																<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
																<properties value=""/>
															</field>
														</children>
													</template>
												</children>
											</tablecol>
										</children>
									</tablerow>
								</children>
							</tablebody>
						</children>
					</table>
					<newline/>
					<text fixtext="Orientación diagnóstico:">
						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
					</text>
					<newline/>
					<template>
						<match match="orientacion_diagnostico"/>
						<children>
							<multilinefield ownvalue="1">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
								<properties cols="75" rows="5"/>
							</multilinefield>
						</children>
					</template>
					<newline/>
					<newline/>
					<text fixtext="Tratamiento:">
						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
					</text>
					<newline/>
					<template>
						<match match="tratamiento"/>
						<children>
							<multilinefield ownvalue="1">
								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px"/>
								<properties cols="75" rows="5"/>
							</multilinefield>
						</children>
					</template>
					<newline/>
					<newline/>
					<template>
						<match match="pruebas_realizadas"/>
						<children>
							<paragraph paragraphtag="p">
								<children>
									<template>
										<match match="cardiocirculatorio"/>
										<children>
											<newline/>
											<paragraph paragraphtag="p">
												<children>
													<table>
														<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
														<children>
															<tablebody>
																<children>
																	<tablerow>
																		<properties bgcolor="#DAE4F2"/>
																		<children>
																			<tablecol>
																				<children>
																					<text fixtext="Cardiocirculatorio">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="insuficiencia_cardiaca"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Insuficiencia cardiaca">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="dolor_toracico"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Dolor toracico inespecifico">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="cardiopatia_isquemica_1"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Cardiopatia isquemica  menor 6h">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="cardiopatia_isquemica_2"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Cardiopatia isquemica  mayor 6h">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="pericarditis"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Pericarditis">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="schock"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Schock, fallo multiorganico">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="arritmias_cardiacas"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Arritmias cardiacas">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="parada_cardio_respiratoria"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Parada cardio-respiratoria">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="diseccion_aortica"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Diseccion aortica">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="isquemia_arterial"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Isquemia arterial periferica">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="trombosis_venosa_profunda"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Trombosis venosa profunda">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="tromboembolismo_pulmonar"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Tromboembolismo pulmonar">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="hipertension_arterial"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Hipertension arterial">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																	<tablerow>
																		<children>
																			<tablecol>
																				<children>
																					<template>
																						<match match="aneurisma"/>
																						<children>
																							<checkbox ownvalue="1">
																								<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																								<properties type="checkbox"/>
																							</checkbox>
																						</children>
																					</template>
																					<text fixtext=" Aneurisma aortico sintom.">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																					</text>
																				</children>
																			</tablecol>
																		</children>
																	</tablerow>
																</children>
															</tablebody>
														</children>
													</table>
												</children>
											</paragraph>
										</children>
									</template>
									<newline/>
									<template>
										<match match="respiratorio"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Respiratorio">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="asma"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Insuficiencia respi. aguda o cronica, Asma">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="esputo_hemoptoico"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Esputo hemoptoico">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hemoptisis"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Hemoptisis">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
										</children>
									</template>
									<newline/>
									<template>
										<match match="digestivo"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Digestivo">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hemorragia_digestiva"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Hemorragia digestiva">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="dolor_abdominal"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Dolor abdominal">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="abdomen_agudo"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Abdomen agudo">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="patologia_hepatica"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Patologia hepatica">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="pancreatitis_aguda"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Pancreatitis aguda">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="sindrome_diarreico"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Sindrome diarreico">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="transplante_hepatico"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Transplante hepatico">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="neurologia"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Neurologia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="accidente_vascular_cerebral"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Accidente vascular cerebral">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="cefalea"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Cefalea">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="coma_causa_no_fijada"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Coma de causa no fijada">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="crisis_convulsivas"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Crisis convulsivas">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="sindr_confusional_agudo"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Sindr. confusional agudo">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="enfermedades_infecciosas"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Enfermedades infecciosas">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="meningitis"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Meningitis, Infecc SNC">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="aids"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" AIDS">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="sepsis"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Sepsis">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="neumonia"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Neumonia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tuberculosis"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Tuberculosis">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="sindr_febril_no_fijado"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Sindr. febril no fijado">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="aparato_urinario"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Aparato urinario">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="insuficiencia_renal"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Insuf. renal aguda">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="colico_renal"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Colico renal">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="retencion_urinaria"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Retencion urinaria">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="hematologia"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Hematologia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="amenia"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Amenia, leucopenia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="trombocitopenia"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Trombocitopenia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="control_hemoglobina"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Control de hemoglobina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="conflicto_rh"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Conflicto Rh materno-fetal">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="endocrinologia"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Endocrinologia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="diabetes_descompensada"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Diabetes descompensada">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hipoglucemia"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Hipoglucemia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="orl"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="ORL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="epistaxis"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Epistaxis">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="infecciones_agudas"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Infecciones agudas ORL">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="alergias"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Alergias">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="reaccion_alergica_severa"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Reaccion alergica severa">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="enfermedades_dermatologicas"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Enfermedades dermatologicas">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="entrodermia"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Entrodermia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="intoxicaciones"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Intoxicaciones">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="etilismo_agudo"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Etilismo agudo">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="etilismo_cronico"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Etilismo cronico">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="sindr_de_privacion"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Sindr. de privacion">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="neurocirugia"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Neurocirugia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="estandar_neurocirugia"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Estandar neurocirugia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="bioquimica"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Bioquimica (Suero)">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="glucosa"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Glucosa">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="urea"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Urea">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="creatinina"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Creatinina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="bilirrubina"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Bilirrubina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="bilirrubina_neonatos"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Bilirrubina neonatos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="amilasa"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Amilasa">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="albumina"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Albumina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ck"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" CK">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="na_k"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Na, K">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="ca"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Ca">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="prot_totales"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Prot. Totales">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="hematimetria"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Hematimetria">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="hemograma"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Hemograma">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="coagulacion"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Coagulacion">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="tq"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" TQ">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="control_heparina"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Control heparina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="coagulacion_preoperatoria"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Coagulacion preoperatoria">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="estudio_basico"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Estudio basico de hemostasia">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="fibrinogeno"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Fibrinogeno">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="orina"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Orina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="elemental"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Elemental (sin sedimentos)">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="sedimento"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Sedimento">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="orina_diabeticos"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Orina diabeticos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="osmolaridad"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Osmolaridad *">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="creslinina"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Creslinina">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="na_k"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Na/K">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="urea"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Urea">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="cl"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Cl *">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="lcr"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="L.C.R.">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="celulas"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Celulas">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="bioquimica"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Bioquimica (Protein. glucosa)">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
									<newline/>
									<template>
										<match match="otros_liquidos"/>
										<children>
											<newline/>
											<table>
												<properties border="0" cellpadding="0" cellspacing="0" width="100%"/>
												<children>
													<tablebody>
														<children>
															<tablerow>
																<properties bgcolor="#DAE4F2"/>
																<children>
																	<tablecol>
																		<children>
																			<text fixtext="Otros liquidos">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="celulas"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Celulas">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
															<tablerow>
																<children>
																	<tablecol>
																		<children>
																			<template>
																				<match match="proteinas"/>
																				<children>
																					<checkbox ownvalue="1">
																						<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																						<properties type="checkbox"/>
																					</checkbox>
																				</children>
																			</template>
																			<text fixtext=" Proteinas">
																				<styles color="#002969" font-family="Verdana, Geneva, Arial, Helvetica, sans-serif" font-size="11px" font-weight="bold"/>
																			</text>
																		</children>
																	</tablecol>
																</children>
															</tablerow>
														</children>
													</tablebody>
												</children>
											</table>
											<newline/>
										</children>
									</template>
								</children>
							</paragraph>
						</children>
					</template>
					<newline/>
					<newline/>
				</children>
			</template>
			<newline/>
		</children>
	</template>
</structure>
