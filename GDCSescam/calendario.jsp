<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="pragma" content="no-cache">
	<link rel="stylesheet" href="css/style_ex.css" type="text/css">

    <script language="javascript">
      <%
        java.util.GregorianCalendar gc = new java.util.GregorianCalendar();
      %>
      var selectedDate = new Date(<%=gc.get(gc.YEAR)%>, <%=gc.get(gc.MONTH)%>, <%=gc.get(gc.DAY_OF_MONTH)%>);
      
      function submitDay(yyyy, mm, dd) {
        var dayFormats = new Array();
        dayFormats["dd"] = dd;
        dayFormats["mm"] = mm;
        dayFormats["yyyy"] = yyyy;
        dayFormats["yyyymmdd"] = yyyy + "" + (mm + 1) + "" + dd;
        dayFormats["dateToShow"] = dd + "/" + (mm + 1) + "/" + yyyy;
        //parent.opener.openBuscaWriteResult("<%=request.getParameter("resultTo")%>", parent, dayFormats);
		window.returnValue = dayFormats["dateToShow"];
		window.close();
      }
      
      function getCellInnerHTML(className, innerText) {
        var s = '<table border="0" cellspacing="0" cellpadding="0" width="100%">';
        s += '<tr><td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td></tr>';
        s += '<tr>';
        s += '<td bgcolor="#002969" width="1"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>';
        s += '<td width="100%"><table border="0" cellspacing="0" cellpadding="3" width="100%"><tr><td class="' + className + '">' + innerText + '</td></tr></table></td>';
        s += '<td bgcolor="#002969" width="1"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>';
        s += '</tr>';
        s += '<tr><td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td></tr>';
        s += '</table>';
        
        return s;
      }
      
      var monthNames = new Array();
      monthNames[0] =  "Enero";
      monthNames[1] =  "Febrero";
      monthNames[2] =  "Marzo";
      monthNames[3] =  "Abril";
      monthNames[4] =  "Mayo";
      monthNames[5] =  "Junio";
      monthNames[6] =  "Julio";
      monthNames[7] =  "Agosto";
      monthNames[8] =  "Septiembre";
      monthNames[9] =  "Octubre";
      monthNames[10] = "Noviembre";
      monthNames[11] = "Diciembre";
      
      function fillCalendarMonth(pYear, pMonth) {
        if (pYear == null || pMonth == null) {
          pYear = <%=gc.get(gc.YEAR)%>;
          pMonth = <%=gc.get(gc.MONTH)%>;
        }
        var calendarMonthDiv = document.all.item("calendarMonthDiv");
        var monthNameDiv     = document.all.item("monthNameDiv");
        
        // Calendar Title
        var innerHTML = '<table border="0" cellspacing="2" cellpadding="2" width="100%" bgcolor="#dae4f2">';
        innerHTML += '<tr>';
        innerHTML += '<td width="1"><img src="imagenes/shim.gif" border="0" height="1" width="10"></td>';
        innerHTML += '<td width="1"><img src="imagenes/shim.gif" border="0" height="1" width="10"></td>';
        innerHTML += '<td width="100%"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>';
        innerHTML += '<td width="1"><img src="imagenes/shim.gif" border="0" height="1" width="10"></td>';
        innerHTML += '<td width="1"><img src="imagenes/shim.gif" border="0" height="1" width="10"></td>';
        innerHTML += '</tr>';
        innerHTML += '<tr>';
        var prevYear = new Date(pYear-1, pMonth, 1);
        innerHTML += '<td><img src="imagenes/icoPrevYear.gif" border="0" onClick="javascript:fillCalendarMonth(' + prevYear.getFullYear() + ', ' + prevYear.getMonth() + ')" alt="Ano Anterior"></td>';
        var prevMonth = new Date(pYear, pMonth, 0);
        innerHTML += '<td><img src="imagenes/icoPrevMonth.gif" border="0" onClick="javascript:fillCalendarMonth(' + prevMonth.getFullYear() + ', ' + prevMonth.getMonth() + ')" alt="Mes Anterior"></td>';
        innerHTML += '<td class="titulo" align="center">' + monthNames[pMonth] + ' / ' + pYear + '</td>';
        var nextMonth = new Date(pYear, pMonth+1, 1);
        innerHTML += '<td><img src="imagenes/icoNextMonth.gif" border="0" onClick="javascript:fillCalendarMonth(' + nextMonth.getFullYear() + ', ' + nextMonth.getMonth() + ')" alt="Mes Seguinte"></td>';
        var nextYear = new Date(pYear+1, pMonth, 1);
        innerHTML += '<td><img src="imagenes/icoNextYear.gif" border="0" onClick="javascript:fillCalendarMonth(' + nextYear.getFullYear() + ', ' + nextYear.getMonth() + ')" alt="Ano Seguinte"></td>';
        innerHTML += '</tr>';
        innerHTML += '</table>';
        monthNameDiv.innerHTML = innerHTML;
        
        // Calendar Month
        var fstDayOfMonth = new Date(pYear, pMonth, 1);
        var lstDayOfMonth = new Date(pYear, pMonth + 1, 0);
        
        var innerHTML = '<table border="0" cellspacing="0" cellpadding="0" width="100%">';
        innerHTML += '<tr bgcolor="#DAE4F2">';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'Lunes') + '</td>';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'Martes') + '</td>';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'Mi�rcoles') + '</td>';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'Jueves') + '</td>';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'Viernes') + '</td>';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'S�bado') + '</td>';
        innerHTML += '<td>' + getCellInnerHTML('titulo', 'Domingo') + '</td>';
        innerHTML += '</tr>';
        
        innerHTML += '<tr>';
        for (var i=0; i<((fstDayOfMonth.getDay()+6)%7); i++) {
          innerHTML += '<td>' + getCellInnerHTML('standar', '&nbsp;') + '</td>';
        }
        var trs = 1;
        var dayOfWeek = fstDayOfMonth.getDay();
        for (var i=fstDayOfMonth.getDate(); i<=lstDayOfMonth.getDate(); i++) {
          if (dayOfWeek == 1 && i != fstDayOfMonth.getDate()) {
            trs++;
            innerHTML += '</tr><tr>';
          }
          innerHTML += '<td>' + getCellInnerHTML('standar', '<a href="#" onClick="submitDay(' + pYear + ', ' + pMonth + ', ' + i + ');">' + i + '</a>') + '</td>';
          dayOfWeek = (dayOfWeek + 1) % 7;
        }
        
        for (var i=((lstDayOfMonth.getDay()+6)%7); i<6; i++) {
          innerHTML += '<td>' + getCellInnerHTML('standar', '&nbsp;') + '</td>';
        }

        if (trs < 6) {        
          innerHTML += '</tr><tr>';
          for (var i=0; i<7; i++) innerHTML += '<td>' + getCellInnerHTML('standar', '&nbsp;') + '</td>';
        }
        
        innerHTML += '</table>';
        
        calendarMonthDiv.innerHTML = innerHTML;
      }
    </script>
  </head>
  <body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" onLoad="fillCalendarMonth();">
    <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
      <tr height="1">
        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr>
        <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
        <td>
          <div id="monthNameDiv" class="standar" style="position:relative;width:100%;height:24px;background-color:#dae4f2;layer-background-color:#dae4f2;"> 
      </div>
        </td>
        <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3" height="1"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr>
        <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
        <td>
          <div id="calendarMonthDiv" style="position:relative;width:218px;height:148px;"> 
        &nbsp; </div>
        </td>
        <td width="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
      <tr height="1">
        <td colspan="3" height="1" bgcolor="#002969"><img src="imagenes/shim.gif" border="0" height="1" width="1"></td>
      </tr>
    </table>
  </body>
</html>