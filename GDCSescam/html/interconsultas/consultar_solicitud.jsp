<%@ page contentType="text/html;charset=ISO-8859-1" %>
<%
// recoger parametros
int numsolicitud = new Integer((String) request.getParameter("solicitud")).intValue();
int numrespuestas = new Integer((String) request.getParameter("respuestas")).intValue();


String tipoSolicitud = (numsolicitud%10) == 0?"Destino":"Respuesta";
boolean con_replica = (numsolicitud > 100 || numsolicitud % 10 != 0);
String nombreSolicitante = "Dr. Jose Martinez Garc�a";

String titulo = "titulo";
String msg= "";
String profesional1=(numsolicitud < 100)? "Dr. Jose Garc�a Martinez":"";
String profesional2=(numsolicitud < 100)? "": "Dr. Jose Garc�a Martinez";


String centro_servicio = "";
String centro_servicio_repl = "";
String fecha = "";


String enlace_anterior = ((numsolicitud%10) == 0)?"":"consultar_solicitud.jsp?solicitud="+(numsolicitud-1)+"&respuestas="+numrespuestas;
String enlace_posterior = ((numsolicitud%10) == numrespuestas)?"":"consultar_solicitud.jsp?solicitud="+(numsolicitud+1)+"&respuestas="+numrespuestas;

switch (numsolicitud) {
	case 10:
	 titulo = "Puede ser benefica la acetazolamida (mucho cuidado)";
	 centro_servicio = "C. H. G. DE ALBACETE / Fisiolog�a";
	 msg = "Este paciente se est� tratanto actualmente con acetazolamida y necesito saber si esto le puede afectar fisiologicamente ";
	 fecha = "22/4/2003";
	 break;
	case 11:
	 titulo = "Investigaciones sobre la acetazolamida";
	 profesional2 =" Dr. Andres Carretero Sosa";
	 centro_servicio = "C. H. G. DE ALBACETE / Fisiolog�a";
	 msg = "He revisado algunos capitulos del PLM y la acetazolamida es un hermoso farmaco que funciona especificamente inhibiendo la sintesis de una sustancia conocida con el nombre de anhidrasa carbonica la cual forma el humor acuoso el cual colabora en mantener la adecuada presion intraocular. Su fin terapeutico esta destinado para disminuir la produccion de la misma en pacientes o personas que sufren de glaucoma cronico simple (angulo abierto)(cuidado).Bueno pero el paso es al efecto sistemico es decir lo que hace para supuestamente inhibir el mal de alturas.";
	 fecha = "24/4/2003";
	 break;
	case 20:
		titulo = "�Soluci�n a la osteocondritis del astragalo?";
    centro_servicio = "C. H. TOLEDO / Traumatolog�a";
		msg= "Debido a una caida escalando padece una osteocondritis del astragalo desde hace siete meses. Su evoluci�n es muy lenta, casi desesperante. LLeva muletas para caminar. Est� haciendo rehabilitaci�n cargando pesos. Se le han hecho diversas pruebas con resultados diversos: gammagraf�as (positiva en el diagn�stico de la lesi�n), TAC y resonancia magn�tica (resultado negativo). Necesito informaci�n";
	  fecha = "15/4/2003";
		break;
	case 21:
		titulo = "Breves indicaciones sobre el tratamiento";
		profesional2="Dr. C�sar Cobi�n";
		centro_servicio = "C. H. TOLEDO / Traumatolog�a";
		msg = "el tratamiento de la osteocondritis del astr�galo es complejo y que no existe una soluci�n fant�stica que pueda aplicarse a todo el mundo. Lo �ltimo que se est� haciendo es el autotransplante de cart�lago articular sacado de la rodilla del propio paciente e implantado en la zona lesionada del tobillo, todo ello v�a artrosc�pica. El resultado, a largo plazo, est� a�n por ver, y los tiempos de recuperaci�n suelen ser largos. De todas formas, cada caso debe ser individualizado pues no a todas las osteocondritis les viene bien este tratamiento.";
	  fecha = "15/4/2003";
		break;

	case 30:
		titulo = "Alergia en Manos";
		centro_servicio = "C. H. MANCHA-CENTRO / Alergolog�a";
		msg = "Presenta alergia en las manos, con determinados brotes, aproximadamente cada semana por medio. Hasta ahora s�lo se ha tratado una crema con cortisona que la controla. En realidad quisiera atacar a la causa.";
		fecha = "10/4/2003";
	 break;

	case 110:
		titulo = "Tolerancia ante medicamentos anti-inflamatorios.";
		profesional1="Dr. C�sar Cobi�n";
		centro_servicio = "Medicina General";
		centro_servicio_repl = "C.H. TOLEDO  / Traumatologia";
		msg = "El paciente no responde bien al tratamiento con Voltaren, esto puede ser debido a una tolerancia a anti-inflamatorios que no aparece en su historia. Necesito m�s informaci�n a este respecto";
		fecha = "25/4/2003";
		break;

	case 120:
		titulo = "Tendencia del paciente a ataques de p�nico o nervisiosismo continuados";
		profesional1="Dr. C�sar Cobi�n";
		centro_servicio = "Medicina General";
		centro_servicio_repl = "C.H. TOLEDO  / Traumatologia";
		msg = "Debido a la lesi�n que padece el paciente y a que la medicacion necesaria es bastante peligrosa necesitaria saber si el paciente sufre de alguna alteracion nerviosa que no est� reflejada en su H.C";
		fecha = "22/4/2003";
		break;
  case 121:
		titulo = "Re: Tendencia del paciente a ataques de p�nico o nervisiosismo continuados";
		profesional1="Dr. C�sar Cobi�n";
		centro_servicio = "Medicina General";
		centro_servicio_repl = "C.H. TOLEDO  / Traumatologia";
		msg = "El paciente es una persona tranquila y durante el tiempo que ha estado a mi cargo no ha presentado problema nervioso alguno";
		fecha = "23/4/2003";
		break;

}

if (centro_servicio_repl.equals("")) centro_servicio_repl = centro_servicio;
String prof_replica = (numsolicitud < 100)? profesional2:profesional1;
%>
<html>
<head>
<title>Interconsultas: Nueva Solicitud</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script Langage="JavaScript">
function ValidarSolicitud () {
	alert('Respuesta cursada');
	window.close();
}

function mostrar(enlace) {
	if (enlace != "")	{
	//  window.showModalDialog(enlace,"","dialogWidth:40em;dialogHeight:24em;status=no");
		parent.main.location = enlace;
	}

}


function comprobarProfesional() {
	if (document.frmSolicitud.codprof.value == "Candia") {
		 document.frmSolicitud.nameprof.value = "Angel Antonio Candia Romero";
	} else {
		if (document.frmSolicitud.codprof.value != "") {
			alert("No existe un profesional con este c�digo de usuario");
			document.frmSolicitud.codprof.value = "";
			document.frmSolicitud.nameprof.value = "";
		}
	}
}

function comprobarServicio() {
	if (document.frmSolicitud.codserv.value == "TRAUMA") {
		 document.frmSolicitud.nameserv.value = "Traumatolog�a General";
	} else {
		if (document.frmSolicitud.codserv.value != "") {
			alert("No existe un servicio para ese centro con este codigo");
			document.frmSolicitud.codserv.value = "";
			document.frmSolicitud.nameserv.value = "";
		}
	}
}
function buscadorProfesional() {
		 var retVal = window.showModalDialog("index_buscador_prof.htm","","dialogWidth:45em;dialogHeight:29em;status=no");
		 if (retVal != null) {
			 document.frmSolicitud.nameprof.value = retVal[0] ;
			 document.frmSolicitud.codprof.value = retVal[1] ;
		 }

}

function buscadorServicio() {
		 var retVal = window.showModalDialog("index_buscador_serv.htm","","dialogWidth:45em;dialogHeight:13em;status=no");
		 if (retVal != null) {
			 document.frmSolicitud.nameserv.value = retVal[0] ;
			 document.frmSolicitud.codserv.value = retVal[1] ;
		 }

}


</script>
</head>
<body bgcolor="#ffffff">
<table class="ventanadatos" border="0" width="100%" align="center" cellpadding=0 cellspacing=0>
    <tr width="100%" height="15px">
      <td>
        <table class="parasuperior" width="100%" border=0 align="left" cellpadding=0 cellspacing=0>
          <tr><td class="sololineassuperior">&nbsp;</td></tr>
          <tr>
             <td class="textopaciente"><%=titulo%></td>
				 </tr>
				 </table>
	    </td>
		</tr>

</table>
<br><br>
<table  style="border-left: 1px solid #002969;border-top: 1px solid #002969"  cellspacing="0" cellpadding="0" bgcolor="#788DB0" align="center">
 <tr height="1">
 <td colspan="3" height="1" bgcolor="#002969"><img src="images/shim.gif" border="0" height="1" width="1"></td>
 </tr>
 <tr>
    <td width="1" align="right" bgcolor="#002969"><img src="images/shim.gif" border="0" height="1" width="1"></td>
 <td>
  <table border="0" cellspacing="2" cellpadding="5" bgcolor="#DAE4F2" align="center">
		 <tr>
      <td class="textocampo" width="25%">Paciente:</td>
      <td  class="textocampo" width="75%" colspan="2">Jose Garc�a Fern�ndez</td>
    </tr>

    <tr>
      <td class="textocampo" width="25%">Solicitante:</td>
      <td class="textocampo" width="75%" colspan="2"><%=profesional1%></td>
     </tr>
		  <tr>
			<td class="textocampo" width="25%">Profesional <%=tipoSolicitud%>:</td>
      <td class="textocampo" width="75%" colspan="2"><%=profesional2%></td>

    </tr>
    <tr>
			<td class="textocampo" width="25%">Centro/Servicio <%=tipoSolicitud%>:</td>
			<td class="textocampo" width="75%" colspan="2"><%=centro_servicio%>
     </td>
		 </tr>
		   <tr>
		  <td  class="textoenlaces" width="25%" >Mensaje:</td>
    </tr>
    <tr>
      <td></td>
      <td valign="top" class="textocampo" align="left" width="75%" colspan="2">
      <%=msg%>
      </td>
    </tr>
<tr><td colspan = 3>
<table><tr>
 <td width="25%"></td>
 <td width="65%"style="cursor:hand;" class="textocampo">
<img style="cursor:hand;" onclick="mostrar('<%=enlace_anterior%>');" src="../../imagenes/play1.gif" alt="Anterior" border="0">
&nbsp;<%=((numsolicitud%10)+1)%> de <%=(numrespuestas+1)%>&nbsp;
<img style="cursor:hand;" onclick="mostrar('<%=enlace_posterior%>');" src="../../imagenes/play.gif" alt="Posterior" border="0"></a></td>
</td>
 <td width="10%" class="textocampo"><%=fecha%></td>
</tr></table>
</tr></td>
    </table>

</td>
    <td width="1" align="right" bgcolor="#002969"><img src="../../imagenes/shim.gif" border="0" height="1" width="1"></td>
</tr>
 <tr height="1">
 <td colspan="3" height="1" bgcolor="#002969"><img src="../../imagenes/shim.gif" border="0" height="1" width="1"></td>
 </tr>
</table>
<table class="fondoblanco" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td class="sololineassuperior4" width="100%">&nbsp;</td>
      <td class="imagen"><img src="../../imagenes/esq_01.gif"></td>
    </tr>
</table>
<img src="../../imagenes/cerrar.gif" style="cursor:hand;" onclick="window.close()" alt="Cerrar Ventana" border="0">
<%
if (con_replica) {
%>

 <form name="frmSolicitud" method="post" onsubmit="ValidarSolicitud();">
 <table  style="border-left: 1px solid #002969;border-top: 1px solid #002969"  cellspacing="0" cellpadding="0" bgcolor="#788DB0" align="center">
 <tr height="1">
 <td colspan="3" height="1" bgcolor="#002969"><img src="images/shim.gif" border="0" height="1" width="1"></td>
 </tr>
 <tr>
    <td width="1" align="right" bgcolor="#002969"><img src="images/shim.gif" border="0" height="1" width="1"></td>
 <td>
  <table border="0" cellspacing="2" cellpadding="5" bgcolor="#DAE4F2" align="center">
	 <tr>
      <td class="textocampo" width="25%">Paciente:</td>
      <td  class="textocampo" width="75%" colspan="2">Jose Garc�a Fern�ndez</td>
    </tr>

    <tr>
      <td class="textocampo" width="25%">Prof. Replicante:</td>
      <td class="textocampo" width="75%" colspan="2">Dr. Jose Garc�a Martinez</td>
     </tr>
		  <tr>
			<td class="textocampo" width="25%">Profesional Replica:</td>
      <td class="textocampo" width="75%" colspan="2"><%=prof_replica%></td>

    </tr>
    <tr>
			<td class="textocampo" width="25%">Centro/Servicio Replica:</td>
			<td class="textocampo" width="75%" colspan="2"><%=centro_servicio_repl%>
     </td>
		 </tr>
		   <tr>
		  <td  class="textoenlaces" width="25%" >Mensaje:</td>
    </tr>
    <tr>
      <td></td>
      <td valign="top" class="textocampo" align="left" width="75%" colspan="2">
      <textarea cols="50" name="txtComentarios" rows=7></textarea>

      </td>
    </tr>
  </table>
</td>
    <td width="1" align="right" bgcolor="#002969"><img src="../../imagenes/shim.gif" border="0" height="1" width="1"></td>
</tr>
 <tr height="1">
 <td colspan="3" height="1" bgcolor="#002969"><img src="../../imagenes/shim.gif" border="0" height="1" width="1"></td>
 </tr>
</table>
</form>

<table class="fondoblanco" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td class="sololineassuperior4" width="100%">&nbsp;</td>
      <td class="imagen"><img src="../../imagenes/esq_01.gif"></td>
    </tr>
</table>

<%
}
%>


</BODY>
</HTML>
