<%@ page language="java" %>

<HTML>
<HEAD>
<TITLE>
Visor DICOM
</TITLE>
</HEAD>
<BODY>
<%
    //Obtenemos la URL del servidor
    String serverURL = "http://" + request.getServerName() + ":" + request.getServerPort() + "/GDCSescam";
    String imageURL = serverURL + "/data";
%>
<APPLET
  CODEBASE = "."
  CODE = "dicomviewer.Viewer.class"
  NAME = "Viewer.java"
  WIDTH = 100%
  HEIGHT = 100%
  HSPACE = 0
  VSPACE = 0
  ALIGN = middle >
<PARAM NAME = "tmpSize" VALUE = "10">
<PARAM NAME = "NUM" VALUE = "15">
<PARAM NAME = "currentNo" VALUE = "0">
<PARAM NAME = "dicURL" VALUE = "<%=serverURL%>/dicomviewer/Dicom.dic">
<PARAM NAME = "imgURL0" VALUE = "<%=imageURL%>/20020424082911-03752.CTS1OC0">
<PARAM NAME = "imgURL1" VALUE = "<%=imageURL%>/20020424083032-03753.CTS1OC0">
<PARAM NAME = "imgURL2" VALUE = "<%=imageURL%>/20020424083033-03754.CTS1OC0">
<PARAM NAME = "imgURL3" VALUE = "<%=imageURL%>/20020424083035-03755.CTS1OC0">
<PARAM NAME = "imgURL4" VALUE = "<%=imageURL%>/20020424083041-03756.CTS1OC0">
<PARAM NAME = "imgURL5" VALUE = "<%=imageURL%>/20020424083042-03757.CTS1OC0">
<PARAM NAME = "imgURL6" VALUE = "<%=imageURL%>/20020424083109-03758.CTS1OC0">
<PARAM NAME = "imgURL7" VALUE = "<%=imageURL%>/20020424083110-03759.CTS1OC0">
<PARAM NAME = "imgURL8" VALUE = "<%=imageURL%>/20020424083114-03760.CTS1OC0">
<PARAM NAME = "imgURL9" VALUE = "<%=imageURL%>/20020424083116-03761.CTS1OC0">
<PARAM NAME = "imgURL10" VALUE = "<%=imageURL%>/20020424083120-03762.CTS1OC0">
<PARAM NAME = "imgURL11" VALUE = "<%=imageURL%>/20020424083122-03763.CTS1OC0">
<PARAM NAME = "imgURL12" VALUE = "<%=imageURL%>/20020424083125-03764.CTS1OC0">
<PARAM NAME = "imgURL13" VALUE = "<%=imageURL%>/20020424083129-03765.CTS1OC0">
<PARAM NAME = "imgURL14" VALUE = "<%=imageURL%>/20020424083154-03766.CTS1OC0">
</APPLET> 
</BODY>
</HTML> 