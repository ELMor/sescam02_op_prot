package isf.util;

/**
 * Utilidades para la presentaci�n de datos en HTML y JavaScript
*/
public class HtmlUtil {

	/**
	 * Reemplaza los c�digos de escape HTML en un String por c�digos seguros.
	 * @param str String a formatear
	 * @return String formateado
	*/
	public static String unescapeHTML(String str) {
		StringBuffer resultado = new StringBuffer();
		char car;

		for (int i = 0; i < str.length(); i++)
			switch (car = str.charAt(i)) {
				case '<':
					resultado.append("&#060;");
					break;
				case '>':
					resultado.append("&#062;");
					break;
				case '&':
					resultado.append("&#038;");
					break;
				case '"':
					resultado.append("&#034;");
					break;
				case '\'':
					resultado.append("&#039;");
					break;
				case '�':
					resultado.append("&#191;");
					break;
				case '�':
					resultado.append("&#161;");
					break;
				case '@':
					resultado.append("&#064;");
					break;
				case '#':
					resultado.append("&#035;");
					break;
				default:
					resultado.append(car);
			}

		return resultado.toString();
	}

	/**
	 * Reemplaza los c�digos de inicio/fin de cadena JavaScript en un String por c�digos seguros.
	 * @param str String a formatear
	 * @return String formateado
	*/
	public static String unescapeJS(String str) {
		StringBuffer resultado = new StringBuffer();
		char car;

		for (int i = 0; i < str.length(); i++)
			switch (car = str.charAt(i)) {
				case '\'':
					resultado.append("\\'");
					break;
				case '\\':
					resultado.append("\\\\");
					break;

				case '"':
					resultado.append("\\\"");
					break;
				case '\n':
					resultado.append("\\\n");
					break;
				default:
					resultado.append(car);
			}

		return resultado.toString();
	}

	/**
	 * Formatea un String completo en par�grafos HTML.
	 * @param str String a formatear
	 * @return String formateado
	*/
	public static String stringToHtml (String str) {
		StringBuffer resultado = new StringBuffer();
		String cadena = unescapeHTML(str);
		char car;

		resultado.append("<P>");

		for (int i = 0; i < cadena.length(); i++)
			switch (car = cadena.charAt(i)) {
				case '\n':
					resultado.append("</P><P>");
					break;
				default:
					resultado.append(car);
			}

		resultado.append("</P>");

		return resultado.toString();
	}

	/**
	 * Filtra objetos nulos (String inclu�dos) por cadenas vac�as,
	 * para eliminar el literal 'null' cuando se pretende mostrar en HTML.
	 * @param objeto Objeto a filtrar.
	 * @return El mismo objeto si no es nulo, o cadena vac�a si es nulo.
	*/
	public static Object filtraNull(Object objeto) {
		return objeto == null ? "" : objeto;
	}
}