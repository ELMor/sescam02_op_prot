package isf.util;


import quadbase.ChartAPI.*;
import quadbase.util.*;
import quadbase.chart.*;

import java.awt.Font;
import java.awt.Color;
import java.applet.Applet;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author sin atribuir
 * @version 1.0
 */

public class UtilGrafica {

	public static final Font LETRA_CORPORATIVA1=new Font("DIN Regular",Font.PLAIN,9); //para uso general

  //Constates para los colores de las gr�ficas
  //azul corporativo al 100%
  public static final Color COLOR1 = new Color(0,39,81);
  public static final Color COLOR5 = new Color(255,0,0);
	public static final Color COLOR9 = new Color(00,255,0);

  /**
     * Determina las caracteristica del eje X de la gr�fica.
     * <br><br>
     *
     * @param titulo Titulo de la gr�fica.
     * @param visible Es visible o no.
     * @param scalaAutom�tica Es o no automatica la scala.
     * @param minScala Si la scala no es autom�tica especifica el valor m�nimo.
     * @param maxScala Si la scala no es autom�tica especifica el valor m�ximo.
     * @param scalaStep Si la scala no es autom�tica especifica el intervalo.
     * @param chart Gr�fica.
     *
     * @return .
     */

    /* MARCADOR XAXIS */
    public static void xAxis(String titulo,boolean visible,boolean scalaAutomatica,double minScala,double maxScala, double scalaStep, QbChart chart) throws Exception {
        // Set x-axis
        IAxis hXAxis = chart.gethXAxis();
        hXAxis.gethTitle().setFont(LETRA_CORPORATIVA1);
        hXAxis.gethTitle().setValue(titulo);
				hXAxis.gethLabel().setAngle(0);
        hXAxis.setColor(Color.black);
        hXAxis.setVisible(visible);

        hXAxis.setThickness(1);
        hXAxis.setArrowhead(false);

        if (visible==true) {
            hXAxis.setTickersVisible(true);
            //Compruebo si la escala es autom�tica
            hXAxis.setGridInFront(false);
            hXAxis.setGridVisible(true);
            hXAxis.setGridThickness(1);
            hXAxis.setGridColor(Color.gray);

            if (scalaAutomatica == false) {
                hXAxis.setScaleAutomatic(false);
                hXAxis.setMinScale(new Double(minScala));
                hXAxis.setMaxScale(new Double(maxScala));
                hXAxis.setScaleStep(new Double(scalaStep));
            }
        }
        else {
            hXAxis.setDisplayLabelAsDate(false);
            hXAxis.setGridAlignedWithTicker(false);
            hXAxis.setGridInFront(false);
            hXAxis.setGridVisible(false);
            hXAxis.setLabelOutsidePlotArea(false);
            hXAxis.setLogScale(false);
            hXAxis.setOriginAutomatic(false);
            hXAxis.setScaleAutomatic(false);
            hXAxis.setShowLogValue(false);
            hXAxis.setTickersVisible(false);
            //hXAxis.setGridVisible(true);
            hXAxis.setGridColor(Color.black);
            hXAxis.setTickersVisible(false);
        }
    }
		 public static void yAxis(String titulo,boolean visible,boolean scalaAutomatica,double minScala,double maxScala, double scalaStep, QbChart chart) throws Exception {
			 IAxis hYAxis = chart.gethYAxis();
			  hYAxis.gethTitle().setFont(LETRA_CORPORATIVA1);
        hYAxis.gethTitle().setValue(titulo);
        hYAxis.setColor(Color.black);
        hYAxis.setVisible(visible);

        hYAxis.setThickness(1);
        hYAxis.setArrowhead(false);

        hYAxis.setGridInFront(false);
        hYAxis.setGridVisible(true);
        hYAxis.setGridThickness(1);
        hYAxis.setGridColor(Color.black);

        if (visible==true) {
            hYAxis.setTickersVisible(true);
            //Compruebo si la escala es autom�tica
            if (scalaAutomatica == false) {
                hYAxis.setScaleAutomatic(false);
                hYAxis.setMinScale(new Double(minScala));
                hYAxis.setMaxScale(new Double(maxScala));
                hYAxis.setScaleStep(new Double(scalaStep));
            }
        }
        else {
            hYAxis.setTickersVisible(false);
        }
    }

		/* MARCADOR LABELFONT */
    public static void labelFont(QbChart chart, boolean xVisible, boolean yVisible, String fontName, int fontSize, boolean bold, boolean turn) {
        IAxis xAxis = chart.gethXAxis();
        IAxis yAxis = chart.gethYAxis();
        ILabel xLabel = xAxis.gethLabel();
        ILabel yLabel = yAxis.gethLabel();

        xLabel.setVisible(xVisible);
        yLabel.setVisible(yVisible);

        xLabel.setColor(Color.black);
        yLabel.setColor(Color.black);

        if (bold){
            xLabel.setFont(new Font(fontName, Font.BOLD, fontSize));
            yLabel.setFont(new Font(fontName, Font.BOLD, fontSize));
        }else{
            xLabel.setFont(new Font(fontName, Font.PLAIN, fontSize));
            yLabel.setFont(new Font(fontName, Font.PLAIN, fontSize));
        }

        if (turn) xLabel.setAngle(90);
    }
		public static void leyenda(boolean visible,String posicion,float x, float y, QbChart chart) throws Exception {
        ILegend hLegend = chart.gethLegend();
        hLegend.setAppearance(QbChart.PLAIN);
        hLegend.setVisible(visible);
        hLegend.setBorderVisible(false);
        hLegend.setBackgroundColor(Color.white);
        hLegend.gethText().setFont(LETRA_CORPORATIVA1);
        if (posicion.equals("HORIZONTAL")) hLegend.setLayout(QbChart.HORIZONTAL);
        else if (posicion.equals("VERTICAL")) hLegend.setLayout(QbChart.VERTICAL);
        hLegend.setPosition(new Position(x,y));
    }

/* MARCADOR POSICIONCHART */
    public static void posicionChart(QbChart chart, float xPos, float yPos, float rWidth, float rHeight) throws Exception {
        IPlot ip = chart.gethChartPlot();
        ip.setPosition(new Position(xPos, yPos));
        ip = chart.gethChartPlot();
        ip.setBackgroundVisible(true);
        ip.setBackgroundColor(Color.white); //fondo de color blanco
        ip.setRelativeWidth((float)rWidth);
        ip.setRelativeHeight((float)rHeight);
    }


		public static QbChart getChart(String [][] records) throws Exception {

			String[] fieldName = {"","",""};
				String [] dataType = new String[] {"String", "String","double"};
				// 2.
				DbData data = new DbData(dataType, fieldName,records);
				// 3.
				ColInfo colInfo = new ColInfo();
        colInfo.series = 1;
        colInfo.category = 0;
        colInfo.value = 2;
        colInfo.subvalue = -1;

				// 4.
				QbChart chart = new QbChart((Applet)null, QbChart.VIEW2D, QbChart.LINE, data, colInfo, null);

				posicionChart(chart, 0.1f, 0.06f, 0.80f, 0.55f);

				// 2. Color
				chart.gethCanvas().setBackgroundColor(Color.white);

				Color colorSeriesColor[] = {COLOR1,COLOR5,COLOR9};
				chart.gethDataPoints().setColors(colorSeriesColor);

				//3. Leyenda.
				leyenda(true,"HORIZONTAL",(float)0.38,(float)0.05,chart);

				yAxis("",true,true,0,500,100,chart);
				xAxis("periodo",true,true,0,0,0,chart);
				labelFont(chart, true, true, "DIN Regular", 9, false, true);

				chart.gethDataPoints().gethLabel().setVisible(false);
				//chart.gethDataPoints().setLineThickness(3);

				chart.gethXAxis().setLabelOutsidePlotArea(true);

				return chart;

		}

}