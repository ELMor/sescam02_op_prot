package isf.util.log;

import java.util.Date;


import isf.util.DateConverse;

/**
 Implementaci�n de un proveedor de log para la salida estandar y
 la salida de errores.
 @author MGO
 @since 2001/04/03
 */
class ConsoleLog implements ILog
{
	/** Idenficador dentro de una traza de un error cr�tico */
	private static String CRITICAL_ERROR_TAG = "ERR+";

	/** Idenficador dentro de una traza de un error  */
	private static String ERROR_TAG          = "ERRO";

	/** Idenficador dentro de una traza de un aviso  */
	private static String WARNING_TAG        = "WARN";

	/** Idenficador dentro de una traza de una traza de informaci�n */
	private static String INFO_TAG           = "INFO";

	/** Idenficador dentro de una traza de una traza de detalle */
	private static String DETAIL_TAG         = "DETA";

	/** Idenficador dentro de una traza de una traza de desarrollo */
	private static String TRACE_TAG          = "TRAC";


	/** Variable que representa la zona cr�tica para controlar
	    el acceso concurrente al escribir en la salida de errores */
	private static Object obCritErr = new Object();

	/** Variable que representa la zona cr�tica para controlar
	    el acceso concurrente al escribir en la salida s */
	private static Object obCritOut = new Object();


	/** Variable que controla que se ha generado la traza inicial en
	    la salida estandar de errores */
	private static boolean bErrInitialized = false;


   /** Variable que controla que se ha generado la traza inicial en
	    la salida estandar  */
	private static boolean bOutInitialized = false;

   /**
     * Logs a text string at CRITICAL_ERROR_LEVEL
     *
     * This method provides the implementation for a quick accessor
     * to log a critical error,
     * replacing typical calls to System.err.println().
     */
    public void criticalError(ExecutionInfo ei, String logString)
    {
	  logErr(CRITICAL_ERROR_LEVEL, ei, logString);
	  return;
    }


	/**
     * Logs a text string at ERROR_LEVEL
     *
     * This method provides the implementation for a quick accessor
     * to log a critical error,
     * replacing typical calls to System.err.println().
     */
    public void error(ExecutionInfo ei, String logString)
	{
	  logErr(ERROR_LEVEL, ei,logString);
	  return;
    }

    /**
     * Logs a text string at WARNING_LEVEL
     */
    public void warning(ExecutionInfo ei, String logString)
	{
	  log(WARNING_LEVEL, ei, logString);
	  return;
    }


    /**
     * Logs a text string at INFO_LEVEL
     */
    public void info(ExecutionInfo ei, String logString)
	{
	  log(INFO_LEVEL, ei, logString);
	  return;
    }

    /**
     * Logs a text string at DETAIL_LEVEL
     */
    public void detail(ExecutionInfo ei, String logString)
    {
	  log(DETAIL_LEVEL, ei, logString);
	  return;
    }

    /**
     * Logs a text string at TRACE_LEVEL
     */
    public void trace(ExecutionInfo ei, String logString)
	{
	  log(TRACE_LEVEL, ei, logString);
	  return;
	}


	/**
	  M�todo que devuelve una cadena de caracteres con el siguiente
	  formato.
	  fecha \t tipo_traza \t Nombre_clase \t metodo \t l�nea \t mensaje
	*/
	private String composeLine(int nTraceLevel, ExecutionInfo ei, String logString)
	{
		StringBuffer strBuff = new StringBuffer();

	    String string = DateConverse.dateToString_ddMMyyhhmmss(new Date());
 	    strBuff.append(string);
		strBuff.append("\t");

		switch (nTraceLevel)
		{

		    case CRITICAL_ERROR_LEVEL:
		    {
		      strBuff.append(CRITICAL_ERROR_TAG);
		      break;
		    }
			case ERROR_LEVEL:
			{
		      strBuff.append(ERROR_TAG);
		      break;
		    }
			case WARNING_LEVEL:
			{
		      strBuff.append(WARNING_TAG);
		      break;
		    }
			case INFO_LEVEL:
			{
		      strBuff.append(INFO_TAG);
		      break;
		    }
			case DETAIL_LEVEL:
			{
		      strBuff.append(DETAIL_TAG);
		      break;
		    }
			case TRACE_LEVEL:
			{
		      strBuff.append(TRACE_TAG);
		      break;
		    }
		}
		strBuff.append("\t");
		strBuff.append(ei.getClassName());
		strBuff.append("\t");
	    strBuff.append(ei.getMethodName());
		strBuff.append("\t");
		if ( null == ei.getLinePos())
		    strBuff.append("?");
		else
		    strBuff.append(ei.getLinePos());
		strBuff.append("\t");
		strBuff.append(logString);
		// strBuff.append("\n");

		return strBuff.toString();


	}


   /**
      M�todo que crea una traza en la salida estandar de errores.
	  @param nTraceLevel Nivel de la traza.
	  @param ei Contexto de ejecuci�n de la traza
	  @param logString Mensaje que se desea dejar constancia.
   */
   protected void logErr( int nTraceLevel, ExecutionInfo ei, String logString)
   {
   		synchronized (obCritErr)
   		{
   		  if (false == bErrInitialized)
		  {
			System.err.println(" ------------------------------------------------------- " );
			System.err.println(" Error log initialized : " + DateConverse.dateToString_ddMMyyhhmmss(new Date()));
   			System.err.println(" ------------------------------------------------------- " );

			bErrInitialized = true;

		  }

		  System.err.println(composeLine(nTraceLevel, ei, logString));
   		}
		return;
   }

   /**
      M�todo que crea una traza en la salida estandar.
	  @param nTraceLevel Nivel de la traza.
	  @param ei Contexto de ejecuci�n de la traza
	  @param logString Mensaje que se desea dejar constancia.
   */
   protected void log( int nTraceLevel, ExecutionInfo ei, String logString)
   {
   		synchronized (obCritOut)
   		{
   		  if (false == bOutInitialized)
		  {
			System.out.println(" ------------------------------------------------------- " );
			System.out.println(" Log initialized : " + DateConverse.dateToString_ddMMyyhhmmss(new Date()));
   			System.out.println(" ------------------------------------------------------- " );

			bOutInitialized = true;

		  }

		  System.out.println(composeLine(nTraceLevel, ei, logString));
   		}
		return;
   }
}
