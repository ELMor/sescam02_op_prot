package isf.util.log; 

/**
   Interfaz que deben cumplir todos los proveedores de log que 
   necesiten integrarse con el FrameWork. 
   @author MGO 
   @since 2001/04/04
*/
public interface ILog
{ 
	/**
	 * Turns the log off, no messages are logged
	 */
	public static final int DEACTIVATE_LEVEL = 0;

	/**
	 * The system encountered a critical error at this logging level, 
	 * which affects the accuracy, integrity, reliability, or 
	 * capability of the system.  Someone should be paged to address
	 * the error as soon as possible.
	 */
	public static final int CRITICAL_ERROR_LEVEL = 1;

	/**
	 * The system encountered an unexpected error at this logging level, 
	 * which probably means the code intercepted an error it cannot handle.
	 * This error is not of a critical nature and can be recovered from
	 * automatically.
	 * Someone should probably be emailed to resolve the error in the near
	 * future to increase the reliability of the product.
	 */
	public static final int ERROR_LEVEL = 2;

	/**
	 * The system encountered an expected error situation.  The system
	 * recovered from it but the fact that it happened should be recorded
	 * to see how frequent it happens.
	 */
	public static final int WARNING_LEVEL = 3;

	/**
	 * Normal logging level.  All interesting periodic events should 
	 * be logged at this level so someone looking through the log can
	 * see the amount and kind of processing happening in the system.
	 */
	public static final int INFO_LEVEL = 4;

	/**
	 * Moderately detailed logging level to be used to help debug
	 * typical problems in the system.  Not so detailed that the
	 * big picture gets lost.
	 */
	public static final int DETAIL_LEVEL = 5;

	/**
	 * Most detailed logging level. Everything
	 * sent to the log will be logged.  Use this level to 
	 * trace system execution for really nasty problems.
	 */
	public static final int TRACE_LEVEL = 6;

	


    /**
     * Logs a text string at CRITICAL_ERROR_LEVEL
     *
     * This method provides the implementation for a quick accessor
     * to log a critical error,
     * replacing typical calls to System.err.println().
     */
    public void criticalError(ExecutionInfo ei, String logString);
    
	

	/**
     * Logs a text string at ERROR_LEVEL
     *
     * This method provides the implementation for a quick accessor
     * to log a critical error,
     * replacing typical calls to System.err.println().
     */
    public void error(ExecutionInfo ei, String logString);

    /**
     * Logs a text string at WARNING_LEVEL
     */
    public void warning(ExecutionInfo ei, String logString);

    /**
     * Logs a text string at INFO_LEVEL
     */
    public void info(ExecutionInfo ei, String logString);
    /**
     * Logs a text string at DETAIL_LEVEL
     */
    public void detail(ExecutionInfo ei, String logString);

    /**
     * Logs a text string at TRACE_LEVEL
     */
    public void trace(ExecutionInfo ei, String logString);
	


} 
