package isf.util.log;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Properties;
import java.net.URL;
import java.io.File;
import isf.util.PropertiesLoader;


/**
  Clase que representa el interfaz del servicio de logs.
  Se han definido 6 niveles de trazas :

    DEACTIVATE_LEVEL (0) logs desactivados.
    CRITICAL_ERROR_LEVEL (1) solo errores cr�ticos.
    ERROR_LEVEL(2). Errores normales.;
    WARNING_LEVEL(3). Avisos;
    INFO_LEVEL(4) Trazas sobre la ejecuci�n a grandes rasgos.
    DETAIL_LEVEL(5). Trazas de detalle de la ejecucion.
    TRACE_LEVEL(6). Trazas de desarrollo.

  @author MGO
  @since 2001/04/03
*/

public class Log
{

	/** Variable que contiene la �nica instancia del servicio de log. */
	private static Log _instance = null;

	/** Nombre del fichero contedor de los proveedores de log disponibles */
	private static String PROVIDERS_FILE = "/providers.properties";

	/** Nombre del fichero contedor de la informaci�n del servicio */
	private static String CONFIG_FILE = "/config.properties";

	/**
	    Nombre de la propiedad del fichero de configuraci�n que
	    contiene el valor m�ximo permitido para las trazas
	*/
	private static String MAX_TRACE_LEVEL_PROP_TAG = "max_trace_level";

	/** Vector de servicios de log disponibles */
	private Vector _vAvailableLogServices = null;

    /** El nivel actual de traza.*/
    private int _nActualTraceLevel = 0;

	/**
	 * Turns the log off, no messages are logged
	 */
	public static final int DEACTIVATE_LEVEL = 0;

	/**
	 * The system encountered a critical error at this logging level,
	 * which affects the accuracy, integrity, reliability, or
	 * capability of the system.  Someone should be paged to address
	 * the error as soon as possible.
	 */
	public static final int CRITICAL_ERROR_LEVEL = 1;

	/**
	 * The system encountered an unexpected error at this logging level,
	 * which probably means the code intercepted an error it cannot handle.
	 * This error is not of a critical nature and can be recovered from
	 * automatically.
	 * Someone should probably be emailed to resolve the error in the near
	 * future to increase the reliability of the product.
	 */
	public static final int ERROR_LEVEL = 2;

	/**
	 * The system encountered an expected error situation.  The system
	 * recovered from it but the fact that it happened should be recorded
	 * to see how frequent it happens.
	 */
	public static final int WARNING_LEVEL = 3;

	/**
	 * Normal logging level.  All interesting periodic events should
	 * be logged at this level so someone looking through the log can
	 * see the amount and kind of processing happening in the system.
	 */
	public static final int INFO_LEVEL = 4;

	/**
	 * Moderately detailed logging level to be used to help debug
	 * typical problems in the system.  Not so detailed that the
	 * big picture gets lost.
	 */
	public static final int DETAIL_LEVEL = 5;

	/**
	 * Most detailed logging level. Everything
	 * sent to the log will be logged.  Use this level to
	 * trace system execution for really nasty problems.
	 */
	public static final int TRACE_LEVEL = 6;


	/** Valor por defecto del nivel de traza */
	private static int DEFAULT_TRACE_LEVEL = INFO_LEVEL;


	/**
	 �nico constructor.
	 */
    private Log()
    {
   	 loadAvailableLogServices();
	 loadLogProperties();
	 return;
    }


    /**
	  M�todo que devuelve la unica instancia del servicio de log.
	*/
	public static synchronized Log getInstance()
	{
		if ( null == _instance)
			_instance = new Log();

		return _instance;

	}


   /**
     * Logs a text string at CRITICAL_ERROR_LEVEL
     *
     * This method provides the implementation for a quick accessor
     * to log a critical error,
     * replacing typical calls to System.err.println().
     */
    public void criticalError(String logString)
    {
	  log(CRITICAL_ERROR_LEVEL, logString);
	  return;
    }


	/**
     * Logs a text string at ERROR_LEVEL
     *
     * This method provides the implementation for a quick accessor
     * to log a critical error,
     * replacing typical calls to System.err.println().
     */
    public void error(String logString)
	{
	  log(ERROR_LEVEL, logString);
	  return;
    }

    /**
     * Logs a text string at WARNING_LEVEL
     */
    public void warning(String logString)
	{
	  log(WARNING_LEVEL, logString);
	  return;
    }


    /**
     * Logs a text string at INFO_LEVEL
     */
    public void info(String logString)
	{
	  log(INFO_LEVEL, logString);
	  return;
    }

    /**
     * Logs a text string at DETAIL_LEVEL
     */
    public void detail(String logString)
    {
	  log(DETAIL_LEVEL, logString);
	  return;
    }

    /**
     * Logs a text string at TRACE_LEVEL
     */
    public void trace(String logString)
	{
	  log(TRACE_LEVEL, logString);
	  return;
	}


	/**
	  Proceseo que envia la traza a todos los proveedores
	  de log registrados si el nivel de la traza es inferior
	  o igual al nivel m�ximo solicitado por configuraci�n.
	  @param nTraceLevel Nivel de la traza solicitada
	  @param logString La cadena a reflejar en el servicio de log
	 */
	protected void log(int nTraceLevel, String logString)
	{
		// La traza no debe ser reportada?
		if (  nTraceLevel > _nActualTraceLevel)
			return;
		else
		{
		  ExecutionInfo ei = LogHelper.getExecutionInfo();

		  ILog logprovider = null;
		  final int nNumLogProviders = _vAvailableLogServices.size();


		  // Para todos los proveedores registrados.
		  for(int n = 0; n < nNumLogProviders; n++)
		  {
		    logprovider =  (ILog) _vAvailableLogServices.elementAt(n);

			switch (nTraceLevel)
			{

			    case CRITICAL_ERROR_LEVEL:
			    {
			      logprovider.criticalError(ei, logString);
			      break;
			    }
			    case ERROR_LEVEL:
			    {
			      logprovider.error(ei, logString);
			      break;
			    }
			    case WARNING_LEVEL:
			    {
 			      logprovider.warning(ei, logString);
			      break;
			    }
			    case INFO_LEVEL:
			    {
 			      logprovider.info(ei, logString);
			      break;
			    }
			    case DETAIL_LEVEL:
			    {
 			      logprovider.detail( ei, logString);
				  break;
			    }
			    case TRACE_LEVEL:
			    {
 			      logprovider.trace( ei, logString);
			      break;
			    }
			}
			logprovider = null;
	      }

		}
	  return;
	}

	/**
	   Metodo que construye el vector con los
	   servicios de log disponibles en funci�n

	*/
	protected void loadAvailableLogServices()
	{

	  _vAvailableLogServices = new Vector();

	  URL urlLogProviders = null;

	  // Transformamos el path en una URL que es completamente portable.
	  urlLogProviders = getClass().getResource(PROVIDERS_FILE);

	  // No encontramos el fichero de configuracion ?
	  if ( null == urlLogProviders)
	  {
		 return;
	  }


	  File file = new File(urlLogProviders.getFile());

	  // No existe el fichero o no se puede leer ?
	  if (false == file.exists() || false == file.canRead() )
	  {
	    return;
	  }

	  Enumeration enum = null;
	  String strClassName = null;
	  Class clLog = null;
	  ILog log = null;

	  Properties props =
	  	PropertiesLoader.getInstance().getProperties(file.getAbsolutePath());


	  // Procesamos todas las entradas:
	  enum = props.elements();

	  while (enum.hasMoreElements())
	  {
        strClassName = (String) enum.nextElement();

		try
		{
		  clLog = Class.forName(strClassName.trim());
		  log = (ILog) clLog.newInstance();
		  _vAvailableLogServices.add(log);

		}
		catch (Exception e)
		{ }
		finally
		{

		  strClassName = null;
		  clLog = null;
		  log = null;
		}

	  }

	  return;
	}

	/**
	   Metodo que establece el nivel de traza
	   solicitado en el sistema.
	 */
	protected void loadLogProperties()
	{
	  URL urlConfigFile = null;

	  // Transformamos el path en una URL que es completamente portable.
	  urlConfigFile = getClass().getResource(CONFIG_FILE);

	  // No encontramos el fichero de configuracion ?
	  if ( null == urlConfigFile)
	  {
	  	_nActualTraceLevel = DEFAULT_TRACE_LEVEL;
		 return;
	  }


	  File file = new File(urlConfigFile.getFile());

	  // No existe el fichero o no se puede leer ?
	  if (false == file.exists() || false == file.canRead() )
	  {
	  	_nActualTraceLevel = DEFAULT_TRACE_LEVEL;
	    return;
	  }


	  Properties props =
	  	PropertiesLoader.getInstance().getProperties(file.getAbsolutePath());

	  try
	  {

	   String strValAux = props.getProperty(MAX_TRACE_LEVEL_PROP_TAG);

	   if (null == strValAux)
	   {
	   	 _nActualTraceLevel = DEFAULT_TRACE_LEVEL;
	   }
	   else
	   {
	   	  _nActualTraceLevel = Integer.parseInt(strValAux);
	   }
	  }
	  catch (NumberFormatException nfe)
	  {
	    _nActualTraceLevel = DEFAULT_TRACE_LEVEL;

	  }

	  return;

	}



}
