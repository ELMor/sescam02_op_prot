///////////////////////////////////////////////////////////
// DeJaved by mDeJava v1.0. Copyright 1999 MoleSoftware. //
//       To download last version of this software:      //
//            http://molesoftware.hypermatr.net          //
//               e-mail:molesoftware@mail.ru             //
///////////////////////////////////////////////////////////
//          JAD JavaDecompiler by Pavel Kuznetsov        //
// www.geocities.com/SiliconValley/Bridge/8617/jad.html  //
///////////////////////////////////////////////////////////

package isf.util.log;


public class ExecutionInfo
{

    private String _strClassName = null;
    private String _strMethod = null;
    private String _strLinePos = null;
    private String _strThreadId = null;

    public ExecutionInfo()
    {
        _strClassName = null;
        _strMethod = null;
        _strLinePos = null;
        _strThreadId = null;
    }

    public String getClassName()
    {
        return _strClassName;
    }

    public String getLinePos()
    {
        return _strLinePos;
    }

    public String getMethodName()
    {
        return _strMethod;
    }

    public String getThreadId()
    {
        return _strThreadId;
    }

    public void setClassName(String s)
    {
        _strClassName = s;
    }

    public void setLinePos(String s)
    {
        _strLinePos = s;
    }

    public void setMethodName(String s)
    {
        _strMethod = s;
    }

    public void setThreadId(String s)
    {
        _strThreadId = s;
    }
}
