///////////////////////////////////////////////////////////
// DeJaved by mDeJava v1.0. Copyright 1999 MoleSoftware. //
//       To download last version of this software:      //
//            http://molesoftware.hypermatr.net          //
//               e-mail:molesoftware@mail.ru             //
///////////////////////////////////////////////////////////
//          JAD JavaDecompiler by Pavel Kuznetsov        //
// www.geocities.com/SiliconValley/Bridge/8617/jad.html  //
///////////////////////////////////////////////////////////

package isf.util.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.StringTokenizer;

// Referenced classes of package isf.util.log:
//            ExecutionInfo

class LogHelper
{

    private static String LOG_PACKAGE = "isf.util.log";
    private static String ISF_TRACER_CLASS = "isf.util.Tracer";

    LogHelper()
    {
    }

    public static ExecutionInfo getExecutionInfo()
    {
        Exception exception = new Exception("8)");
        StringWriter stringwriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(stringwriter));
        String s = stringwriter.toString();
        int i = s.indexOf("at isf.util.log.Log.log");
        i = s.indexOf("at", i + 2);
        s = s.substring(i);
        StringTokenizer stringtokenizer = new StringTokenizer(s, "\t");
        Object obj = null;
        Object obj1 = null;
        Object obj2 = null;
        boolean flag = false;
        ExecutionInfo executioninfo = null;
        for(boolean flag1 = false; !flag1 && stringtokenizer.hasMoreTokens();)
        {
            String s1 = stringtokenizer.nextToken();
            if(s1.indexOf(LOG_PACKAGE) == -1 && s1.indexOf(ISF_TRACER_CLASS) == -1)
            {
                flag1 = true;
                executioninfo = parseStackTraceLine(s1);
            }
            s1 = null;
        }

        executioninfo.setThreadId(Thread.currentThread().getName());
        return executioninfo;
    }

    private static ExecutionInfo parseStackTraceLine(String s)
    {
        ExecutionInfo executioninfo = new ExecutionInfo();
        int i = s.indexOf("at ");
        if(i == -1)
            i = 0;
        else
            i += 3;
        int j = s.indexOf("(");
        if(j == -1)
            return executioninfo;
        String s1 = s.substring(i, j).trim();
        int k = s1.lastIndexOf(46);
        if(k == -1)
            return executioninfo;
        executioninfo.setClassName(s1.substring(0, k));
        executioninfo.setMethodName(s1.substring(k + 1));
        k = s.indexOf(":");
        if(k == -1)
            return executioninfo;
        j = s.indexOf(")");
        if(j == -1)
        {
            return executioninfo;
        }
        else
        {
            executioninfo.setLinePos(s.substring(k + 1, j));
            return executioninfo;
        }
    }

}
