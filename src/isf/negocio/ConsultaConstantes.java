package isf.negocio;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author sin atribuir
 * @version 1.0
 */

import java.util.Properties;
import java.util.Hashtable;
import java.util.Vector;
import isf.xml.ConsultaXQuery;

public class ConsultaConstantes {

	static ConsultaConstantes consulta = null;
	private Hashtable ht_constantes = null;
  private ConsultaConstantes() {
		ht_constantes = new Hashtable();
		// peso
		Properties prop_peso = new Properties();
		prop_peso.setProperty("maximo",new Long(100).toString());
		prop_peso.setProperty("minimo",new Long(50).toString());
		prop_peso.setProperty("nombre_largo","Peso (kg.)");
		ht_constantes.put("peso",prop_peso);
		// talla
		Properties prop_talla = new Properties();
		prop_talla.setProperty("maximo",new Long(250).toString());
		prop_talla.setProperty("minimo",new Long(130).toString());
		prop_talla.setProperty("nombre_largo","Talla (cm.)");
		ht_constantes.put("talla",prop_talla);
		// colesterol
		Properties prop_colesterol = new Properties();
		prop_colesterol.setProperty("maximo",new Long(210).toString());
		prop_colesterol.setProperty("minimo",new Long(100).toString());
		prop_colesterol.setProperty("nombre_largo","Colesterol (u.m.i.)");
		ht_constantes.put("colesterol",prop_colesterol);
		// presion arterial
		Properties prop_presion = new Properties();
		prop_presion.setProperty("maximo",new Long(250).toString());
		prop_presion.setProperty("minimo",new Long(300).toString());
		prop_presion.setProperty("nombre_largo","Presion arterial");
		ht_constantes.put("presionarterial",prop_presion);
		// trigliceridos
		Properties prop_trigliceridos = new Properties();
		prop_trigliceridos.setProperty("maximo",new Long(250).toString());
		prop_trigliceridos.setProperty("minimo",new Long(300).toString());
		prop_trigliceridos.setProperty("nombre_largo","Trigliceridos (u.m.i.)");
		ht_constantes.put("trigliceridos",prop_trigliceridos);

  }

	public static ConsultaConstantes getInstance() {
		if (consulta == null) {
			consulta = new ConsultaConstantes();
		}
		return consulta;
	}

	public Properties getDatosConstantes(Properties prop_params) {
		Properties prop_result = new Properties();
		Vector v_datos =  ConsultaXQuery.getDatosConstantes(prop_params);
		String constante = prop_params.getProperty("constante","");
		prop_result.put("datos",v_datos);
		prop_result.setProperty("constante",constante);
		if (constante.equals("")) {
			prop_result.put("datos_constante",new Properties());
		} else prop_result.put("datos_constante",ht_constantes.get(constante));

		return prop_result;
	}


  public static void main(String[] args) {
    ConsultaConstantes consultaConstantes1 = new ConsultaConstantes();
  }
}