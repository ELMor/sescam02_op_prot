// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   AnimationThread.java

package dicomviewer;

import java.awt.Scrollbar;
import java.awt.TextComponent;

// Referenced classes of package dicomviewer:
//            Viewer

public class AnimationThread extends Thread
{

    Viewer parent;
    boolean isStop;
    boolean isNext;
    int interval;

    public AnimationThread(Viewer viewer)
    {
        isStop = false;
        isNext = true;
        interval = 1000;
        parent = viewer;
    }

    public void run()
    {
        while(!isStop) 
        {
            if(isNext)
                nextImage();
            else
                prevImage();
            try
            {
                Thread.sleep(interval);
            }
            catch(InterruptedException _ex) { }
        }
    }

    public void changeInterval(int i)
    {
        interval = i;
    }

    public void requestStop()
    {
        isStop = true;
    }

    public void changeNext(boolean flag)
    {
        isNext = flag;
    }

    private void nextImage()
    {
        parent.imageNo_old = parent.imageNo;
        if(parent.imageNo < parent.NUM - 1)
            parent.imageNo++;
        else
            parent.imageNo = 0;
        parent.imageNo_S.setValue(parent.imageNo + 1);
        parent.imageNo_F.setText(String.valueOf(parent.imageNo + 1));
        parent.changeImageNo();
    }

    private void prevImage()
    {
        parent.imageNo_old = parent.imageNo;
        if(parent.imageNo > 0)
            parent.imageNo--;
        else
            parent.imageNo = parent.NUM - 1;
        parent.imageNo_S.setValue(parent.imageNo + 1);
        parent.imageNo_F.setText(String.valueOf(parent.imageNo + 1));
        parent.changeImageNo();
    }
}
