// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LoaderThread.java

package dicomviewer;

import java.applet.Applet;
import java.io.PrintStream;

// Referenced classes of package dicomviewer:
//            Viewer

public class LoaderThread extends Thread
{

    int debug_level;
    Viewer parent;
    int start;
    int end;
    boolean isInc;

    public LoaderThread(int i, int j, int k, Viewer viewer)
    {
        debug_level = 3;
        parent = viewer;
        start = i;
        end = i + j;
        isInc = i - k >= 0;
    }

    public void run()
    {
        if(isInc)
        {
            for(int i = start; i < end; i++)
            {
                if(parent.confirmStopRequest())
                {
                    if(debug_level > 3)
                        System.out.println(" Stoped!");
                    parent.changeStopRequest(false);
                    return;
                }
                if(debug_level > 3)
                    System.out.print(i);
                if(parent.index[i] == -1)
                {
                    parent.index[i] = -2;
                    if(debug_level > 3)
                        System.out.print(")");
                    parent.postData(i, start, end);
                }
                if(debug_level > 3)
                    System.out.print(" ");
            }

        } else
        {
            for(int j = end - 1; j >= start; j--)
            {
                if(parent.confirmStopRequest())
                {
                    if(debug_level > 3)
                        System.out.println(" Stoped!");
                    parent.changeStopRequest(false);
                    return;
                }
                if(debug_level > 3)
                    System.out.print(j);
                if(parent.index[j] == -1)
                {
                    parent.index[j] = -2;
                    if(debug_level > 3)
                        System.out.print(")");
                    parent.postData(j, start, end);
                }
                if(debug_level > 3)
                    System.out.print(" ");
            }

        }
        parent.showStatus("Dicom Data Load Done.");
        if(debug_level > 3)
            System.out.println();
        parent.changeStopRequest(false);
    }
}
