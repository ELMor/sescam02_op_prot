// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   ImageData.java

package dicomviewer;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.io.PrintStream;

// Referenced classes of package dicomviewer:
//            DicomData

public class ImageData
{

    int debug_level;
    boolean blackANDwhite;
    boolean rgbMode;
    boolean inv;
    int pixel[];
    int orgPixel[];
    int pixLength;
    int pixelMin;
    int pixelMax;
    int width;
    int height;
    int histogram[];
    int histMax;
    int ww;
    int wl;
    int defaultPixel[];
    int defaultWidth;
    int defaultHeight;
    int defaultWW;
    int defaultWL;
    Image image;
    Toolkit toolkit;
    MemoryImageSource source;

    public void setData(DicomData dicomdata)
    {
        if(debug_level > 3)
            System.out.println("Now set width and height....");
        width = Integer.parseInt(dicomdata.getAnalyzedValue("(0028,0011)"));
        height = Integer.parseInt(dicomdata.getAnalyzedValue("(0028,0010)"));
        defaultWidth = width;
        defaultHeight = height;
        if(debug_level > 3)
            System.out.println("Image width  : " + width);
        if(debug_level > 3)
            System.out.println("Image heigth : " + height);
        if(debug_level > 3)
            System.out.print("Now set byte[] to int[]....");
        orgPixel = new int[width * height];
        pixLength = orgPixel.length;
        pixel = new int[pixLength];
        defaultPixel = new int[pixLength];
        byte abyte0[] = new byte[dicomdata.getValue("(7fe0,0010)").length];
        System.arraycopy(dicomdata.getValue("(7fe0,0010)"), 0, abyte0, 0, abyte0.length);
        if(debug_level > 3)
            System.out.println(" OK!");
        if(dicomdata.isContain("(0028,0004)") && dicomdata.getAnalyzedValue("(0028,0004)").trim().equals("RGB"))
        {
            rgbMode = true;
            for(int i = 0; i < pixLength; i++)
                orgPixel[i] = 0xff000000 | (0xff & abyte0[3 * i]) << 16 | (0xff & abyte0[3 * i + 1]) << 8 | 0xff & abyte0[3 * i + 2];

        } else
        {
            if(dicomdata.getAnalyzedValue("(0028,0100)").trim().equals("16"))
            {
                boolean flag = false;
                for(int i1 = 0; i1 < pixLength; i1++)
                {
                    short word0 = (short)((0xff & abyte0[2 * i1 + 1]) << 8 | 0xff & abyte0[2 * i1]);
                    orgPixel[i1] = word0;
                }

            } else
            {
                for(int j = 0; j < pixLength; j++)
                    orgPixel[j] = 0xff & abyte0[j];

            }
            int k = Integer.parseInt(dicomdata.getAnalyzedValue("(0028,0101)"));
            int j1 = Integer.parseInt(dicomdata.getAnalyzedValue("(0028,0102)"));
            int l1 = (j1 - k) + 1;
            if(l1 > 0)
            {
                for(int i2 = 0; i2 < pixLength; i2++)
                    orgPixel[i2] = orgPixel[i2] >> l1;

            }
        }
        System.arraycopy(orgPixel, 0, defaultPixel, 0, pixLength);
        abyte0 = null;
        if(debug_level > 3)
            System.out.print("Now set pixelMin and pixelMax....");
        pixelMin = 0;
        pixelMax = 0;
        for(int l = 0; l < pixLength; l++)
        {
            if(pixelMin > orgPixel[l])
                pixelMin = orgPixel[l];
            if(pixelMax < orgPixel[l])
                pixelMax = orgPixel[l];
        }

        if(debug_level > 3)
            System.out.println(" OK!");
        if(debug_level > 3)
            System.out.print("Now set WW/WL....");
        if(dicomdata.isContain("(0028,1051)"))
            try
            {
                ww = Integer.parseInt(dicomdata.getAnalyzedValue("(0028,1051)").replace('+', ' ').trim());
            }
            catch(NumberFormatException _ex)
            {
                ww = pixelMax - pixelMin;
            }
        else
            ww = pixelMax - pixelMin;
        if(dicomdata.isContain("(0028,1050)"))
            try
            {
                wl = Integer.parseInt(dicomdata.getAnalyzedValue("(0028,1050)").replace('+', ' ').trim());
            }
            catch(NumberFormatException _ex)
            {
                wl = (ww >> 1) + pixelMin;
            }
        else
            wl = (ww >> 1) + pixelMin;
        defaultWW = ww;
        defaultWL = wl;
        if(debug_level > 3)
            System.out.println(" OK!");
        if(debug_level > 3)
            System.out.println("WW :" + ww + " WL :" + wl);
        for(int k1 = 0; k1 < pixel.length; k1++)
            pixel[k1] = 0xff000000;

        source = new MemoryImageSource(width, height, pixel, 0, width);
        source.setAnimated(true);
        toolkit = Toolkit.getDefaultToolkit();
        image = toolkit.createImage(source);
    }

    private void contrast()
    {
        if(!rgbMode)
        {
            int i = ww >> 1;
            int j = wl - i;
            int k = wl + i;
            if(blackANDwhite)
            {
                double d = 255D / (double)ww;
                for(int k1 = 0; k1 < pixLength; k1++)
                {
                    int i1 = orgPixel[k1];
                    if(i1 <= j)
                        i1 = 0;
                    else
                    if(i1 >= k)
                        i1 = 255;
                    else
                        i1 = (int)((double)(i1 - j) * d);
                    pixel[k1] = 0xff000000 | i1 << 16 | i1 << 8 | i1;
                    if(inv)
                        pixel[k1] = ~pixel[k1] & 0xffffff | pixel[k1] & 0xff000000;
                }

                return;
            }
            float f = 0.67F / (float)ww;
            int l = ww + j;
            for(int l1 = 0; l1 < pixLength; l1++)
            {
                int j1 = orgPixel[l1];
                float f1;
                if(j1 <= j)
                    f1 = 0.67F;
                else
                if(j1 >= k)
                    f1 = 0.0F;
                else
                    f1 = (float)(l - j1) * f;
                pixel[l1] = hue2RGB(f1);
                if(inv)
                    pixel[l1] = ~pixel[l1] & 0xffffff | pixel[l1] & 0xff000000;
            }

            return;
        } else
        {
            System.arraycopy(orgPixel, 0, pixel, 0, pixel.length);
            return;
        }
    }

    private int hue2RGB(float f)
    {
        int i = 0;
        int j = 0;
        int k = 0;
        float f1 = (f - (float)Math.floor(f)) * 6F;
        float f2 = f1 - (float)Math.floor(f1);
        float f3 = 1.0F - f2;
        switch((int)f1)
        {
        case 0: // '\0'
            i = 255;
            j = (int)(f2 * 255F + 0.5F);
            k = 0;
            break;

        case 1: // '\001'
            i = (int)(f3 * 255F + 0.5F);
            j = 255;
            k = 0;
            break;

        case 2: // '\002'
            i = 0;
            j = 255;
            k = (int)(f2 * 255F + 0.5F);
            break;

        case 3: // '\003'
            i = 0;
            j = (int)(f3 * 255F + 0.5F);
            k = 255;
            break;

        case 4: // '\004'
            i = (int)(f2 * 255F + 0.5F);
            j = 0;
            k = 255;
            break;

        case 5: // '\005'
            i = 255;
            j = 0;
            k = (int)(f3 * 255F + 0.5F);
            break;
        }
        return 0xff000000 | i << 16 | j << 8 | k;
    }

    private Image getImage()
    {
        source.newPixels();
        return image;
    }

    public Image getDefaultImage()
    {
        contrast();
        return getImage();
    }

    public boolean color()
    {
        return rgbMode;
    }

    public Image wwANDwl(int i, int j)
    {
        ww = i;
        wl = j;
        contrast();
        return getImage();
    }

    public void setWwWl(int i, int j)
    {
        ww = i;
        wl = j;
    }

    public Image getImageWWWL2Current(int i, int j)
    {
        ww = defaultWW + i;
        wl = defaultWL + j;
        contrast();
        return getImage();
    }

    public int getWW()
    {
        return ww;
    }

    public int getWL()
    {
        return wl;
    }

    public int getDefaultWW()
    {
        return defaultWW;
    }

    public int getDefaultWL()
    {
        return defaultWL;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public int getPixelMin()
    {
        return pixelMin;
    }

    public int getPixelMax()
    {
        return pixelMax;
    }

    public void inverse()
    {
        inv = !inv;
    }

    public void setInverse(boolean flag)
    {
        inv = flag;
    }

    public void setColor(boolean flag)
    {
        blackANDwhite = !flag;
    }

    public void changeColor()
    {
        blackANDwhite = !blackANDwhite;
    }

    public void setDefaultPixel()
    {
        System.arraycopy(defaultPixel, 0, orgPixel, 0, pixLength);
        width = defaultWidth;
        height = defaultHeight;
        blackANDwhite = true;
        inv = false;
    }

    public void rotateL()
    {
        int ai[] = new int[orgPixel.length];
        System.arraycopy(orgPixel, 0, ai, 0, ai.length);
        for(int j = 0; j < height; j++)
        {
            for(int k = 0; k < width; k++)
                orgPixel[(width - k - 1) * height + j] = ai[j * width + k];

        }

        int i = width;
        width = height;
        height = i;
    }

    public void rotateR()
    {
        int ai[] = new int[orgPixel.length];
        System.arraycopy(orgPixel, 0, ai, 0, ai.length);
        for(int j = 0; j < height; j++)
        {
            for(int k = 0; k < width; k++)
                orgPixel[k * height + (height - j - 1)] = ai[j * width + k];

        }

        int i = width;
        width = height;
        height = i;
    }

    public void flipLR()
    {
        int ai[] = new int[orgPixel.length];
        System.arraycopy(orgPixel, 0, ai, 0, ai.length);
        for(int k = 0; k < height; k++)
        {
            int i = k * width;
            int j = (i + width) - 1;
            for(int l = 0; l < width; l++)
                orgPixel[j - l] = ai[i + l];

        }

    }

    public void flipUD()
    {
        int ai[] = new int[orgPixel.length];
        System.arraycopy(orgPixel, 0, ai, 0, ai.length);
        for(int k = 0; k < height; k++)
        {
            int i = (height - k - 1) * width;
            int j = k * width;
            for(int l = 0; l < width; l++)
                orgPixel[i + l] = ai[j + l];

        }

    }

    private void makeHistogram()
    {
        for(int i = 0; i < 256; i++)
            histogram[i] = 0;

        histMax = 0;
        for(int j = 0; j < pixel.length; j++)
        {
            int k = 0xff & pixel[j];
            histogram[k]++;
        }

        for(int l = 0; l < 256; l++)
            if(histMax < histogram[l])
                histMax = histogram[l];

    }

    public int[] getHistogram()
    {
        return histogram;
    }

    public int getHistMax()
    {
        return histMax;
    }

    public Image reviseHistogram()
    {
        calcRevisedHistogram();
        return getImage();
    }

    private void calcRevisedHistogram()
    {
        double ad[] = new double[256];
        int i = 0;
        int ai[] = new int[256];
        makeHistogram();
        for(int j = 0; j < pixel.length; j++)
            pixel[j] = 0xff & pixel[j];

        double d = 1.0D / (double)(height * width);
        for(int k = 0; k < 256; k++)
        {
            i += histogram[k];
            ad[k] = (double)i * d;
        }

        double d1 = ad[0];
        double d2 = 255D / (1.0D - d1);
        for(int l = 0; l < 256; l++)
            ai[l] = (int)((ad[l] - d1) * d2);

        for(int i1 = 0; i1 < pixel.length; i1++)
            pixel[i1] = 0xff000000 | ai[pixel[i1]] << 16 | ai[pixel[i1]] << 8 | ai[pixel[i1]];

    }

    public ImageData()
    {
        debug_level = 3;
        blackANDwhite = true;
        rgbMode = false;
        inv = false;
        histogram = new int[256];
    }
}
