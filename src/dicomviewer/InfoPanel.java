// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   InfoPanel.java

package dicomviewer;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;

// Referenced classes of package dicomviewer:
//            DicomData

public class InfoPanel extends Panel
{

    DicomData dicomData;
    GridBagLayout layout;
    GridBagConstraints c;
    Label label1;
    Label id_L;
    Label id_F;
    Label name_L;
    Label name_F;
    Label age_L;
    Label age_F;
    Label sex_L;
    Label sex_F;
    Label label2;
    Label sid_L;
    Label sid_F;
    Label date_L;
    Label date_F;
    Label time_L;
    Label time_F;

    public InfoPanel()
    {
        layout = new GridBagLayout();
        c = new GridBagConstraints();
        label1 = new Label("Patient Info.");
        id_L = new Label("ID");
        id_F = new Label();
        name_L = new Label("Name");
        name_F = new Label();
        age_L = new Label("Age");
        age_F = new Label();
        sex_L = new Label("Sex");
        sex_F = new Label();
        label2 = new Label("Study Info.");
        sid_L = new Label("ID");
        sid_F = new Label();
        date_L = new Label("Date");
        date_F = new Label();
        time_L = new Label("Time");
        time_F = new Label();
        try
        {
            jbInit();
            return;
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void jbInit()
        throws Exception
    {
        setLayout(layout);
        c.fill = 2;
        c.weightx = 1.0D;
        c.weighty = 0.0D;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        layout.setConstraints(label1, c);
        add(label1);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        layout.setConstraints(id_F, c);
        add(id_F);
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 2;
        layout.setConstraints(name_F, c);
        add(name_F);
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        layout.setConstraints(age_F, c);
        add(age_F);
        c.gridx = 1;
        c.gridy = 3;
        c.gridwidth = 1;
        layout.setConstraints(sex_F, c);
        add(sex_F);
        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 2;
        layout.setConstraints(label2, c);
        add(label2);
        c.gridx = 0;
        c.gridy = 6;
        c.gridwidth = 2;
        layout.setConstraints(sid_F, c);
        add(sid_F);
        c.gridx = 0;
        c.gridy = 7;
        c.gridwidth = 2;
        layout.setConstraints(date_F, c);
        add(date_F);
        c.gridx = 0;
        c.gridy = 8;
        c.gridwidth = 2;
        layout.setConstraints(time_F, c);
        add(time_F);
    }

    public void setDicomData(DicomData dicomdata)
    {
        dicomData = dicomdata;
        setLabel(id_F, "(0010,0020)");
        setLabel(name_F, "(0010,0010)");
        setLabel(age_F, "(0010,1010)");
        setLabel(sex_F, "(0010,0040)");
        setLabel(sid_F, "(0020,0010)");
        setLabel(date_F, "(0008,0020)");
        setLabel(time_F, "(0008,0030)");
    }

    private void setLabel(Label label, String s)
    {
        if(dicomData.isContain(s))
        {
            label.setText(dicomData.getAnalyzedValue(s));
            label.setEnabled(true);
            return;
        } else
        {
            label.setText("none");
            label.setEnabled(false);
            return;
        }
    }
}
