// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DicomDic.java

package dicomviewer;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class DicomDic
{
    class Dicomvalue
    {

        String name;
        String vr;
        String vm;
        String version;

        Dicomvalue()
        {
        }
    }


    int debug_level;
    Hashtable table;

    public DicomDic(String s)
    {
        debug_level = 3;
        table = new Hashtable();
        if(debug_level > 3)
            System.out.println("Now Loading DicomDic from " + s + "...");
        try
        {
            if(debug_level > 3)
                System.out.println("Now Making Resive Stream....");
            java.io.InputStream inputstream;
            if(s.equals("none"))
            {
                inputstream = getClass().getResourceAsStream("Dicom.dic");
            } else
            {
                URL url = new URL(s);
                inputstream = url.openStream();
            }
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputstream));
            int i = 0;
            String s1;
            while((s1 = bufferedreader.readLine()) != null) 
                if(!s1.startsWith("#"))
                {
                    i++;
                    String s2 = null;
                    Dicomvalue dicomvalue = new Dicomvalue();
                    StringTokenizer stringtokenizer = new StringTokenizer(s1);
                    s2 = stringtokenizer.nextToken();
                    if(debug_level > 5)
                        System.out.println("Tag  : " + s2);
                    dicomvalue.vr = stringtokenizer.nextToken();
                    if(debug_level > 5)
                        System.out.println("VR   : " + dicomvalue.vr);
                    dicomvalue.name = stringtokenizer.nextToken();
                    if(debug_level > 5)
                        System.out.println("Name : " + dicomvalue.name);
                    dicomvalue.vm = stringtokenizer.nextToken();
                    if(debug_level > 5)
                        System.out.println("VM   : " + dicomvalue.vm);
                    dicomvalue.version = stringtokenizer.nextToken();
                    if(debug_level > 5)
                        System.out.println("Ver. : " + dicomvalue.version);
                    table.put(s2, dicomvalue);
                }
            bufferedreader.close();
            return;
        }
        catch(EOFException eofexception)
        {
            System.out.println("EOFException: " + eofexception.getMessage());
            return;
        }
        catch(IOException ioexception)
        {
            System.out.println("IOException: " + ioexception.getMessage());
            return;
        }
        catch(Exception exception)
        {
            System.out.println("Exception: " + exception.getMessage());
        }
    }

    public String getName(String s)
    {
        return ((Dicomvalue)table.get(s)).name;
    }

    public String getVR(String s)
    {
        return ((Dicomvalue)table.get(s)).vr;
    }

    public String getVM(String s)
    {
        return ((Dicomvalue)table.get(s)).vm;
    }

    public String getVersion(String s)
    {
        return ((Dicomvalue)table.get(s)).version;
    }

    public boolean isContain(String s)
    {
        return table.containsKey(s);
    }
}
