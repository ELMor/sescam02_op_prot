// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   TagInfoFrame.java

package dicomviewer;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

// Referenced classes of package dicomviewer:
//            DicomData, hGrid

public class TagInfoFrame extends Frame
{

    DicomData dicomData[];
    java.util.Vector vector8;
    java.util.Vector vector10;
    java.util.Vector vector18;
    java.util.Vector vector20;
    java.util.Vector vector28;
    java.util.Vector vectorOther;
    List tag_list;
    hGrid table;
    Panel panel1;
    Label label1;
    Choice imgNo_choice;

    public TagInfoFrame()
    {
        super("DICOM Tag Browser");
        tag_list = new List(5, false);
        table = new hGrid();
        panel1 = new Panel();
        label1 = new Label();
        imgNo_choice = new Choice();
        try
        {
            jbInit();
            return;
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void setTagInfoFrame(int i)
    {
        dicomData = new DicomData[i];
        for(int j = 0; j < i; j++)
            imgNo_choice.add("0");

    }

    public void setDicomData(DicomData dicomdata, int i, int j)
    {
        dicomData[i] = dicomdata;
        imgNo_choice.remove(i);
        imgNo_choice.insert(Integer.toString(j + 1), i);
    }

    private void jbInit()
        throws Exception
    {
        String as[] = new String[4];
        as[0] = "Tag";
        as[1] = "VR";
        as[2] = "Name";
        as[3] = "Value";
        table.setHead(as);
        tag_list.add("0008 - Identifying");
        tag_list.add("0010 - Patient");
        tag_list.add("0018 - Acquisition");
        tag_list.add("0020 - Relationship");
        tag_list.add("0028 - ImagePresentation");
        tag_list.add("otherz");
        tag_list.select(0);
        label1.setText("ImageNo");
        add(table, "Center");
        add(panel1, "North");
        panel1.add(label1, null);
        panel1.add(imgNo_choice, null);
        panel1.add(tag_list, null);
        tag_list.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent itemevent)
            {
                switch(tag_list.getSelectedIndex())
                {
                case 0: // '\0'
                    showTagInfo(vector8);
                    return;

                case 1: // '\001'
                    showTagInfo(vector10);
                    return;

                case 2: // '\002'
                    showTagInfo(vector18);
                    return;

                case 3: // '\003'
                    showTagInfo(vector20);
                    return;

                case 4: // '\004'
                    showTagInfo(vector28);
                    return;

                case 5: // '\005'
                    showTagInfo(vectorOther);
                    return;
                }
                showTagInfo(vector8);
            }

        });
        imgNo_choice.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent itemevent)
            {
                setVector();
                switch(tag_list.getSelectedIndex())
                {
                case 0: // '\0'
                    showTagInfo(vector8);
                    return;

                case 1: // '\001'
                    showTagInfo(vector10);
                    return;

                case 2: // '\002'
                    showTagInfo(vector18);
                    return;

                case 3: // '\003'
                    showTagInfo(vector20);
                    return;

                case 4: // '\004'
                    showTagInfo(vector28);
                    return;

                case 5: // '\005'
                    showTagInfo(vectorOther);
                    return;
                }
                showTagInfo(vector8);
            }

        });
    }

    public void setImageNo(int i)
    {
        imgNo_choice.select(Integer.toString(i + 1));
        setVector();
        tag_list.select(0);
        showTagInfo(vector8);
    }

    private void setVector()
    {
        int i = imgNo_choice.getSelectedIndex();
        vector8 = new java.util.Vector();
        vector10 = new java.util.Vector();
        vector18 = new java.util.Vector();
        vector20 = new java.util.Vector();
        vector28 = new java.util.Vector();
        vectorOther = new java.util.Vector();
        for(java.util.Enumeration enumeration = dicomData[i].keys(); enumeration.hasMoreElements();)
        {
            String s = enumeration.nextElement().toString();
            String as[] = new String[4];
            as[0] = s;
            as[1] = dicomData[i].getVR(s);
            StringBuffer stringbuffer = new StringBuffer(dicomData[i].getName(s));
            stringbuffer.setLength(30);
            as[2] = stringbuffer.toString().replace('\0', ' ');
            as[3] = dicomData[i].getAnalyzedValue(s);
            if(s.substring(1, 5).equals("0008"))
                vector8.addElement(as);
            else
            if(s.substring(1, 5).equals("0010"))
                vector10.addElement(as);
            else
            if(s.substring(1, 5).equals("0018"))
                vector18.addElement(as);
            else
            if(s.substring(1, 5).equals("0020"))
                vector20.addElement(as);
            else
            if(s.substring(1, 5).equals("0028"))
                vector28.addElement(as);
            else
                vectorOther.addElement(as);
        }

    }

    private void showTagInfo(java.util.Vector vector)
    {
        table.removeRows();
        for(int i = 0; i < vector.size(); i++)
            table.addRow((String[])vector.elementAt(i));

        table.sort(0);
    }


}
