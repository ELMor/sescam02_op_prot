// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   hGrid.java

package dicomviewer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.TextComponent;
import java.awt.TextField;
import java.util.Vector;

public class hGrid extends Panel
{

    Dimension dMinimum;
    Font fFont;
    FontMetrics fMetrics;
    Graphics gImage;
    Image iImage;
    int iWidth;
    int iHeight;
    int iRowHeadWidth;
    int iRowHeight;
    int iFirstRow;
    int iGridWidth;
    int iGridHeight;
    int iX;
    int iY;
    String sColHead[];
    Vector vData;
    Vector vRowHead;
    int iColWidth[];
    int iColCount;
    int iRowCount;
    Scrollbar sbHoriz;
    Scrollbar sbVert;
    int iSbWidth;
    int iSbHeight;
    boolean bDrag;
    int iXDrag;
    int iColDrag;
    TextField tEdit;
    boolean bEditing;
    int iSelectRow;
    int iSelectColumn;
    Component cCallback;
    int iEditX;
    int iEditY;
    int iEditWidth;

    public hGrid()
    {
        fFont = new Font("Dialog", 0, 12);
        iSelectRow = -1;
        setLayout(null);
        sbHoriz = new Scrollbar(0);
        add(sbHoriz);
        sbVert = new Scrollbar(1);
        add(sbVert);
        tEdit = new TextField();
        add(tEdit);
    }

    public void setMinimumSize(Dimension dimension)
    {
        dMinimum = dimension;
    }

    public void setCallback(Component component)
    {
        cCallback = component;
    }

    public void setBounds(int i, int j, int k, int l)
    {
        super.setBounds(i, j, k, l);
        iSbHeight = sbHoriz.getPreferredSize().height;
        iSbWidth = sbVert.getPreferredSize().width;
        iHeight = l - iSbHeight;
        iWidth = k - iSbWidth;
        sbHoriz.setBounds(0, iHeight, iWidth, iSbHeight);
        sbVert.setBounds(iWidth, 0, iSbWidth, iHeight);
        adjustScroll();
        iImage = null;
        showText();
        repaint();
    }

    public void setHead(String as[])
    {
        vData = new Vector();
        vRowHead = new Vector();
        iColCount = as.length;
        sColHead = new String[iColCount];
        iColWidth = new int[iColCount];
        for(int i = 0; i < iColCount; i++)
        {
            sColHead[i] = as[i];
            iColWidth[i] = 100;
        }

        iRowCount = 0;
    }

    public void addRow(String s, String as[])
    {
        vRowHead.addElement(s);
        if(as.length != iColCount)
            return;
        String as1[] = new String[iColCount];
        for(int i = 0; i < iColCount; i++)
            as1[i] = as[i];

        vData.addElement(as1);
        iRowCount++;
        adjustScroll();
        repaint();
    }

    public void addRow(String as[])
    {
        addRow("", as);
    }

    public void removeRows()
    {
        if(!vData.isEmpty())
        {
            vData.removeAllElements();
            iRowCount = 0;
            adjustScroll();
            repaint();
        }
    }

    private void adjustScroll()
    {
        if(iRowHeight == 0)
            return;
        int i = 0;
        for(int j = 0; j < iColCount; j++)
            i += iColWidth[j];

        iGridWidth = i;
        iGridHeight = iRowHeight * (iRowCount + 1);
        sbHoriz.setValues(iX, iWidth, 0, iGridWidth + iRowHeadWidth);
        int k = iY / iRowHeight;
        int l = iHeight / iRowHeight;
        sbVert.setValues(k, l, 0, iRowCount + 1);
        iX = sbHoriz.getValue();
        iY = iRowHeight * sbVert.getValue();
    }

    public boolean handleEvent(Event event)
    {
        switch(event.id)
        {
        case 601: // Event.SCROLL_LINE_UP
        case 602: // Event.SCROLL_LINE_DOWN
        case 603: // Event.SCROLL_PAGE_UP
        case 604: // Event.SCROLL_PAGE_DOWN
        case 605: // Event.SCROLL_ABSOLUTE
            iX = sbHoriz.getValue();
            iY = iRowHeight * sbVert.getValue();
            repaint();
            return true;
        }
        return super.handleEvent(event);
    }

    public void paint(Graphics g)
    {
        if(g == null)
            return;
        if(iWidth <= 0 || iHeight <= 0)
            return;
        g.setColor(Color.lightGray);
        g.fillRect(iWidth, iHeight, iSbWidth, iSbHeight);
        if(iImage == null)
        {
            iImage = createImage(iWidth, iHeight);
            gImage = iImage.getGraphics();
            if(fMetrics == null)
                fMetrics = gImage.getFontMetrics();
        }
        gImage.setFont(fFont);
        if(iRowHeight == 0)
        {
            iRowHeight = fMetrics.getHeight() + 2;
            for(int i = 0; i < iColCount; i++)
                calcAutoWidth(i);

            adjustScroll();
        }
        gImage.setColor(Color.white);
        gImage.fillRect(0, 0, iWidth, iHeight);
        gImage.setColor(Color.darkGray);
        gImage.drawLine(0, iRowHeight, iWidth, iRowHeight);
        int j = -iX + iRowHeadWidth;
        for(int l = 0; l < iColCount; l++)
        {
            int i1 = iColWidth[l];
            gImage.setColor(Color.lightGray);
            gImage.fillRect(j + 1, 0, i1 - 2, iRowHeight);
            gImage.setColor(Color.black);
            gImage.drawString(sColHead[l], j + 2, iRowHeight - 5);
            gImage.setColor(Color.darkGray);
            gImage.drawLine((j + i1) - 1, 0, (j + i1) - 1, iRowHeight - 1);
            gImage.setColor(Color.white);
            gImage.drawLine(j + i1, 0, j + i1, iRowHeight - 1);
            j += i1;
        }

        gImage.setColor(Color.lightGray);
        gImage.fillRect(0, 0, iRowHeadWidth + 1, iRowHeight);
        gImage.fillRect(j + 1, 0, iWidth - j, iRowHeight);
        gImage.drawLine(0, 0, 0, iRowHeight - 1);
        int j1 = (iRowHeight + 1) - iY;
        int l1 = 0;
        for(; j1 < iRowHeight + 1; j1 += iRowHeight)
            l1++;

        iFirstRow = l1;
        for(int k1 = iRowHeight + 1; k1 < iHeight && l1 < iRowCount; k1 += iRowHeight)
        {
            int k = -iX + iRowHeadWidth;
            for(int i2 = 0; i2 < iColCount; i2++)
            {
                int j2 = iColWidth[i2];
                Color color = Color.white;
                Color color1 = Color.black;
                if(iSelectRow == l1)
                {
                    color = Color.black;
                    color1 = Color.white;
                }
                gImage.setColor(color);
                gImage.fillRect(k, k1, j2 - 1, iRowHeight - 1);
                gImage.setColor(color1);
                gImage.drawString(getDisplay(i2, l1), k + 2, (k1 + iRowHeight) - 5);
                gImage.setColor(Color.lightGray);
                gImage.drawLine((k + j2) - 1, k1, (k + j2) - 1, (k1 + iRowHeight) - 1);
                gImage.drawLine(k, (k1 + iRowHeight) - 1, (k + j2) - 1, (k1 + iRowHeight) - 1);
                k += j2;
            }

            gImage.setColor(Color.white);
            gImage.fillRect(k, k1, iWidth - k, iRowHeight - 1);
            if(iRowHeadWidth != 0)
            {
                gImage.setColor(Color.lightGray);
                gImage.fillRect(0, k1, iRowHeadWidth, iRowHeight - 1);
                gImage.setColor(Color.black);
                String s = (String)vRowHead.elementAt(l1);
                gImage.drawString(s, 2, (k1 + iRowHeight) - 5);
                gImage.setColor(Color.darkGray);
                gImage.drawLine(0, (k1 + iRowHeight) - 1, iRowHeadWidth - 1, (k1 + iRowHeight) - 1);
                gImage.drawLine(iRowHeadWidth - 1, k1, iRowHeadWidth - 1, (k1 + iRowHeight) - 1);
                gImage.setColor(Color.white);
                gImage.drawLine(0, k1, iRowHeadWidth - 2, k1);
            }
            l1++;
        }

        g.drawImage(iImage, 0, 0, this);
        showText();
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public boolean keyDown(Event event, int i)
    {
        int j = iSelectRow;
        int k = iSelectColumn;
        if(j < 0 || j >= iRowCount)
            return false;
        switch(i)
        {
        case 9: // '\t'
            if((event.modifiers & 1) != 0)
                k--;
            else
                k++;
            if(k < 0)
                k = 0;
            if(k >= iColCount)
                k = iColCount - 1;
            select(j, k);
            return true;

        case 1004: 
            if(j > 0)
                select(j - 1, k);
            return true;

        case 10: // '\n'
        case 1005: 
            if(j < iRowCount - 1)
                select(j + 1, k);
            return true;

        case 27: // '\033'
            iSelectRow = -1;
            select(j, k);
            repaint();
            return true;
        }
        return false;
    }

    public boolean mouseMove(Event event, int i, int j)
    {
        if(j <= iRowHeight)
        {
            int k = i;
            i = (i + iX) - iGridWidth - iRowHeadWidth;
            int l;
            for(l = iColCount - 1; l >= 0; l--)
            {
                if(i > -7 && i < 7)
                    break;
                i += iColWidth[l];
            }

            if(l >= 0)
            {
                if(!bDrag)
                {
                    setCursor(new Cursor(11));
                    bDrag = true;
                    iXDrag = k - iColWidth[l];
                    iColDrag = l;
                }
                return true;
            }
        }
        return mouseExit(event, i, j);
    }

    public boolean mouseDrag(Event event, int i, int j)
    {
        if(bDrag && i < iWidth)
        {
            int k = i - iXDrag;
            if(k < 0)
                k = 0;
            iColWidth[iColDrag] = k;
            adjustScroll();
            repaint();
        }
        return true;
    }

    public boolean mouseExit(Event event, int i, int j)
    {
        if(bDrag)
        {
            setCursor(new Cursor(0));
            bDrag = false;
        }
        return true;
    }

    public boolean mouseDown(Event event, int i, int j)
    {
        if(iRowHeight == 0 || i > iWidth || j > iHeight)
            return true;
        i += iX - iRowHeadWidth;
        int k = 0;
        for(int l = 0; l < iColCount; l++)
        {
            int i1 = iColWidth[l];
            if(i >= 0 && i <= i1)
            {
                k = l;
                break;
            }
            i -= i1;
        }

        if(j > iRowHeight)
        {
            int j1 = (j / iRowHeight - 1) + iFirstRow;
            if(j1 >= 0 && j1 < iRowCount)
            {
                select(j1, k);
                if(cCallback != null)
                {
                    Event event1 = new Event(cCallback, 1001, String.valueOf(iSelectRow));
                    cCallback.action(event1, String.valueOf(iSelectRow));
                }
            }
        } else
        if(!bDrag)
            sort(k);
        return true;
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public Dimension getgetPreferredSize()
    {
        return getMinimumSize();
    }

    public Dimension getgetMinimumSize()
    {
        return getMinimumSize();
    }

    public Dimension getMinimumSize()
    {
        return dMinimum;
    }

    private void calcAutoWidth(int i)
    {
        int j = 10;
        iRowHeadWidth = 0;
        j = Math.max(j, fMetrics.stringWidth(sColHead[i]));
        for(int k = 0; k < iRowCount; k++)
        {
            String as[] = (String[])vData.elementAt(k);
            j = Math.max(j, fMetrics.stringWidth(as[i]));
            String s = (String)vRowHead.elementAt(k);
            iRowHeadWidth = Math.max(iRowHeadWidth, fMetrics.stringWidth(s) + 6);
        }

        iColWidth[i] = j + 6;
    }

    public void sort(int i)
    {
        select(-1, 0);
        int j = iRowCount;
        int k;
        for(k = 1; k < j && get(i, k - 1).compareTo(get(i, k)) <= 0; k++);
        if(k == j)
            return;
        for(int l = (j >> 1) - 1; l > 0; l--)
            siftup(l, j - 1, i);

        for(int i1 = j - 1; i1 > 0; i1--)
        {
            siftup(0, i1, i);
            String as[] = (String[])vData.elementAt(0);
            vData.setElementAt(vData.elementAt(i1), 0);
            vData.setElementAt(as, i1);
        }

    }

    private void siftup(int i, int j, int k)
    {
        Object obj = vData.elementAt(i);
        String s = get(k, i);
        int l = i;
        int i1;
        for(i1 = i + i + 1; i1 < j; i1 = (i = i1) + i1 + 1)
        {
            if(get(k, i1).compareTo(get(k, i1 + 1)) < 0)
                i1++;
            vData.setElementAt(vData.elementAt(i1), i);
            i = i1;
        }

        if(i1 == j)
        {
            vData.setElementAt(vData.elementAt(i1), i);
            i = i1;
        }
        for(int j1 = i - 1 >> 1; j1 >= l && get(k, j1).compareTo(s) < 1; j1 = (i = j1) - 1 >> 1)
            vData.setElementAt(vData.elementAt(j1), i);

        vData.setElementAt(obj, i);
    }

    private void select(int i, int j)
    {
        if(iSelectRow >= 0 && iSelectRow < iRowCount)
            set(iSelectColumn, iSelectRow, tEdit.getText());
        iSelectRow = i;
        iSelectColumn = j;
        if(i < 0 || i >= iRowCount || j < 0 || j >= iColCount)
        {
            iSelectRow = -1;
            iSelectColumn = 0;
        }
        tEdit.setVisible(false);
        bEditing = false;
        if(iSelectRow >= 0)
        {
            while(iSelectRow < iFirstRow && iFirstRow > 0) 
            {
                iFirstRow--;
                iY -= iRowHeight;
            }
            while(iSelectRow > (iFirstRow - 2) + iHeight / iRowHeight && iFirstRow < iRowCount - 1) 
            {
                iFirstRow++;
                iY += iRowHeight;
            }
            calcEditPos();
            if(iEditX > iWidth)
            {
                iX += iEditX - iWidth;
                calcEditPos();
            }
            if(iEditX + iEditWidth > iWidth)
            {
                iX += (iEditX + iEditWidth) - iWidth;
                calcEditPos();
            }
            if(iEditX < iRowHeadWidth)
            {
                iX += iEditX - iRowHeadWidth;
                calcEditPos();
            }
            tEdit.setText(get(iSelectColumn, iSelectRow));
            adjustScroll();
        }
        repaint();
    }

    private void calcEditPos()
    {
        int i = -iX;
        for(int j = 0; j < iSelectColumn; j++)
        {
            int k = iColWidth[j];
            i += k;
        }

        iEditY = ((iSelectRow - iFirstRow) + 1) * iRowHeight;
        int l = 0;
        if(iColWidth != null)
            l = iColWidth[iSelectColumn];
        iEditX = i + iRowHeadWidth;
        iEditWidth = l;
    }

    private void showText()
    {
        calcEditPos();
        int i = iEditX;
        int j = iEditY;
        int k = iEditWidth;
        k = Math.min(k, iWidth - i);
        if(i < 0)
        {
            k += i;
            i = 0;
        }
        if(iSelectRow >= 0 && k > 0 && j >= iRowHeight && j < iHeight - iRowHeight)
        {
            tEdit.setBounds(i, j, k, iRowHeight);
            if(!bEditing)
            {
                tEdit.setVisible(true);
                tEdit.requestFocus();
                tEdit.selectAll();
                bEditing = true;
                return;
            }
        } else
        if(bEditing)
        {
            tEdit.setVisible(false);
            bEditing = false;
        }
    }

    private String getDisplay(int i, int j)
    {
        return ((String[])vData.elementAt(j))[i];
    }

    private String get(int i, int j)
    {
        return ((String[])vData.elementAt(j))[i];
    }

    private void set(int i, int j, String s)
    {
        String as[] = (String[])vData.elementAt(j);
        as[i] = s;
    }
}
