// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   Viewer.java

package dicomviewer;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.Scrollbar;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;

// Referenced classes of package dicomviewer:
//            AnimationThread, BorderPanel, DicomData, DicomDic,
//            DicomFile, ImageData, ImageTiledCanvas, InfoPanel,
//            LoaderThread, MyLabel, TagInfoFrame

public class Viewer extends Applet
{
    class MyCheckBoxListener
        implements ItemListener
    {

        public void itemStateChanged(ItemEvent itemevent)
        {
            if(wwwlSingle_C.getState())
                synchro_flag = false;
            else
            if(wwwlALL_C.getState())
                synchro_flag = true;
            imageTiledCanvas.setMoveState(move_C.getState());
            imageTiledCanvas.setZoomState(zoom_C.getState());
            imageTiledCanvas.setLoupeState(loupe_C.getState());
        }

        MyCheckBoxListener()
        {
        }
    }

    class MyKeyListener
        implements KeyListener
    {

        boolean isWW;

        public void keyTyped(KeyEvent keyevent)
        {
        }

        public void keyPressed(KeyEvent keyevent)
        {
            int i = keyevent.getKeyCode();
            if(keyevent.isControlDown())
            {
                if(i == 87)
                {
                    isWW = false;
                    if(keyevent.isShiftDown())
                    {
                        changeSelectCheckBox(wwwlSingle_C);
                        return;
                    } else
                    {
                        changeSelectCheckBox(wwwlALL_C);
                        return;
                    }
                }
                if(i == 81)
                {
                    isWW = true;
                    if(keyevent.isShiftDown())
                    {
                        changeSelectCheckBox(wwwlSingle_C);
                        return;
                    } else
                    {
                        changeSelectCheckBox(wwwlALL_C);
                        return;
                    }
                }
                if(i == 79)
                {
                    changeSelectCheckBox(move_C);
                    return;
                }
                if(i == 90)
                {
                    changeSelectCheckBox(zoom_C);
                    return;
                }
                if(i == 85)
                {
                    changeSelectCheckBox(loupe_C);
                    return;
                }
                if(i == 68)
                {
                    defaultWwWl();
                    return;
                }
                if(i == 73)
                {
                    if(keyevent.isShiftDown())
                    {
                        studyInfo_C.setState(!studyInfo_C.getState());
                        imageTiledCanvas.setStudyInfo_flag(studyInfo_C.getState());
                        return;
                    }
                    if(notCine)
                    {
                        inv_flag = true;
                        showTile();
                        inv_flag = false;
                        return;
                    } else
                    {
                        cineNext1_B.setFont(plain);
                        cineNext2_B.setFont(plain);
                        cinePrev1_B.setFont(bold);
                        cinePrev2_B.setFont(plain);
                        animationThread.changeInterval(1000);
                        animationThread.changeNext(false);
                        return;
                    }
                }
                if(i == 84)
                {
                    if(keyevent.isShiftDown())
                        if(!tagInfoFrame.isShowing())
                        {
                            tag_B.setLabel("Hide  Tag Info");
                            tagInfoFrame.setImageNo(imageNo);
                            tagInfoFrame.setVisible(true);
                            return;
                        } else
                        {
                            tag_B.setLabel("Show  Tag Info");
                            tagInfoFrame.setVisible(false);
                            return;
                        }
                    if(notCine)
                    {
                        getCanvasSize();
                        return;
                    } else
                    {
                        cineNext1_B.setFont(bold);
                        cineNext2_B.setFont(plain);
                        cinePrev1_B.setFont(plain);
                        cinePrev2_B.setFont(plain);
                        animationThread.changeInterval(1000);
                        animationThread.changeNext(true);
                        return;
                    }
                }
                if(i == 82)
                {
                    if(keyevent.isShiftDown())
                    {
                        rotateR_flag = true;
                        showTile();
                        rotateR_flag = false;
                        return;
                    }
                    if(notCine)
                    {
                        rotateL_flag = true;
                        showTile();
                        rotateL_flag = false;
                        return;
                    } else
                    {
                        cineNext1_B.setFont(plain);
                        cineNext2_B.setFont(plain);
                        cinePrev1_B.setFont(plain);
                        cinePrev2_B.setFont(bold);
                        animationThread.changeInterval(300);
                        animationThread.changeNext(false);
                        return;
                    }
                }
                if(i == 70)
                {
                    if(keyevent.isShiftDown())
                    {
                        flipLR_flag = true;
                        showTile();
                        flipLR_flag = false;
                        return;
                    }
                    if(notCine)
                    {
                        flipUD_flag = true;
                        showTile();
                        flipUD_flag = false;
                        return;
                    } else
                    {
                        cineNext1_B.setFont(plain);
                        cineNext2_B.setFont(bold);
                        cinePrev1_B.setFont(plain);
                        cinePrev2_B.setFont(plain);
                        animationThread.changeInterval(300);
                        animationThread.changeNext(true);
                        return;
                    }
                }
                if(i == 67)
                {
                    if(keyevent.isShiftDown())
                    {
                        cineMode();
                        return;
                    }
                    if(notCine)
                    {
                        reset_flag = true;
                        showTile();
                        reset_flag = false;
                        return;
                    } else
                    {
                        cineMode();
                        return;
                    }
                }
                if(i == 77)
                {
                    if(more_B.isEnabled())
                    {
                        moreFrame();
                        return;
                    }
                } else
                if(i == 76)
                {
                    if(less_B.isEnabled())
                    {
                        lessFrame();
                        return;
                    }
                } else
                {
                    if(i == 49)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(1.0D);
                                return;
                            } else
                            {
                                key_changeZoom(1.0D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(0.0D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.0D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, 0.0D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.0D);
                            return;
                        }
                    }
                    if(i == 50)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.90000000000000002D);
                                return;
                            } else
                            {
                                key_changeZoom(1.2D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.01D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.01D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.01D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.01D);
                            return;
                        }
                    }
                    if(i == 51)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.80000000000000004D);
                                return;
                            } else
                            {
                                key_changeZoom(1.3999999999999999D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.02D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.02D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.02D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.02D);
                            return;
                        }
                    }
                    if(i == 52)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.69999999999999996D);
                                return;
                            } else
                            {
                                key_changeZoom(1.6000000000000001D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.029999999999999999D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.029999999999999999D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.029999999999999999D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.029999999999999999D);
                            return;
                        }
                    }
                    if(i == 53)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.59999999999999998D);
                                return;
                            } else
                            {
                                key_changeZoom(1.8D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.050000000000000003D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.050000000000000003D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.050000000000000003D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.050000000000000003D);
                            return;
                        }
                    }
                    if(i == 54)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.5D);
                                return;
                            } else
                            {
                                key_changeZoom(2D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.10000000000000001D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.10000000000000001D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.10000000000000001D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.10000000000000001D);
                            return;
                        }
                    }
                    if(i == 55)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.40000000000000002D);
                                return;
                            } else
                            {
                                key_changeZoom(2.2000000000000002D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.20000000000000001D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.20000000000000001D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.20000000000000001D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.20000000000000001D);
                            return;
                        }
                    }
                    if(i == 56)
                    {
                        if(zoom_C.getState())
                            if(keyevent.isShiftDown())
                            {
                                key_changeZoom(0.29999999999999999D);
                                return;
                            } else
                            {
                                key_changeZoom(2.3999999999999999D);
                                return;
                            }
                        if(!wwwlSingle_C.getState() && !wwwlALL_C.getState())
                            changeSelectCheckBox(wwwlALL_C);
                        if(isWW)
                            if(keyevent.isShiftDown())
                            {
                                key_changeWwWl(-0.40000000000000002D, 0.0D);
                                return;
                            } else
                            {
                                key_changeWwWl(0.40000000000000002D, 0.0D);
                                return;
                            }
                        if(keyevent.isShiftDown())
                        {
                            key_changeWwWl(0.0D, -0.40000000000000002D);
                            return;
                        } else
                        {
                            key_changeWwWl(0.0D, 0.40000000000000002D);
                            return;
                        }
                    }
                    if(i == 57)
                    {
                        if(imageNo > 0)
                        {
                            imageNo_old = imageNo;
                            imageNo--;
                            imageNo_S.setValue(imageNo + 1);
                            imageNo_F.setText(String.valueOf(imageNo + 1));
                            changeImageNo();
                            return;
                        }
                    } else
                    if(i == 48 && imageNo < NUM - 1)
                    {
                        imageNo_old = imageNo;
                        imageNo++;
                        imageNo_S.setValue(imageNo + 1);
                        imageNo_F.setText(String.valueOf(imageNo + 1));
                        changeImageNo();
                    }
                }
            }
        }

        public void keyReleased(KeyEvent keyevent)
        {
            keyevent.getKeyCode();
        }

        MyKeyListener()
        {
            isWW = false;
        }
    }


    int debug_level;
    boolean isLtlEndian;
    boolean vrType;
    boolean privacy;
    int NUM;
    String dicURL;
    String imgURL[];
    DicomFile dicomFile;
    DicomData dicomData;
    ImageData imageData;
    DicomData dicomData_tmp[];
    ImageData imageData_tmp[];
    int index[];
    int TMPSIZE;
    int arrayIndex[];
    int start;
    int start_old;
    int ww[];
    int wl[];
    double zoom;
    int canvasSize;
    boolean inv_flag;
    boolean rotateL_flag;
    boolean rotateR_flag;
    boolean flipLR_flag;
    boolean flipUD_flag;
    boolean reset_flag;
    int imageNo;
    boolean synchro_flag;
    int imageNo_old;
    LoaderThread loader;
    boolean isThreadStarted;
    boolean requestStop;
    ImageTiledCanvas imageTiledCanvas;
    int row;
    int column;
    TagInfoFrame tagInfoFrame;
    int tile_start;
    int width;
    int height;
    AnimationThread animationThread;
    boolean notCine;
    MyCheckBoxListener myCheckBoxListener;
    MyKeyListener myKeyListener;
    Font bold;
    Font plain;
    BorderLayout borderLayout1;
    GridBagLayout gridBagLayout1;
    GridBagLayout gridBagLayout2;
    GridBagConstraints gridBagConstraints1;
    GridBagConstraints gridBagConstraints2;
    ScrollPane scrollPane1;
    Panel panel1;
    Panel controlPanel;
    BorderPanel borderPanel1;
    Panel mousePanel;
    InfoPanel infoPanel;
    Panel buttonPanel;
    Panel copyrightPanel;
    Label label1;
    Label label2;
    MyLabel copyright_L;
    TextField imageNo_F;
    Scrollbar imageNo_S;
    CheckboxGroup checkboxGroup1;
    Checkbox wwwlSingle_C;
    Checkbox wwwlALL_C;
    Checkbox move_C;
    Checkbox zoom_C;
    Checkbox loupe_C;
    Checkbox studyInfo_C;
    Button fit_B;
    Button default_B;
    Button less_B;
    Button more_B;
    Button tag_B;
    Button inv_B;
    Button rotateL_B;
    Button rotateR_B;
    Button flipLR_B;
    Button flipUD_B;
    Button reset_B;
    Button cine_B;
    Button cineNext1_B;
    Button cinePrev1_B;
    Button cineNext2_B;
    Button cinePrev2_B;

    public String getParameter(String s, String s1)
    {
        String s2 = getParameter(s) == null ? s1 : getParameter(s);
        if(debug_level > 3)
            System.out.println("Parameter " + s + " is " + s2);
        return s2;
    }

    public String[][] getParameterInfo()
    {
        String as[][] = {
            {
                "isLtlEndian", "boolean", "\u8EE2\u9001\u69CB\u6587\u3002LittleEndian\u306A\u3089true"
            }, {
                "vrType", "boolean", "VR\u306E\u7A2E\u985E\u3002\u660E\u793A\u7684VR\u306A\u3089true"
            }, {
                "patientPrivacy", "boolean", "\u60A3\u8005\u30D7\u30E9\u30A4\u30D0\u30B7\u30FC\u4FDD\u8B77\u306E\u305F\u3081\u60A3\u8005\u540D\u5909\u63DB\u3059\u308B\u3068\u304Dtrue"
            }, {
                "tmpSize", "int", "\u30AD\u30E3\u30C3\u30B7\u30E5\u3055\u308C\u308B\u6700\u5927\u753B\u50CF\u679A\u6570(\u5076\u6570)"
            }, {
                "NUM", "int", "\u753B\u50CF\u679A\u6570"
            }, {
                "currentNo", "int", "\u6700\u521D\u306B\u898B\u308B\u753B\u50CF\u756A\u53F7 (0-?)"
            }, {
                "dicURL", "String", "\u8F9E\u66F8URL"
            }, {
                "imgURL", "String", "\u753B\u50CFURL"
            }
        };
        return as;
    }

    public Viewer()
    {
        debug_level = 300;
        zoom = 1.0D;
        canvasSize = 100;
        inv_flag = false;
        rotateL_flag = false;
        rotateR_flag = false;
        flipLR_flag = false;
        flipUD_flag = false;
        reset_flag = false;
        synchro_flag = true;
        requestStop = false;
        row = 1;
        column = 1;
        notCine = true;
        myCheckBoxListener = new MyCheckBoxListener();
        myKeyListener = new MyKeyListener();
        bold = new Font("Dialog", 1, 12);
        plain = new Font("Dialog", 0, 12);
        borderLayout1 = new BorderLayout();
        gridBagLayout1 = new GridBagLayout();
        gridBagLayout2 = new GridBagLayout();
        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints2 = new GridBagConstraints();
        scrollPane1 = new ScrollPane();
        panel1 = new Panel();
        controlPanel = new Panel();
        borderPanel1 = new BorderPanel("Mouse Manupilation");
        mousePanel = new Panel();
        infoPanel = new InfoPanel();
        buttonPanel = new Panel();
        copyrightPanel = new Panel();
        label1 = new Label();
        label2 = new Label("ImageNo");
        copyright_L = new MyLabel("Copyright (C) 2000 Nagoya Institute of Technology, Iwata lab. & KtC");
        imageNo_F = new TextField(3);
        imageNo_S = new Scrollbar(0);
        checkboxGroup1 = new CheckboxGroup();
        wwwlSingle_C = new Checkbox("WL/WW(Single)", false, checkboxGroup1);
        wwwlALL_C = new Checkbox("WL/WW(All images)", true, checkboxGroup1);
        move_C = new Checkbox("Move", false, checkboxGroup1);
        zoom_C = new Checkbox("Zoom", false, checkboxGroup1);
        loupe_C = new Checkbox("Loupe", false, checkboxGroup1);
        studyInfo_C = new Checkbox("Annotation", true);
        fit_B = new Button("Reset Move/Zoom");
        default_B = new Button("Default WL/WW");
        less_B = new Button("LessFrame");
        more_B = new Button("MoreFrame");
        tag_B = new Button("Show Tag Info");
        inv_B = new Button("Reverse");
        rotateL_B = new Button("Rotate L");
        rotateR_B = new Button("Rotate R");
        flipLR_B = new Button("Flip RL");
        flipUD_B = new Button("Flip UD");
        reset_B = new Button("Reset Angle");
        cine_B = new Button("Cine Mode");
        cineNext1_B = new Button("->");
        cinePrev1_B = new Button("<-");
        cineNext2_B = new Button(">>");
        cinePrev2_B = new Button("<<");
    }

    public void init()
    {
        if(debug_level > 3)
            System.out.println("Now Loading Parameter....");
        try
        {
            isLtlEndian = Boolean.valueOf(getParameter("isLtlEndian", "true")).booleanValue();
            vrType = Boolean.valueOf(getParameter("vrType", "false")).booleanValue();
            privacy = Boolean.valueOf(getParameter("patientPrivacy", "false")).booleanValue();
            TMPSIZE = Integer.parseInt(getParameter("tmpSize", "10"));
            NUM = Integer.parseInt(getParameter("NUM", "1"));
            imageNo = Integer.parseInt(getParameter("currentNo", "0"));
            dicURL = getParameter("dicURL", "none");
            imgURL = new String[NUM];
            for(int i = 0; i < NUM; i++)
                imgURL[i] = getParameter("imgURL" + i, "");

            jbInit();
            return;
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void jbInit()
        throws Exception
    {
        isThreadStarted = false;
        setLayout(borderLayout1);
        controlPanel.setLayout(gridBagLayout1);
        mousePanel.setLayout(gridBagLayout2);
        imageNo_F.setText(String.valueOf(imageNo + 1));
        imageNo_S.setValues(imageNo + 1, 0, 1, NUM + 1);
        less_B.setEnabled(false);
        cineNext1_B.setFont(bold);
        cineNext2_B.setFont(plain);
        cinePrev1_B.setFont(plain);
        cinePrev2_B.setFont(plain);
        cineNext1_B.setEnabled(false);
        cineNext2_B.setEnabled(false);
        cinePrev1_B.setEnabled(false);
        cinePrev2_B.setEnabled(false);
        panel1.setLayout(new BorderLayout());
        add(scrollPane1, "Center");
        add(panel1, "West");
        add(copyrightPanel, "South");
        copyright_L.setAppletContext(getAppletContext());
        copyrightPanel.setLayout(new FlowLayout(2, 5, 0));
        copyrightPanel.add(copyright_L);
        buttonPanel.setLayout(new GridLayout(2, 1));
        buttonPanel.add(studyInfo_C);
        buttonPanel.add(tag_B);
        panel1.add(buttonPanel, "South");
        panel1.add(infoPanel, "Center");
        gridBagConstraints2.fill = 1;
        add2mousePanel(0, 0, 6, wwwlALL_C);
        add2mousePanel(0, 1, 6, wwwlSingle_C);
        add2mousePanel(0, 2, 1, label1);
        add2mousePanel(1, 2, 5, default_B);
        add2mousePanel(0, 3, 1, label1);
        add2mousePanel(1, 3, 5, inv_B);
        add2mousePanel(0, 4, 6, move_C);
        add2mousePanel(0, 5, 6, zoom_C);
        add2mousePanel(0, 6, 1, label1);
        add2mousePanel(1, 6, 5, fit_B);
        add2mousePanel(0, 7, 6, loupe_C);
        borderPanel1.add(mousePanel);
        panel1.add(controlPanel, "North");
        gridBagConstraints1.fill = 1;
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.gridwidth = 4;
        gridBagConstraints1.gridheight = 9;
        gridBagConstraints1.insets = new Insets(3, 3, 0, 3);
        gridBagLayout1.setConstraints(borderPanel1, gridBagConstraints1);
        controlPanel.add(borderPanel1);
        gridBagConstraints1.insets = new Insets(0, 0, 0, 0);
        gridBagConstraints1.gridheight = 1;
        int i = 9;
        add2controlPanel(0, i, 4, reset_B);
        i++;
        add2controlPanel(0, i, 2, rotateL_B);
        add2controlPanel(2, i, 2, rotateR_B);
        i++;
        add2controlPanel(0, i, 2, flipLR_B);
        add2controlPanel(2, i, 2, flipUD_B);
        i++;
        gridBagConstraints1.insets = new Insets(5, 0, 0, 0);
        add2controlPanel(0, i, 4, cine_B);
        i++;
        gridBagConstraints1.insets = new Insets(0, 0, 0, 0);
        add2controlPanel(0, i, 1, cinePrev2_B);
        add2controlPanel(1, i, 1, cinePrev1_B);
        add2controlPanel(2, i, 1, cineNext1_B);
        add2controlPanel(3, i, 1, cineNext2_B);
        i++;
        gridBagConstraints1.insets = new Insets(5, 0, 0, 0);
        add2controlPanel(0, i, 2, label2);
        add2controlPanel(2, i, 2, imageNo_F);
        i++;
        gridBagConstraints1.insets = new Insets(0, 0, 0, 0);
        add2controlPanel(0, i, 4, imageNo_S);
        i++;
        add2controlPanel(0, i, 2, less_B);
        add2controlPanel(2, i, 2, more_B);
        i++;
        scrollPane1.setBackground(Color.black);
        tagInfoFrame = new TagInfoFrame();
        tagInfoFrame.setSize(500, 400);
        tagInfoFrame.setResizable(true);
        imageNo_F.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                imageNo_S.setValue((int)getFieldValue(imageNo_F));
                imageNo_old = imageNo;
                imageNo = imageNo_S.getValue() - 1;
                changeImageNo();
            }

        });
        imageNo_F.addKeyListener(myKeyListener);
        imageNo_S.addAdjustmentListener(new AdjustmentListener() {

            public void adjustmentValueChanged(AdjustmentEvent adjustmentevent)
            {
                imageNo_F.setText(String.valueOf(imageNo_S.getValue()));
                imageNo_old = imageNo;
                imageNo = imageNo_S.getValue() - 1;
                changeImageNo();
            }

        });
        imageNo_S.addKeyListener(myKeyListener);
        wwwlSingle_C.addItemListener(myCheckBoxListener);
        wwwlSingle_C.addKeyListener(myKeyListener);
        wwwlALL_C.addItemListener(myCheckBoxListener);
        wwwlALL_C.addKeyListener(myKeyListener);
        move_C.addItemListener(myCheckBoxListener);
        move_C.addKeyListener(myKeyListener);
        zoom_C.addItemListener(myCheckBoxListener);
        zoom_C.addKeyListener(myKeyListener);
        loupe_C.addItemListener(myCheckBoxListener);
        loupe_C.addKeyListener(myKeyListener);
        studyInfo_C.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent itemevent)
            {
                imageTiledCanvas.setStudyInfo_flag(studyInfo_C.getState());
            }

        });
        studyInfo_C.addKeyListener(myKeyListener);
        fit_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                getCanvasSize();
            }

        });
        fit_B.addKeyListener(myKeyListener);
        default_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                defaultWwWl();
            }

        });
        default_B.addKeyListener(myKeyListener);
        more_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                moreFrame();
            }

        });
        more_B.addKeyListener(myKeyListener);
        less_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                lessFrame();
            }

        });
        less_B.addKeyListener(myKeyListener);
        cine_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                cineMode();
            }

        });
        cine_B.addKeyListener(myKeyListener);
        cineNext1_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                cineNext1_B.setFont(bold);
                cineNext2_B.setFont(plain);
                cinePrev1_B.setFont(plain);
                cinePrev2_B.setFont(plain);
                animationThread.changeInterval(1000);
                animationThread.changeNext(true);
            }

        });
        cineNext1_B.addKeyListener(myKeyListener);
        cinePrev1_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                cineNext1_B.setFont(plain);
                cineNext2_B.setFont(plain);
                cinePrev1_B.setFont(bold);
                cinePrev2_B.setFont(plain);
                animationThread.changeInterval(1000);
                animationThread.changeNext(false);
            }

        });
        cinePrev1_B.addKeyListener(myKeyListener);
        cineNext2_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                cineNext1_B.setFont(plain);
                cineNext2_B.setFont(bold);
                cinePrev1_B.setFont(plain);
                cinePrev2_B.setFont(plain);
                animationThread.changeInterval(300);
                animationThread.changeNext(true);
            }

        });
        cineNext2_B.addKeyListener(myKeyListener);
        cinePrev2_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                cineNext1_B.setFont(plain);
                cineNext2_B.setFont(plain);
                cinePrev1_B.setFont(plain);
                cinePrev2_B.setFont(bold);
                animationThread.changeInterval(300);
                animationThread.changeNext(false);
            }

        });
        cinePrev2_B.addKeyListener(myKeyListener);
        inv_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                inv_flag = true;
                showTile();
                inv_flag = false;
            }

        });
        inv_B.addKeyListener(myKeyListener);
        rotateL_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                rotateL_flag = true;
                showTile();
                rotateL_flag = false;
            }

        });
        rotateL_B.addKeyListener(myKeyListener);
        rotateR_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                rotateR_flag = true;
                showTile();
                rotateR_flag = false;
            }

        });
        rotateR_B.addKeyListener(myKeyListener);
        flipLR_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                flipLR_flag = true;
                showTile();
                flipLR_flag = false;
            }

        });
        flipLR_B.addKeyListener(myKeyListener);
        flipUD_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                flipUD_flag = true;
                showTile();
                flipUD_flag = false;
            }

        });
        flipUD_B.addKeyListener(myKeyListener);
        reset_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                reset_flag = true;
                showTile();
                reset_flag = false;
            }

        });
        reset_B.addKeyListener(myKeyListener);
        tag_B.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(!tagInfoFrame.isShowing())
                {
                    tag_B.setLabel("Hide  Tag Info");
                    tagInfoFrame.setImageNo(imageNo);
                    tagInfoFrame.setVisible(true);
                    return;
                } else
                {
                    tag_B.setLabel("Show  Tag Info");
                    tagInfoFrame.setVisible(false);
                    return;
                }
            }

        });
        tag_B.addKeyListener(myKeyListener);
        tagInfoFrame.addWindowListener(new WindowListener() {

            public void windowClosing(WindowEvent windowevent)
            {
                tag_B.setLabel("Show  Tag Info");
                tagInfoFrame.setVisible(false);
            }

            public void windowOpened(WindowEvent windowevent)
            {
            }

            public void windowClosed(WindowEvent windowevent)
            {
            }

            public void windowIconified(WindowEvent windowevent)
            {
            }

            public void windowDeiconified(WindowEvent windowevent)
            {
            }

            public void windowActivated(WindowEvent windowevent)
            {
            }

            public void windowDeactivated(WindowEvent windowevent)
            {
            }

        });
        scrollPane1.addKeyListener(myKeyListener);
        panel1.addKeyListener(myKeyListener);
        controlPanel.addKeyListener(myKeyListener);
        buttonPanel.addKeyListener(myKeyListener);
        mousePanel.addKeyListener(myKeyListener);
        borderPanel1.addKeyListener(myKeyListener);
        infoPanel.addKeyListener(myKeyListener);
        copyrightPanel.addKeyListener(myKeyListener);
        addKeyListener(myKeyListener);
        requestFocus();
    }

    private void add2controlPanel(int i, int j, int k, Component component)
    {
        gridBagConstraints1.gridx = i;
        gridBagConstraints1.gridy = j;
        gridBagConstraints1.gridwidth = k;
        gridBagLayout1.setConstraints(component, gridBagConstraints1);
        controlPanel.add(component);
    }

    private void add2mousePanel(int i, int j, int k, Component component)
    {
        gridBagConstraints2.gridx = i;
        gridBagConstraints2.gridy = j;
        gridBagConstraints2.gridwidth = k;
        gridBagLayout2.setConstraints(component, gridBagConstraints2);
        mousePanel.add(component);
    }

    public void start()
    {
        if(isThreadStarted)
            return;
        dicomData = new DicomData();
        imageData = new ImageData();
        if(NUM > TMPSIZE)
        {
            arrayIndex = new int[TMPSIZE];
            dicomData_tmp = new DicomData[TMPSIZE];
            imageData_tmp = new ImageData[TMPSIZE];
            tagInfoFrame.setTagInfoFrame(TMPSIZE);
        } else
        {
            arrayIndex = new int[NUM];
            dicomData_tmp = new DicomData[NUM];
            imageData_tmp = new ImageData[NUM];
            tagInfoFrame.setTagInfoFrame(NUM);
        }
        index = new int[NUM];
        ww = new int[NUM];
        wl = new int[NUM];
        for(int i = 0; i < NUM; i++)
            index[i] = -1;

        for(int j = 0; j < arrayIndex.length; j++)
            arrayIndex[j] = -1;

        for(int k = 0; k < NUM; k++)
            ww[k] = 0;

        for(int l = 0; l < NUM; l++)
            wl[l] = 0;

        showStatus("Now Loading Dicom Dictionary....");
        DicomDic dicomdic = new DicomDic(dicURL);
        dicomFile = new DicomFile(isLtlEndian, vrType, privacy, dicomdic);
        dicomdic = null;
        int i1 = dicomData_tmp.length;
        if(imageNo - (i1 >> 1) <= 0)
            start = 0;
        else
            start = imageNo - (i1 >> 1);
        if(start + i1 > NUM)
            start = NUM - i1;
        loader = new LoaderThread(start, i1, 0, this);
        start_old = start;
        loader.start();
        takeData(imageNo);
        width = imageData.getWidth();
        height = imageData.getHeight();
        imageTiledCanvas = new ImageTiledCanvas(width, height, this);
        imageTiledCanvas.addKeyListener(myKeyListener);
        showTile();
        scrollPane1.add(imageTiledCanvas, null);
        if(imageData.color())
            setRGBEnabled(false);
        if(NUM == 1)
        {
            wwwlALL_C.setEnabled(false);
            cine_B.setEnabled(false);
            more_B.setEnabled(false);
            checkboxGroup1.setSelectedCheckbox(wwwlSingle_C);
            synchro_flag = false;
        }
        isThreadStarted = true;
    }

    public void stop()
    {
        if(loader != null)
        {
            if(loader.isAlive())
                changeStopRequest(true);
            while(requestStop)
                try
                {
                    wait(200L);
                    if(!loader.isAlive())
                        changeStopRequest(false);
                }
                catch(InterruptedException _ex) { }
            loader = null;
        }
        if(animationThread != null)
        {
            animationThread.requestStop();
            animationThread = null;
        }
    }

    public void destroy()
    {
        stop();
    }

    public synchronized void postData(int i, int j, int k)
    {
        int l = 0;
        int i1 = 0;
        DicomData dicomdata = new DicomData();
        ImageData imagedata = new ImageData();
        showStatus("Now Loading Dicom File....  (" + (i + 1) + ")");
        dicomdata = dicomFile.load(imgURL[i]);
        showStatus("Now Creating Dicom Image....  (" + (i + 1) + ")");
        imagedata.setData(dicomdata);
        for(int j1 = 0; j1 < arrayIndex.length; j1++)
            if(j > arrayIndex[j1] || k <= arrayIndex[j1])
            {
                i1 = arrayIndex[j1];
                l = j1;
            }

        if(i1 > -1)
            index[i1] = -1;
        index[i] = l;
        arrayIndex[l] = i;
        dicomData_tmp[l] = null;
        imageData_tmp[l] = null;
        dicomData_tmp[l] = dicomdata;
        imageData_tmp[l] = imagedata;
        tagInfoFrame.setDicomData(dicomdata, l, i);
        notifyAll();
        dicomdata = null;
        imagedata = null;
    }

    public synchronized void takeData(int i)
    {
        while(index[i] < 0)
            try
            {
                wait();
            }
            catch(InterruptedException _ex) { }
        int j = index[i];
        dicomData = null;
        imageData = null;
        dicomData = dicomData_tmp[j];
        imageData = imageData_tmp[j];
        infoPanel.setDicomData(dicomData);
        notifyAll();
    }

    public synchronized ImageData takeImageData(int i)
    {
        while(index[i] < 0)
            try
            {
                wait();
            }
            catch(InterruptedException _ex) { }
        int j = index[i];
        notifyAll();
        return imageData_tmp[j];
    }

    public synchronized boolean confirmStopRequest()
    {
        return requestStop;
    }

    public synchronized void changeStopRequest(boolean flag)
    {
        requestStop = flag;
        notifyAll();
    }

    public synchronized void startLoaderThread(int i, int j, int k)
    {
        if(loader.isAlive())
            changeStopRequest(true);
        while(requestStop)
            try
            {
                wait(200L);
                if(!loader.isAlive())
                    changeStopRequest(false);
            }
            catch(InterruptedException _ex) { }
        loader = null;
        loader = new LoaderThread(i, j, k, this);
        loader.start();
        notifyAll();
    }

    private double getFieldValue(TextField textfield)
    {
        double d;
        try
        {
            d = Double.valueOf(textfield.getText()).doubleValue();
        }
        catch(NumberFormatException _ex)
        {
            d = 0.0D;
        }
        return d;
    }

    public void drag_changeZoom(int i)
    {
        zoom -= 0.0050000000000000001D * (double)i;
        if(zoom < 0.25D)
            zoom = 0.25D;
        else
        if(zoom > 2D)
            zoom = 2D;
        imageTiledCanvas.changeZoom(zoom);
    }

    private void changeCanvasSize()
    {
        double d = 100D / (double)canvasSize;
        if(d > 2D)
            zoom = 2D;
        else
        if(d < 0.25D)
            zoom = 0.25D;
        else
            zoom = d;
        imageTiledCanvas.changeCanvasSize((double)canvasSize * 0.01D);
        imageTiledCanvas.changeZoom(zoom);
    }

    private void getCanvasSize()
    {
        Dimension dimension = scrollPane1.getSize();
        int i = (int)(((double)dimension.width / (double)(width * column)) * 100D);
        int j = (int)(((double)dimension.height / (double)(height * row)) * 100D);
        int k;
        if(j < i)
            k = j;
        else
            k = i;
        if(k > 100)
            k = 100;
        canvasSize = k;
        changeCanvasSize();
    }

    public void drag_changeWwWl(int i, int j)
    {
        int k = imageData.getWW() + i;
        if(k < 0)
            k = 0;
        else
        if(k > imageData.getPixelMax() - imageData.getPixelMin())
            k = imageData.getPixelMax() - imageData.getPixelMin();
        int l = imageData.getWL() + j;
        if(l < imageData.getPixelMin())
            l = imageData.getPixelMin();
        else
        if(l > imageData.getPixelMax())
            l = imageData.getPixelMax();
        imageTiledCanvas.setWW_WL(k, l, imageNo - tile_start);
        imageTiledCanvas.changeImage(imageData.wwANDwl(k, l), imageNo - tile_start);
    }

    public void dragDone_changeWwWl(int i, int j, int k, int l)
    {
        if(!synchro_flag)
        {
            int i1 = imageData.getWW() + i;
            if(i1 < 0)
                i1 = 0;
            else
            if(i1 > imageData.getPixelMax() - imageData.getPixelMin())
                i1 = imageData.getPixelMax() - imageData.getPixelMin();
            int k1 = imageData.getWL() + k;
            if(k1 < imageData.getPixelMin())
                k1 = imageData.getPixelMin();
            else
            if(k1 > imageData.getPixelMax())
                k1 = imageData.getPixelMax();
            imageTiledCanvas.setWW_WL(i1, k1, imageNo - tile_start);
            imageTiledCanvas.changeImage(imageData.wwANDwl(i1, k1), imageNo - tile_start);
            ww[imageNo] += j;
            wl[imageNo] += l;
            return;
        }
        int j2 = 0;
        int i2 = row * column;
        for(int k2 = tile_start; k2 < tile_start + i2; k2++)
        {
            if(j2 >= dicomData_tmp.length)
                break;
            ImageData imagedata = takeImageData(k2);
            int j1;
            int l1;
            if(k2 == imageNo)
            {
                j1 = imagedata.getWW() + i;
                if(j1 < 0)
                    j1 = 0;
                else
                if(j1 > imagedata.getPixelMax() - imagedata.getPixelMin())
                    j1 = imagedata.getPixelMax() - imagedata.getPixelMin();
                l1 = imagedata.getWL() + k;
                if(l1 < imagedata.getPixelMin())
                    l1 = imagedata.getPixelMin();
                else
                if(l1 > imagedata.getPixelMax())
                    l1 = imagedata.getPixelMax();
            } else
            {
                j1 = imagedata.getWW() + j;
                if(j1 < 0)
                    j1 = 0;
                else
                if(j1 > imagedata.getPixelMax() - imagedata.getPixelMin())
                    j1 = imagedata.getPixelMax() - imagedata.getPixelMin();
                l1 = imagedata.getWL() + l;
                if(l1 < imagedata.getPixelMin())
                    l1 = imagedata.getPixelMin();
                else
                if(l1 > imagedata.getPixelMax())
                    l1 = imagedata.getPixelMax();
            }
            imageTiledCanvas.setWW_WL(j1, l1, j2);
            imageTiledCanvas.setImage(imagedata.wwANDwl(j1, l1), j2);
            j2++;
        }

        for(int l2 = j2; l2 < i2; l2++)
            imageTiledCanvas.setImage(null, l2);

        for(int i3 = 0; i3 < NUM; i3++)
            ww[i3] += j;

        for(int j3 = 0; j3 < NUM; j3++)
            wl[j3] += l;

    }

    private void defaultWwWl()
    {
        if(!synchro_flag)
        {
            int i = imageData.getDefaultWW();
            int k = imageData.getDefaultWL();
            imageTiledCanvas.setWW_WL(i, k, imageNo - tile_start);
            imageTiledCanvas.changeImage(imageData.wwANDwl(i, k), imageNo - tile_start);
            ww[imageNo] = 0;
            wl[imageNo] = 0;
            return;
        }
        int j1 = 0;
        int i1 = row * column;
        for(int k1 = tile_start; k1 < tile_start + i1; k1++)
        {
            if(j1 >= dicomData_tmp.length)
                break;
            ImageData imagedata = takeImageData(k1);
            int j = imagedata.getDefaultWW();
            int l = imagedata.getDefaultWL();
            imageTiledCanvas.setWW_WL(j, l, j1);
            imageTiledCanvas.setImage(imagedata.wwANDwl(j, l), j1);
            j1++;
        }

        for(int l1 = j1; l1 < i1; l1++)
            imageTiledCanvas.setImage(null, l1);

        for(int i2 = 0; i2 < NUM; i2++)
            ww[i2] = 0;

        for(int j2 = 0; j2 < NUM; j2++)
            wl[j2] = 0;

    }

    private void cineMode()
    {
        if(animationThread == null)
        {
            notCine = false;
            cine_B.setLabel("Stop");
            cineNext1_B.setFont(bold);
            cineNext2_B.setFont(plain);
            cinePrev1_B.setFont(plain);
            cinePrev2_B.setFont(plain);
            cineNext1_B.setEnabled(true);
            cinePrev1_B.setEnabled(true);
            cineNext2_B.setEnabled(true);
            cinePrev2_B.setEnabled(true);
            animationThread = new AnimationThread(this);
            animationThread.start();
            return;
        } else
        {
            notCine = true;
            cine_B.setLabel("Cine Mode");
            cineNext1_B.setEnabled(false);
            cineNext2_B.setEnabled(false);
            cinePrev1_B.setEnabled(false);
            cinePrev2_B.setEnabled(false);
            animationThread.requestStop();
            animationThread = null;
            return;
        }
    }

    public void changeImageNo()
    {
        int i = dicomData_tmp.length;
        if(imageNo - (i >> 1) <= 0)
            start = 0;
        else
            start = imageNo - (i >> 1);
        if(start + i > NUM)
            start = NUM - i;
        startLoaderThread(start, i, start_old);
        start_old = start;
        showTile();
    }

    private void moreFrame()
    {
        double d2 = (double)canvasSize * 0.01D;
        Dimension dimension = scrollPane1.getSize();
        if(!less_B.isEnabled())
            less_B.setEnabled(true);
        double d = (double)dimension.height - (double)height * d2 * (double)row;
        double d1 = (double)dimension.width - (double)width * d2 * (double)column;
        if(d > d1)
            row = row + 1;
        else
            column = column + 1;
        if(row > 9)
            row = 9;
        if(column > 9)
            column = 9;
        getCanvasSize();
        if(row * column >= arrayIndex.length)
            more_B.setEnabled(false);
        showTile();
        scrollPane1.setVisible(false);
        scrollPane1.setBackground(Color.black);
        scrollPane1.setVisible(true);
    }

    private void lessFrame()
    {
        double d2 = (double)canvasSize * 0.01D;
        Dimension dimension = scrollPane1.getSize();
        if(!more_B.isEnabled())
            more_B.setEnabled(true);
        double d = (double)dimension.height - (double)height * d2 * (double)row;
        double d1 = (double)dimension.width - (double)width * d2 * (double)column;
        if(d < d1)
        {
            if(row == 1)
                column = column - 1;
            else
                row = row - 1;
        } else
        if(column == 1)
            row = row - 1;
        else
            column = column - 1;
        if(row < 1)
            row = 1;
        if(column < 1)
            column = 1;
        getCanvasSize();
        if(row * column == 1)
            less_B.setEnabled(false);
        showTile();
        scrollPane1.setVisible(false);
        scrollPane1.setBackground(Color.black);
        scrollPane1.setVisible(true);
    }

    public void changeActive(int i)
    {
        int j = index[i];
        if(j == -1)
            return;
        dicomData = null;
        imageData = null;
        dicomData = dicomData_tmp[j];
        imageData = imageData_tmp[j];
        infoPanel.setDicomData(dicomData);
        imageNo = i;
        imageNo_F.setText(String.valueOf(i + 1));
        imageNo_S.setValue(i + 1);
        if(imageData.color())
        {
            setRGBEnabled(false);
            return;
        } else
        {
            setRGBEnabled(true);
            return;
        }
    }

    private void showTile()
    {
        int i = row * column;
        int j = dicomData_tmp.length;
        imageTiledCanvas.setTileType(row, column);
        int k = 0;
        if(imageNo - (i >> 1) <= 0)
            tile_start = 0;
        else
            tile_start = imageNo - (i >> 1);
        if(tile_start + i > start + j)
            tile_start = (start + j) - i;
        if(tile_start < start)
            tile_start = start;
        imageTiledCanvas.setStartNo(tile_start);
        for(int l = tile_start; l < tile_start + i; l++)
        {
            if(k >= j)
                break;
            ImageData imagedata = takeImageData(l);
            if(inv_flag)
                imagedata.inverse();
            if(rotateL_flag)
                imagedata.rotateL();
            if(rotateR_flag)
                imagedata.rotateR();
            if(flipLR_flag)
                imagedata.flipLR();
            if(flipUD_flag)
                imagedata.flipUD();
            if(reset_flag)
                imagedata.setDefaultPixel();
            if(l == imageNo)
                imageTiledCanvas.setActiveNo(k);
            imageTiledCanvas.setImage(imagedata.getImageWWWL2Current(ww[l], wl[l]), k);
            imageTiledCanvas.setWW_WL(imagedata.getWW(), imagedata.getWL(), k);
            imageTiledCanvas.setStudyInfo(dicomData_tmp[index[l]], k);
            k++;
        }

        for(int i1 = k; i1 < i; i1++)
        {
            imageTiledCanvas.setImage(null, i1);
            imageTiledCanvas.setStudyInfo(null, k);
        }

        takeData(imageNo);
        if(imageData.color())
        {
            setRGBEnabled(false);
            return;
        } else
        {
            setRGBEnabled(true);
            return;
        }
    }

    private void setRGBEnabled(boolean flag)
    {
        default_B.setEnabled(flag);
        inv_B.setEnabled(flag);
        wwwlSingle_C.setEnabled(flag);
        if(NUM == 1)
            wwwlALL_C.setEnabled(false);
        else
            wwwlALL_C.setEnabled(flag);
        if(!flag && (wwwlSingle_C.getState() || wwwlALL_C.getState()))
        {
            move_C.setState(true);
            imageTiledCanvas.setMoveState(true);
        }
    }

    public boolean isFocusTraversable()
    {
        return true;
    }

    public String getAppletInfo()
    {
        return "Applet Information";
    }

    private void changeSelectCheckBox(Checkbox checkbox)
    {
        checkboxGroup1.setSelectedCheckbox(checkbox);
        if(checkbox == wwwlSingle_C)
            synchro_flag = false;
        else
        if(checkbox == wwwlALL_C)
            synchro_flag = true;
        imageTiledCanvas.setMoveState(move_C.getState());
        imageTiledCanvas.setZoomState(zoom_C.getState());
        imageTiledCanvas.setLoupeState(loupe_C.getState());
    }

    private void key_changeZoom(double d)
    {
        double d1 = 100D / (double)canvasSize;
        if(d1 > 2D)
            d1 = 2D;
        else
        if(d1 < 0.25D)
            d1 = 0.25D;
        zoom = d1 * d;
        if(zoom < 0.25D)
            zoom = 0.25D;
        else
        if(zoom > 2D)
            zoom = 2D;
        imageTiledCanvas.changeZoom(zoom);
    }

    private void key_changeWwWl(double d, double d1)
    {
        if(!synchro_flag)
        {
            int i1 = imageData.getPixelMax() - imageData.getPixelMin();
            int i = imageData.getDefaultWW() + ww[imageNo] + (int)((double)i1 * d);
            if(i < 0)
                i = 0;
            else
            if(i > i1)
                i = i1;
            int k = imageData.getDefaultWL() + wl[imageNo] + (int)((double)i * d1);
            if(k < imageData.getPixelMin())
                k = imageData.getPixelMin();
            else
            if(k > imageData.getPixelMax())
                k = imageData.getPixelMax();
            imageTiledCanvas.setWW_WL(i, k, imageNo - tile_start);
            imageTiledCanvas.changeImage(imageData.wwANDwl(i, k), imageNo - tile_start);
            return;
        }
        int l1 = 0;
        int k1 = row * column;
        for(int i2 = tile_start; i2 < tile_start + k1; i2++)
        {
            if(l1 >= dicomData_tmp.length)
                break;
            ImageData imagedata = takeImageData(i2);
            int j1 = imagedata.getPixelMax() - imagedata.getPixelMin();
            int j = imagedata.getDefaultWW() + ww[i2] + (int)((double)j1 * d);
            if(j < 0)
                j = 0;
            else
            if(j > j1)
                j = j1;
            int l = imagedata.getDefaultWL() + wl[i2] + (int)((double)j * d1);
            if(l < imagedata.getPixelMin())
                l = imagedata.getPixelMin();
            else
            if(l > imagedata.getPixelMax())
                l = imagedata.getPixelMax();
            imageTiledCanvas.setWW_WL(j, l, l1);
            imageTiledCanvas.setImage(imagedata.wwANDwl(j, l), l1);
            l1++;
        }

        for(int j2 = l1; j2 < k1; j2++)
            imageTiledCanvas.setImage(null, j2);

    }










}
