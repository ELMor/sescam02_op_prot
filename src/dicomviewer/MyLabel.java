// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   MyLabel.java

package dicomviewer;

import java.applet.AppletContext;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import java.net.URL;

public class MyLabel extends Canvas
{
    class MyMouseListener extends MouseAdapter
    {

        public void mouseEntered(MouseEvent mouseevent)
        {
            setCursor(new Cursor(12));
            color = Color.blue;
            repaint();
        }

        public void mouseExited(MouseEvent mouseevent)
        {
            setCursor(new Cursor(0));
            color = Color.black;
            repaint();
        }

        public void mousePressed(MouseEvent mouseevent)
        {
            try
            {
                URL url = new URL("http://mars.elcom.nitech.ac.jp/dicom/index-e.html");
                appletContext.showDocument(url, "_blank");
                return;
            }
            catch(Exception exception)
            {
                System.out.println("Exception: " + exception.getMessage());
            }
        }

        public void mouseReleased(MouseEvent mouseevent)
        {
        }

        MyMouseListener()
        {
        }
    }


    public static final int LEFT = 0;
    public static final int CENTER = 1;
    public static final int RIGHT = 2;
    private String label;
    private int alignment;
    private static final int hgap = 5;
    private static final int vgap = 3;
    AppletContext appletContext;
    Color color;

    public MyLabel()
    {
        this("", 0);
    }

    public MyLabel(String s)
    {
        this(s, 0);
    }

    public MyLabel(String s, int i)
    {
        color = Color.black;
        label = s;
        alignment = i;
        addMouseListener(new MyMouseListener());
    }

    public void setText(String s)
    {
        label = s;
        repaint();
    }

    public String getText()
    {
        return label;
    }

    public void setAlignment(int i)
    {
        alignment = i;
        repaint();
    }

    public int getAlignment()
    {
        return alignment;
    }

    public Dimension preferredSize()
    {
        getFont();
        int i = getFont().getSize();
        int j = getFontMetrics(getFont()).stringWidth(label);
        return new Dimension(j + 10, i + 6);
    }

    public Dimension minimumSize()
    {
        getFont();
        int i = getFont().getSize();
        int j = getFontMetrics(getFont()).stringWidth(label);
        return new Dimension(j, i);
    }

    public void paint(Graphics g)
    {
        Dimension dimension = size();
        Font font = g.getFont();
        int i = g.getFontMetrics().stringWidth(label);
        int j = font.getSize();
        int k;
        switch(alignment)
        {
        default:
            k = dimension.width - i <= 5 ? 0 : 5;
            break;

        case 1: // '\001'
            k = (dimension.width - i) / 2;
            break;

        case 2: // '\002'
            k = dimension.width - i - (dimension.width - i <= 5 ? 0 : 5);
            break;
        }
        int l = (dimension.height + j) / 2 - g.getFontMetrics(font).getMaxDescent();
        g.setColor(color);
        g.drawString(label, k, l);
    }

    public void setAppletContext(AppletContext appletcontext)
    {
        appletContext = appletcontext;
    }
}
