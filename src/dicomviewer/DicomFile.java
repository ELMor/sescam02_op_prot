// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DicomFile.java

package dicomviewer;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.StringTokenizer;

// Referenced classes of package dicomviewer:
//            DicomData, DicomDic

public class DicomFile
{

    int debug_level;
    boolean isLtlEndian;
    boolean vrType;
    boolean patientPrivacy;
    boolean VReqSQ;
    boolean containDic;
    DicomDic dicomDic;
    DicomData dicomData;

    public DicomFile(boolean flag, boolean flag1, boolean flag2, DicomDic dicomdic)
    {
        debug_level = 3;
        VReqSQ = false;
        patientPrivacy = flag2;
        isLtlEndian = flag;
        vrType = flag1;
        dicomDic = dicomdic;
    }

    public DicomFile(boolean flag, boolean flag1, DicomDic dicomdic)
    {
        this(flag, flag1, false, dicomdic);
    }

    public DicomFile(DicomDic dicomdic)
    {
        this(true, false, false, dicomdic);
    }

    public DicomData load(String s)
    {
        dicomData = new DicomData();
        try
        {
            URL url = new URL(s);
            BufferedInputStream bufferedinputstream = new BufferedInputStream(url.openStream());
            DataInputStream datainputstream = new DataInputStream(bufferedinputstream);
            byte abyte0[] = new byte[2];
            byte abyte1[] = new byte[4];
            String s4;
            String s5;
            for(; datainputstream.read(abyte0) != -1; analyzer(s4, s5))
            {
                int j = readInt2(abyte0);
                String s2 = Integer.toString((j & 0xf000) >> 12, 16);
                s2 = s2 + Integer.toString((j & 0xf00) >> 8, 16);
                s2 = s2 + Integer.toString((j & 0xf0) >> 4, 16);
                s2 = s2 + Integer.toString(j & 0xf, 16);
                datainputstream.readFully(abyte0);
                j = readInt2(abyte0);
                String s3 = Integer.toString((j & 0xf000) >> 12, 16);
                s3 = s3 + Integer.toString((j & 0xf00) >> 8, 16);
                s3 = s3 + Integer.toString((j & 0xf0) >> 4, 16);
                s3 = s3 + Integer.toString(j & 0xf, 16);
                s4 = "(" + s2 + "," + s3 + ")";
                if(debug_level > 3)
                    System.out.println("currentTag is : " + s4);
                dicomData.setTag(s4);
                containDic = dicomDic.isContain(s4);
                int k;
                if(vrType && !VReqSQ)
                {
                    StringBuffer stringbuffer1 = new StringBuffer(2);
                    datainputstream.readFully(abyte0);
                    for(int l = 0; l < 2; l++)
                        stringbuffer1.append((char)abyte0[l]);

                    dicomData.setVR(s4, stringbuffer1.toString());
                    if(stringbuffer1.toString().equals("OB") || stringbuffer1.toString().equals("OW") || stringbuffer1.toString().equals("SQ"))
                    {
                        datainputstream.skip(2L);
                        datainputstream.readFully(abyte1);
                        k = readInt4(abyte1);
                    } else
                    {
                        datainputstream.readFully(abyte0);
                        k = readInt2(abyte0);
                    }
                } else
                {
                    if(containDic)
                        dicomData.setVR(s4, dicomDic.getVR(s4));
                    else
                        dicomData.setVR(s4, "na");
                    datainputstream.readFully(abyte1);
                    k = readInt4(abyte1);
                }
                if(s4.equals("(fffe,e0dd)"))
                    VReqSQ = false;
                s5 = dicomData.getVR(s4);
                if(debug_level > 3)
                    System.out.println("currentVR is : " + s5);
                if(debug_level > 3)
                    System.out.println("currentLength: " + k);
                if(k == -1)
                {
                    VReqSQ = true;
                    k = 0;
                }
                byte abyte2[] = new byte[k];
                datainputstream.readFully(abyte2);
                dicomData.setValue(s4, abyte2);
                if(containDic)
                {
                    dicomData.setName(s4, dicomDic.getName(s4));
                    dicomData.setVM(s4, dicomDic.getVM(s4));
                    dicomData.setVersion(s4, dicomDic.getVersion(s4));
                } else
                {
                    dicomData.setName(s4, "NotContainedInDICOMDictionary");
                    dicomData.setVM(s4, "na");
                    dicomData.setVersion(s4, "na");
                }
                if(debug_level > 3)
                    System.out.println("currentName is : " + dicomData.getName(s4));
            }

            datainputstream.close();
            bufferedinputstream.close();
        }
        catch(EOFException eofexception)
        {
            System.out.println("DicomFile.EOFException: " + eofexception.getMessage());
        }
        catch(IOException ioexception)
        {
            System.out.println("DicomFile.IOException: " + ioexception.getMessage());
        }
        catch(Exception exception)
        {
            System.out.println("DicomFile.Exception: " + exception.getMessage());
        }
        if(patientPrivacy)
        {
            String s1 = dicomData.getAnalyzedValue("(0010,0010)");
            StringBuffer stringbuffer = new StringBuffer(s1);
            for(int i = 0; i < s1.length(); i++)
                if(i % 2 == 1)
                    stringbuffer.setCharAt(i, '*');

            dicomData.setAnalyzedValue("(0010,0010)", stringbuffer.toString());
        }
        return dicomData;
    }

    private int readInt2(byte abyte0[])
    {
        int i;
        if(isLtlEndian)
            i = (0xff & abyte0[1]) << 8 | 0xff & abyte0[0];
        else
            i = (0xff & abyte0[0]) << 8 | 0xff & abyte0[1];
        return i;
    }

    private int readInt4(byte abyte0[])
    {
        int i;
        if(isLtlEndian)
            i = (0xff & abyte0[3]) << 24 | (0xff & abyte0[2]) << 16 | (0xff & abyte0[1]) << 8 | 0xff & abyte0[0];
        else
            i = (0xff & abyte0[0]) << 24 | (0xff & abyte0[1]) << 16 | (0xff & abyte0[2]) << 8 | 0xff & abyte0[3];
        return i;
    }

    private void analyzer(String s, String s1)
    {
        if(s1 == null)
            dicomData.setAnalyzedValue(s, "Not contain VR.");
        else
        if(dicomData.getValueLength(s) == 0)
            dicomData.setAnalyzedValue(s, "");
        else
        if(s1.equals("PN") | s1.equals("LO") | s1.equals("SH") | s1.equals("LT") | s1.equals("ST") | s1.equals("UI") | s1.equals("DS") | s1.equals("CS") | s1.equals("IS") | s1.equals("AS"))
        {
            for(int i = 0; i < dicomData.getValueLength(s); i++)
                if(dicomData.getValue(s)[i] == 0)
                    dicomData.getValue(s)[i] = 20;

            dicomData.setAnalyzedValue(s, new String(dicomData.getValue(s)));
        } else
        if(s1.equals("SS"))
        {
            int j;
            if(isLtlEndian)
                j = (dicomData.getValue(s)[1] & 0xff) << 8 | dicomData.getValue(s)[0] & 0xff;
            else
                j = (dicomData.getValue(s)[0] & 0xff) << 8 | dicomData.getValue(s)[1] & 0xff;
            if((j & 0x8000) == 32768)
                j |= 0xffff0000;
            dicomData.setAnalyzedValue(s, Integer.toString(j));
        } else
        if(s1.equals("US"))
        {
            int k;
            if(isLtlEndian)
                k = (dicomData.getValue(s)[1] & 0xff) << 8 | dicomData.getValue(s)[0] & 0xff;
            else
                k = (dicomData.getValue(s)[0] & 0xff) << 8 | dicomData.getValue(s)[1] & 0xff;
            dicomData.setAnalyzedValue(s, Integer.toString(k));
        } else
        if(s1.equals("UL"))
        {
            int l;
            if(isLtlEndian)
                l = (dicomData.getValue(s)[3] & 0xff) << 24 | (dicomData.getValue(s)[2] & 0xff) << 16 | (dicomData.getValue(s)[1] & 0xff) << 8 | dicomData.getValue(s)[0] & 0xff;
            else
                l = (dicomData.getValue(s)[0] & 0xff) << 24 | (dicomData.getValue(s)[1] & 0xff) << 16 | (dicomData.getValue(s)[2] & 0xff) << 8 | dicomData.getValue(s)[3] & 0xff;
            dicomData.setAnalyzedValue(s, Integer.toString(l));
        } else
        if(s1.equals("TM"))
        {
            dicomData.setAnalyzedValue(s, new String(dicomData.getValue(s)));
            StringBuffer stringbuffer = new StringBuffer(dicomData.getAnalyzedValue(s));
            stringbuffer.insert(2, ":");
            stringbuffer.insert(5, ":");
            dicomData.setAnalyzedValue(s, stringbuffer.toString());
        } else
        if(s1.equals("DA"))
        {
            dicomData.setAnalyzedValue(s, new String(dicomData.getValue(s)));
            if(dicomData.getValueLength(s) == 8)
            {
                StringBuffer stringbuffer1 = new StringBuffer(dicomData.getAnalyzedValue(s));
                stringbuffer1.insert(4, "-");
                stringbuffer1.insert(7, "-");
                dicomData.setAnalyzedValue(s, stringbuffer1.toString());
            } else
            if(dicomData.getValueLength(s) == 10)
            {
                StringTokenizer stringtokenizer = new StringTokenizer(dicomData.getAnalyzedValue(s), ".");
                String s2 = stringtokenizer.nextToken();
                s2 = s2 + "-" + stringtokenizer.nextToken();
                s2 = s2 + "-" + stringtokenizer.nextToken();
                dicomData.setAnalyzedValue(s, s2);
            }
        } else
        {
            dicomData.setAnalyzedValue(s, "Unknown VR");
        }
        if(debug_level > 3)
            System.out.println("AnalyzedValue :" + dicomData.getAnalyzedValue(s));
    }
}
