// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   BorderPanel.java

package dicomviewer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Panel;

public class BorderPanel extends Panel
{

    boolean isLabelNull;
    Color color;
    Color upLeft;
    Color downRight;
    String label;
    Font font;
    FontMetrics fm;

    public BorderPanel()
    {
        isLabelNull = true;
        font = new Font("Dialog", 0, 12);
        fm = getFontMetrics(font);
        super.setLayout(new FlowLayout(0, 5, 5));
    }

    public BorderPanel(String s)
    {
        isLabelNull = true;
        isLabelNull = false;
        font = new Font("Dialog", 0, 12);
        fm = getFontMetrics(font);
        label = s;
        super.setLayout(new FlowLayout(1, 5, fm.getHeight()));
    }

    public void setLayout(LayoutManager layoutmanager)
    {
    }

    public void paint(Graphics g)
    {
        color = Color.gray;
        upLeft = color.darker();
        downRight = color.brighter();
        int i;
        int j;
        int k;
        if(isLabelNull)
        {
            i = getSize().width;
            j = getSize().height;
            k = 0;
            g.setColor(upLeft);
            g.drawLine(0, 0, i - 1, 0);
            g.setColor(downRight);
            g.drawLine(1, 1, i - 2, 1);
        } else
        {
            i = getSize().width;
            j = (getSize().height - fm.getHeight()) + 5;
            k = fm.getAscent() >> 1;
            int l = fm.stringWidth(label);
            g.setFont(font);
            g.setColor(Color.black);
            g.drawString(label, fm.getAscent(), 10);
            g.setColor(upLeft);
            g.drawLine(0, k, 7, k);
            g.drawLine(l + 16, k, i - 1, k);
            g.setColor(downRight);
            g.drawLine(1, k + 1, 7, k + 1);
            g.drawLine(l + 16, k + 1, i - 2, k + 1);
        }
        g.setColor(upLeft);
        g.drawLine(0, k, 0, j - 1);
        g.setColor(downRight);
        g.drawLine(1, k + 1, 1, j - 2);
        g.setColor(upLeft);
        g.drawLine(1, j - 2, i - 2, j - 2);
        g.setColor(downRight);
        g.drawLine(0, j - 1, i - 1, j - 1);
        g.setColor(upLeft);
        g.drawLine(i - 2, k + 1, i - 2, j - 2);
        g.setColor(downRight);
        g.drawLine(i - 1, k, i - 1, j - 1);
    }

    public void updata(Graphics g)
    {
        paint(g);
    }
}
