// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   DicomData.java

package dicomviewer;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

public class DicomData
    implements Serializable
{
    class Dicomvalue
    {

        String name;
        String vr;
        String vm;
        String version;
        byte value[];
        int valueLength;
        String analyzedValue;

        Dicomvalue()
        {
        }
    }


    Hashtable table;

    public void setTag(String s)
    {
        table.put(s, new Dicomvalue());
    }

    public void setValue(String s, byte abyte0[])
    {
        ((Dicomvalue)table.get(s)).value = new byte[abyte0.length];
        System.arraycopy(abyte0, 0, ((Dicomvalue)table.get(s)).value, 0, abyte0.length);
        ((Dicomvalue)table.get(s)).valueLength = abyte0.length;
    }

    public void setName(String s, String s1)
    {
        ((Dicomvalue)table.get(s)).name = s1;
    }

    public void setVR(String s, String s1)
    {
        ((Dicomvalue)table.get(s)).vr = s1;
    }

    public void setVM(String s, String s1)
    {
        ((Dicomvalue)table.get(s)).vm = s1;
    }

    public void setVersion(String s, String s1)
    {
        ((Dicomvalue)table.get(s)).version = s1;
    }

    public void setAnalyzedValue(String s, String s1)
    {
        ((Dicomvalue)table.get(s)).analyzedValue = s1;
    }

    public byte[] getValue(String s)
    {
        return ((Dicomvalue)table.get(s)).value;
    }

    public int getValueLength(String s)
    {
        return ((Dicomvalue)table.get(s)).valueLength;
    }

    public String getName(String s)
    {
        return ((Dicomvalue)table.get(s)).name;
    }

    public String getVR(String s)
    {
        return ((Dicomvalue)table.get(s)).vr;
    }

    public String getVM(String s)
    {
        return ((Dicomvalue)table.get(s)).vm;
    }

    public String getVersion(String s)
    {
        return ((Dicomvalue)table.get(s)).version;
    }

    public String getAnalyzedValue(String s)
    {
        return ((Dicomvalue)table.get(s)).analyzedValue;
    }

    public Enumeration keys()
    {
        return table.keys();
    }

    public void removeAll()
    {
        table.clear();
    }

    public boolean isContain(String s)
    {
        return table.containsKey(s);
    }

    public DicomData()
    {
        table = new Hashtable();
    }
}
